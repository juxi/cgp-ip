﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace CGPIP2
{
    public partial class ServerStatusForm : Form
    {
        public Series FitnessVsTimeSeries = null;
        public Series TestcasesVsTimeSeries = null;
        private double LastFitnessAdded = double.MaxValue;
        public ServerStatusForm()
        {
            InitializeComponent();
        }

        private void ServerStatusForm_Load(object sender, EventArgs e)
        {
            timer1.Interval = 1000;
            timer1.Enabled = true;

            FitnessVsTimeChart.ChartAreas[0].AxisX.Title = "Evaluations";
            FitnessVsTimeChart.ChartAreas[0].AxisY.Title = "Fitness";
            FitnessVsTimeSeries = new Series();
          FitnessVsTimeSeries.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
          
          TestcasesVsTimeSeries = new Series();

          TestcasesVsTimeSeries.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
          FitnessVsTimeChart.ChartAreas[0].AxisY2.Title = "Test cases";
          FitnessVsTimeChart.ChartAreas[0].AxisY2.Enabled = AxisEnabled.True;
          FitnessVsTimeChart.ChartAreas[0].AxisY2.IsInterlaced = false;
            
          this.FitnessVsTimeChart.Series.Add(FitnessVsTimeSeries);
          this.FitnessVsTimeChart.Series.Add(TestcasesVsTimeSeries);
          FitnessVsTimeSeries.YAxisType = AxisType.Primary;
          TestcasesVsTimeSeries.YAxisType = AxisType.Secondary;
          
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
              CGPIP2CommunicationsService.ServerIndLock.WaitOne();

#if Pareto
            List<int> NodeCounts = CGPIP2CommunicationsService.ServerInds.Keys.ToList();
            NodeCounts.Sort();

            if (NodeCounts.Count > 0)
            {
                dataGridView1.RowCount = NodeCounts.Count+1;
                dataGridView1.ColumnCount = 2;
                
                dataGridView1[0, 0].Value = "Node count";
                dataGridView1[1, 0].Value = "Fitness";

                ParetoChart.Series.Clear();
                ParetoChart.ChartAreas[0].AxisX.Title = "Node count";
                ParetoChart.ChartAreas[0].AxisY.Title = "Fitness";

                Series ParetoSeries = new Series();
                ParetoSeries.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
                ParetoSeries.MarkerStyle = MarkerStyle.Circle;
                ParetoSeries.MarkerSize = 4;
                for (int i = 0; i < NodeCounts.Count; i++)
                {
                    dataGridView1[0, i+1].Value = NodeCounts[i].ToString();
                    dataGridView1[1, i + 1].Value = CGPIP2CommunicationsService.ServerInds[NodeCounts[i]].Fitness.Error.ToString();
                    ParetoSeries.Points.AddXY(dataGridView1[0, i + 1].Value, dataGridView1[1, i + 1].Value);
                }

                ParetoChart.Series.Add(ParetoSeries);


            }

#endif
            if (CGPIP2CommunicationsService.ServerInd != null)
            {
                
                if (LastFitnessAdded != CGPIP2CommunicationsService.ServerInd.Fitness.Error)
                {
                    FitnessVsTimeChart.ChartAreas[0].AxisX.Minimum = 0;
                    FitnessVsTimeSeries.Points.AddXY(CGPIP2CommunicationsService.TotalEvaluations(), CGPIP2CommunicationsService.ServerInd.Fitness.Error);
                    TestcasesVsTimeSeries.Points.AddXY(CGPIP2CommunicationsService.TotalEvaluations(),Parameters.ServerTestCasesCount);
                    FitnessVsTimeSeries.Color = Color.Red;
                    LastFitnessAdded = CGPIP2CommunicationsService.ServerInd.Fitness.Error;

                 //   FitnessVsTimeChart.SaveImage("./trace/fitnessVstimePlot.emf", ChartImageFormat.EmfPlus);
                }
                
            }

            CGPIP2CommunicationsService.ServerIndLock.ReleaseMutex();
            
        }
    }
}
