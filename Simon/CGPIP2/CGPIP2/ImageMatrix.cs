﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CGPIP2
{
    public partial class ImageMatrix : Form
    {
        public static ImageMatrix ImageMatrixForm = null;

        private int Rows;
        private int Cols;
        private int CellWidth;
        private int CellHeight;
        public Bitmap Buffer;
        private Graphics Canvas;
       
        public Bitmap[] ImageCells = null;

        public double[] MCCs = null;
        public CGPImageTestCase.TestCaseUseType[] UseType = null;
        public bool[] CorrectClassification = null;

        public ImageMatrix()
        {
            InitializeComponent();
            UpdateDisplayCb += UpdateDisplayFn;
        }

        public ImageMatrix(int Rows, int Cols, int CellWidth, int CellHeight, int MaxImages)
        {
            UpdateDisplayCb += UpdateDisplayFn;
            this.Rows = Rows;
            this.Cols = Cols;
            this.CellWidth = CellWidth;
            this.CellHeight = CellHeight;
            this.ImageCells = new Bitmap[this.Rows * this.Cols];
            this.Buffer = new Bitmap(this.Cols * this.CellWidth, (this.Rows * this.CellHeight)+50);
            this.MCCs = new double[this.Rows * this.Cols];
            this.UseType = new CGPImageTestCase.TestCaseUseType[this.Rows * this.Cols];
            this.CorrectClassification = new bool[this.Rows * this.Cols];
            InitializeComponent();
            
            this.Width = this.Buffer.Width+50;
            this.Height = this.Buffer.Height+50;



            this.Canvas = Graphics.FromImage(this.Buffer);
            this.MaxImages = MaxImages;

        }

        private void ImageMatrix_Load(object sender, EventArgs e)
        {

        }
        bool Busy = false;
        private int MaxImages;
        private string Caption;

        public delegate void UpdateDisplayDelegate();
        public UpdateDisplayDelegate UpdateDisplayCb;

        private void UpdateDisplayFn()
        {
           
            if (Busy) return;
            Busy = true;
            this.Canvas.Clear(Color.Black);
            int Index = 0;
            for (int i = 0; i < this.Cols;i++ )
                for (int j = 0; j < this.Rows; j++)
                {
                    if (Index >= this.MaxImages)
                        continue;

                    
                    if (this.ImageCells[Index] != null )
                    {
                        
                            Rectangle Src = new Rectangle(0, 0, this.ImageCells[Index].Width, this.ImageCells[Index].Height);

                            double Scale = (double)Math.Max(this.CellHeight, this.CellWidth) / Math.Max(this.ImageCells[Index].Width, this.ImageCells[Index].Height);
                            int DestWidth = (int)(Scale * this.ImageCells[Index].Width);
                            int DestHeight = (int)(Scale * this.ImageCells[Index].Height);
                            Rectangle Dst = new Rectangle((i * this.CellWidth) + ((CellWidth - DestWidth) / 2), (j * this.CellHeight) + ((CellHeight - DestHeight) / 2), DestWidth, DestHeight);

                        this.Canvas.DrawImage(this.ImageCells[Index], Dst, Src, GraphicsUnit.Pixel);

                        Pen MCCPen = new Pen(this.MCCs[Index]<0 ? Color.Red : Color.Blue, 4);
                        int MCCLineY = (j * this.CellHeight)+this.CellHeight-(int)(MCCPen.Width /2 );
                        int MCCLineStartX= (i * this.CellWidth) + (this.CellWidth / 2);
                        int MCCLineEndX = MCCLineStartX + (int)(this.MCCs[Index] * (this.CellWidth / 2));
                        this.Canvas.DrawLine(MCCPen, MCCLineStartX, MCCLineY, MCCLineEndX, MCCLineY);

                        if (this.UseType[Index] == CGPImageTestCase.TestCaseUseType.Validation)
                        {
                            Pen ValidationPen = new Pen(Color.FromArgb(64,255,255,64), 2);
                            this.Canvas.DrawRectangle(ValidationPen, Dst);
                        }
                       /* if (Parameters.ImageIsClassification)
                        {
                            if (!this.CorrectClassification[Index])
                            {
                                Pen ClassificationPen = new Pen(Color.Red);
                                this.Canvas.DrawLine(ClassificationPen, 
                                    i * this.CellWidth, 
                                    j * this.CellHeight, 
                                    (i * this.CellWidth)+this.CellWidth, 
                                    (j * this.CellHeight)+this.CellHeight);
                            }
                        }*/

                    }
                    else
                    {
                        Rectangle Dst = new Rectangle(i * this.CellWidth, j * this.CellHeight, this.CellWidth, this.CellHeight);
                        this.Canvas.FillRectangle(new SolidBrush(Color.FromArgb(32,32,32)), Dst);
                    }
                    Index++;
                    
                    
                }

            int FontSize = 24;

            if (Caption == null)
                Caption = "**";

            if (Caption.Length > 100)
                Caption = Caption.Substring(0,97)+"...";

            Font F = new Font(FontFamily.GenericSansSerif, FontSize);
            SizeF TextSize = this.Canvas.MeasureString(this.Caption, F);



            while (TextSize.Width > this.Buffer.Width)
            {
                FontSize--;
                F = new Font(FontFamily.GenericSansSerif, FontSize);
                TextSize = this.Canvas.MeasureString(this.Caption, F);
            }

            Brush FontBrush = new SolidBrush(Color.White);
            Brush BGBrush = new SolidBrush(Color.Black);

           
            this.Canvas.FillRectangle(BGBrush, 0, this.Buffer.Height - TextSize.Height, this.Buffer.Width, TextSize.Height);
            this.Canvas.DrawString(this.Caption, F, FontBrush, 0, this.Buffer.Height - TextSize.Height);

               
                Busy = false;
                this.panAndZoomPictureBox1.Image = this.Buffer;

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        internal void SetCaption(string p)
        {
            this.Caption = p;
        }

        private void panAndZoomPictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
