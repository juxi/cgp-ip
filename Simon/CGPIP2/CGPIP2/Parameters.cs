﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CGPIP2
{
    public static class Parameters
    {
        public static void SetParameter(string ParameterName, string ParameterValue)
        {
            Type type = typeof(Parameters);
            string[] ValueTokens = null;
            Reporting.Say("Trying to set " + ParameterName);
            foreach (var p in type.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy))
            {
             
                if (p.Name == ParameterName)
                {
                    Reporting.Say("\t" + p.Name + " = " + ParameterValue + ", was "+p.GetValue(null));
                    if (ParameterValue == "null" || ParameterValue == "")
                    {
                        p.SetValue(null, null);
                    }
                    else if (p.FieldType == typeof(string))
                    {
                        p.SetValue(null, ParameterValue);
                    }
                    else if (p.FieldType == typeof(bool))
                    {
                        p.SetValue(null, bool.Parse(ParameterValue));
                    }
                    else if (p.FieldType == typeof(double))
                    {
                        p.SetValue(null, double.Parse(ParameterValue));
                    }
                    else if (p.FieldType == typeof(int))
                    {
                        p.SetValue(null, int.Parse(ParameterValue));
                    }
                    else if (p.FieldType == typeof(Double[]))
                    {
                        ValueTokens = ParameterValue.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        double[] Values = new double[ValueTokens.Length];
                        for(int i=0;i<ValueTokens.Length;i++)
                            Values[i] =double.Parse(ValueTokens[i]);
                        p.SetValue(null, Values);
                    }
                    else if (p.FieldType == typeof(float[]))
                    {
                        ValueTokens = ParameterValue.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        float[] Values = new float[ValueTokens.Length];
                        for (int i = 0; i < ValueTokens.Length; i++)
                            Values[i] = float.Parse(ValueTokens[i]);
                        p.SetValue(null, Values);
                    }
                    else if (p.FieldType == typeof(int[]))
                    {
                        ValueTokens = ParameterValue.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        int[] Values = new int[ValueTokens.Length];
                        for (int i = 0; i < ValueTokens.Length; i++)
                            Values[i] = int.Parse(ValueTokens[i]);
                        p.SetValue(null, Values);
                    }
                    else if (p.FieldType == typeof(Size[]))
                    {
                        ValueTokens = ParameterValue.Split(new char[] { ',',' ' }, StringSplitOptions.RemoveEmptyEntries);
                        int[] Values = new int[ValueTokens.Length];
                        for (int i = 0; i < ValueTokens.Length; i++)
                            Values[i] = int.Parse(ValueTokens[i]);
                        Size[] Sizes = new Size[Values.Length / 2];
                        for (int i = 0; i < ValueTokens.Length / 2; i++)
                        {
                            Sizes[i] = new Size(Values[i * 2], Values[(i * 2) + 1]);
                        }
                        p.SetValue(null, Sizes);
                    }
                    else
                        throw new Exception(ParameterName + " cannot be set this way. I don't know the type info");
                }
            }
        }


        ///////////////////////////////////////////////////////////////////
        //  Loading the parameters from a file, these control e.g. folders
        //  for the input/ouput images, etc.
        ///////////////////////////////////////////////////////////////////
        public static void LoadParameters(string ParametersFileName)
        {
            // read the file if it exists
            if (!File.Exists(ParametersFileName))
                throw new Exception("Cannot find " + ParametersFileName);
            string[] Lines = File.ReadAllLines(ParametersFileName);

            Console.WriteLine("Setting parameters from config file " + ParametersFileName);
            
            foreach (string Line in Lines)
            {
                string[] Tokens = Line.Split('\t');

                // load another file, if specified (for stacking of parameters)
                if (Line.StartsWith("#file"))
                {
                    LoadParameters(Tokens[1]);
                }

                // ignore comment lines
                if (Line.StartsWith("#") || Line.StartsWith("//"))
                    continue;

                // parse and set the parameters specified in the file
                try
                {
                    SetParameter(Tokens[0].Trim(), Tokens[1]);
                }
                catch (Exception e)
                {
                    Console.WriteLine("error setting " + Tokens[0] + " to " + Tokens[1]);
                    Console.WriteLine(e.ToString());
                    Console.ReadLine();
                }
            }
            
            // if the folders are not set but a base folder is specified
            if (BaseOutputFolder.Length != 0) 
            {
                // overwrite previously set predictions output folder
                if (PredictionsOutputFolder.Length == 0)
                {
                    Console.WriteLine("warning: setting PredictionsOutputFolder will be overwritten");
                } 
                SetParameter("PredictionsOutputFolder", BaseOutputFolder + ExperimentName + "\\Predictions\\");                
                
                // overwrite previously set server output folder
                if (ServerOutputFolder.Length == 0)
                {
                    Console.WriteLine("warning: setting ServerOutputFolder will be overwritten");
                } 
                SetParameter("ServerOutputFolder", BaseOutputFolder + ExperimentName + "\\Server\\");
                
                // overwrite previously set trace output folder
                if (TraceOutputFolder.Length == 0)
                {
                    Console.WriteLine("warning: setting TraceOutputFolder will be overwritten");
                } 
                SetParameter("TraceOutputFolder", BaseOutputFolder + ExperimentName + "\\Trace\\");
            }

        }
      

        static Parameters()
        {
            LoadParameters("parameters.ini");
        }

        public enum FitnessType
        {
            Accuracy,
            Difference,
            MCC
        }


        public enum FitnessAggregation
        {
            ALL,
            AVERAGE,
            WORST
        }

        public static ulong LastFitnessImprovement = 0;

        public static bool FitnessIsClassification = true;
        public static FitnessType FitnessScoreType = FitnessType.MCC;
        public static FitnessAggregation FitnessAggregationType = FitnessAggregation.ALL;
        public static double[] Rescales = {  0.1,0.25,0.15};
        public static Size[] RescaleToFit = null;
        public static int ServerTestCasesCount = 1;
        public static double IncreaseThreshold = 0.3;// +(0.005 * _TestCasesCount);

        public static bool ProcessValidation = false;
        
        public static int DefaultImageWidth = -1;
        public static int DefaultImageHeight = -1;

        public static int MaxFilesToLoad =500;
        public static int MaxFilesToToTest = 15;

        public static bool ShowTrace = false;

        public static int NumberOfPopulations = 1;

        public static int LocalSyncInterval = 10;

        public static int ParametersEvolveInterval = 4;

        public static int OutputCount = 1;
        public static int InputCount = 7;

        public static int RemoteSyncInterval = 20; 

        public static bool Visualize = false;

        public static bool EvolveParameters = false;

        public static int ImageBorder = 4;
        public static int VisualizationSize =64;

        public static int MaxImageDimension = 1024;
        public static int MinImageDimension = 8;
        public static bool[] EvaluationMask;
        public static double ValidationSplit  = 0.3;
        public static int MaxGenotypes =1;

        public static bool EvolveParametersOnly = false;
        public static ulong EvaluationTimeout = 100;

        public static bool Replay = false;
        public static string ReplayFile = "./hands4/ServerIndividual.xml";

        public static bool AddFitnessCasesInSequence = true;
        public static int ServerTestCasesCountIncrement  = 1;

        public static int  MedianFilterOutput = 0;
        public static bool PreComputeAdditionalInputs = false;
        public static bool LocalNormalizeInputs = true;
        public static bool CropToIgnoreMask = false;
        public static bool ScaleInputs = true;
        public static bool SplitHSB = true;
        public static bool SplitBGR = true;
        public static bool FilterTargetsForRed = true;
        public static bool FilterTargetsForBlue = true;
        public static bool InvertTargetClasses = true;
        public static string ImageFilter = "*.png";
        public static int[] ImagePropertyApertures = new int[] {1};

        // the parameters specifying the input and output folders 
        public static string BaseOutputFolder = "";       // Juxi added
        public static string TestCasesInputFolder = "";
        public static string TestCasesTargetsFolder = "";
        public static string PredictionsOutputFolder = "";//   "./Predictions";
        public static string ServerOutputFolder = "";     //   "./ServerOutput/";
        public static string TraceOutputFolder = "";      //   "./RunTrace/";        
// PredictionsOutputFolder	C:\\work\\MI\\zotafoam\\Predictions\\
// ServerOutputFolder	C:\\work\\MI\\zotafoam\\Server\\
// TraceOutputFolder	C:\\work\\MI\\zotafoam\\Trace\\
        
        public static string ExperimentName = "CGPIP";
               
        
        public static int FilterImagesWithSmallDimensions = -1;

        public static bool ImageIsClassification = false;
        public static bool UseOnlyLocalNormalizeInputs = true;
        public static bool OptimizeSpeed = false;
        public static bool OptimizeLength= false;
        public static double OptimizationThreshold = 0.3;
     
        public static double TPWeight = 100000d;
        public static double FPWeight = 100000d;
        public static double TNWeight = 100000d;
        public static double FNWeight = 100000d;

        public static bool ChunkTestCases = false;
        public static int  ChunkTestCaseCount = 64;
        public static int  ChunkTestCaseSize = 32;
        public static double SmartChunkScale = 4;
        public static double[] AddRotatedTestCases = null;//new double[] {0, -5, 5,15,-15};
        public static bool SmartChunkAddRandomAreas = true;
        public static bool AllowRepeats = false;
        public static int FilterImagesWithLargeDimensions = 0;
        public static string PrecookedFolder = "./PuffinsPrecooked/";
        public static bool LoadFromPrecooked = true;
        public static bool SaveToPrecooked = false;
        public static int SmartChunkAddRandomAreasCount = 10;

        public static bool CropInput = true;
        public static int Crop_TX = 100;
        public static int Crop_TY = 0;
        public static int Crop_BX = 1150;
        public static int Crop_BY =665;
        public static bool ResizeInputsToLargest = false;

        public static float[] InputImageBrightnesses = { -1};//, 0.8f, 0.5f,0.25f };

    }

    public class PopulationParameters
    {
        public int GraphSize =50;
       
        public double MutationRate = 0.01;
     
        public int PopulationSize = 5;

        public PopulationParameters Clone()
        {
            PopulationParameters P = new PopulationParameters();

            P.GraphSize = this.GraphSize;
 

            P.MutationRate = this.MutationRate;
         
            P.PopulationSize = this.PopulationSize;

            return P;
        }

        public void Mutate()
        {
            int Gene = FastRandom.Rng.Next(0, 2);
            bool Adjust = FastRandom.Rng.NextBool();

            switch(Gene)
            {
                case(0):
                    if (Adjust)
                        this.MutationRate += (FastRandom.Rng.NextDouble() * 0.1) - 0.05;
                    else
                        this.MutationRate = FastRandom.Rng.NextDouble() * 0.5;
                    break;
               
                case (1):
                    if (Adjust)
                    {
                        this.PopulationSize += FastRandom.Rng.Next(-2, 2);
                        if (PopulationSize < 2)
                            PopulationSize = 2;
                        if (PopulationSize > 20)
                            PopulationSize = 20;
                    }
                    else
                        this.PopulationSize = FastRandom.Rng.Next(2, 10);
                    break;
            }
        }
    }
}