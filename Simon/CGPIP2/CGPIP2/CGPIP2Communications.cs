﻿#define notPareto
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Threading;

namespace CGPIP2
{
    [DataContract]
    public class CGPIP2Communications
    {
        public CGPIP2Communications()
        {

        }
    }
    [ServiceContract()]
    public interface iCGPIP2CommunicationsService
    {
        [OperationContract]
        bool SendIndividual(CGPIndividual Ind, int TestCases, int MaxTestCases,  bool[] EvaluationMask);
      
        [OperationContract]
        CGPIndividual GetIndividual(out bool[] EvaluationMask);

        [OperationContract]
        double GetError();

        [OperationContract]
        void Test();

        [OperationContract]
        int ClientIndex();

        [OperationContract]
        int TestCasesCount();

        [OperationContract]
        bool[] GetEvaluationMask(int TestCases, int MaxTestCases);

        [OperationContract]
        ulong ReportEvaluations(string ProcessID, ulong Evaluations);
              

    }

    public class CGPIP2CommunicationsService : iCGPIP2CommunicationsService
    {
        public static CGPIndividual ServerInd = null;
#if Pareto
        public static Dictionary<int, CGPIndividual> ServerInds = new Dictionary<int, CGPIndividual>();
#endif
        public static Mutex ServerIndLock = new Mutex();
       
        private static bool[] EvaluationMask;
        private static FastRandom RNG = new FastRandom(6666);
        
        
        private static DateTime LastIncrease = DateTime.Now;
#if Pareto
        bool iCGPIP2CommunicationsService.SendIndividual(CGPIndividual Ind, int TestCases, int MaxTestCases, bool[] ClientEvaluationMask)
        {
            bool Result = false;
        
            ServerIndLock.WaitOne();
            if (TestCases != Parameters.ServerTestCasesCount)
            {
                Reporting.Say("Rejecting. Test cases are out of synch. \t" + TestCases + "\t" + Parameters.ServerTestCasesCount);
                ServerIndLock.ReleaseMutex();
                return false;
            }
            ServerIndLock.ReleaseMutex();


            ServerIndLock.WaitOne();
            if (ClientEvaluationMask.Length != CGPIP2CommunicationsService.EvaluationMask.Length)
            {
                Reporting.Say("Rejecting. Test cases are out of synch. 3");
                ServerIndLock.ReleaseMutex();
                return false;
            }
            else
            {
                for(int i=0;i<CGPIP2CommunicationsService.EvaluationMask.Length;i++)
                    if (CGPIP2CommunicationsService.EvaluationMask[i] != ClientEvaluationMask[i])
                    {
                        Reporting.Say("Rejecting. Test cases are out of synch. 4");
                        ServerIndLock.ReleaseMutex();
                        return false;
                    }
            }
            ServerIndLock.ReleaseMutex();

            if (CGPIP2CommunicationsService.ServerInd == null)
            {
                ServerIndLock.WaitOne();
                CGPIP2CommunicationsService.ServerInd = Ind;
                ServerIndLock.ReleaseMutex();
                Reporting.Say("Accepting individual. Reason: I have nothing.");
                Reporting.Say("Threshold = " + Parameters.IncreaseThreshold);
                Result = true;
            }
            else
            {
                if (Ind.Fitness.Error < CGPIP2CommunicationsService.ServerInd.Fitness.Error)
                {
                    ServerIndLock.WaitOne();
                    CGPIP2CommunicationsService.ServerInd = Ind;
                    ServerIndLock.ReleaseMutex();
                    Reporting.Say("Accepting individual. Reason: " + Ind.Fitness.Error + " is better than " + CGPIP2CommunicationsService.ServerInd.Fitness.Error);
                   // Reporting.Say("Threshold = " + IncreaseThreshold);
                    Result = true;
                }                
            }

            ServerIndLock.WaitOne();
            if (ServerInds.ContainsKey(Ind.Fitness.TotalEvaluatedNodeCount))
            {
                CGPIndividual CurrentBest = ServerInds[Ind.Fitness.TotalEvaluatedNodeCount];
                if (CurrentBest.Fitness.Error > Ind.Fitness.Error)
                    ServerInds[Ind.Fitness.TotalEvaluatedNodeCount] = Ind;
            }
            else
            {
                ServerInds.Add(Ind.Fitness.TotalEvaluatedNodeCount, Ind);
            }
            ServerIndLock.ReleaseMutex();

            if (Result)
            {
                ServerIndLock.WaitOne();
                CGPIndividual.SaveToXml(CGPIP2CommunicationsService.ServerInd, "ServerIndividual.xml");
                ServerIndLock.ReleaseMutex();
            }

            TimeSpan TimeSinceLastIncrement =
                DateTime.Now.Subtract(LastIncrease);
            bool TimeOut = TimeSinceLastIncrement.TotalMinutes > 10;
            if ((Result && Parameters.ServerTestCasesCount < MaxTestCases) || TimeOut)
            {
                if (Parameters.ServerTestCasesCount < MaxTestCases && (ServerInd.Fitness.Error < Parameters.IncreaseThreshold || TimeOut))
                {
                    LastIncrease = DateTime.Now;
                    ServerIndLock.WaitOne();
                    int CurrentTestCasesCount = Parameters.ServerTestCasesCount;
                    Parameters.ServerTestCasesCount += Parameters.ServerTestCasesCountIncrement;
                    if (Parameters.ServerTestCasesCount > MaxTestCases)
                        Parameters.ServerTestCasesCount = MaxTestCases;
                    
                    CGPIP2CommunicationsService.ServerInd = null;

                    if (Parameters.AddFitnessCasesInSequence)
                    {
                        for (int i = 0; i < Parameters.ServerTestCasesCount; i++)
                        {
                            int p = i;//
                            EvaluationMask[p] = true;
                        }
                    }
                    else
                    {

                        for (int i = 0; i < Parameters.ServerTestCasesCount - CurrentTestCasesCount; i++)                        
                        {
                            int p = RNG.Next(0, MaxTestCases);
                            int MaskAttempts = 0;
                            bool MaskSucess = true;
                            while (EvaluationMask[p] && MaskSucess)
                            {
                                p = RNG.Next(0, MaxTestCases);
                                MaskAttempts++;
                                if (MaskAttempts >= MaxTestCases * 10)
                                {
                                    MaskSucess = false;
                                }
                            }
                            if (MaskSucess)
                                EvaluationMask[p] = true;
                            else
                                break;
                        }
                    }
                    /*EvaluationMask = new bool[MaxTestCases];

                    for (int i = 0; i < TestCases; i++)
                    {
                        int p = RNG.Next(0, MaxTestCases);
                        while (EvaluationMask[p])
                            p = RNG.Next(0, MaxTestCases);
                        EvaluationMask[p] = true;
                    }
                    */
                    

                    if (Parameters.ServerTestCasesCount > MaxTestCases)
                        Parameters.ServerTestCasesCount = MaxTestCases;
                    Reporting.Say("Increased number of test cases. Now " + Parameters.ServerTestCasesCount);
                    if (TimeOut)
                        Reporting.Say("Due to boredom");
                    
                    ServerInds = new Dictionary<int, CGPIndividual>();
                    Reporting.Say("Threshold = " + Parameters.IncreaseThreshold);
                    ServerIndLock.ReleaseMutex();
                }
            }

            return Result;
        }
#else


        bool iCGPIP2CommunicationsService.SendIndividual(CGPIndividual Ind, int TestCases, int MaxTestCases, bool[] ClientEvaluationMask)
        {
            bool Result = false;


            ServerIndLock.WaitOne();
            if (TestCases != Parameters.ServerTestCasesCount)
            {
                Reporting.Say("Rejecting. Test cases are out of synch." +  TestCases + " != " + Parameters.ServerTestCasesCount);
                
                ServerIndLock.ReleaseMutex();
                return false;
            }
            ServerIndLock.ReleaseMutex();


            ServerIndLock.WaitOne();
            if (ClientEvaluationMask.Length != CGPIP2CommunicationsService.EvaluationMask.Length)
            {
                Reporting.Say("Rejecting. Test cases are out of synch. 3");
                ServerIndLock.ReleaseMutex();
                return false;
            }
            else
            {
                for (int i = 0; i < CGPIP2CommunicationsService.EvaluationMask.Length; i++)
                    if (CGPIP2CommunicationsService.EvaluationMask[i] != ClientEvaluationMask[i])
                    {
                        Reporting.Say("Rejecting. Test cases are out of synch. 4");
                        ServerIndLock.ReleaseMutex();
                        return false;
                    }
            }
            ServerIndLock.ReleaseMutex();

            if (CGPIP2CommunicationsService.ServerInd == null)
            {
                ServerIndLock.WaitOne();
                CGPIP2CommunicationsService.ServerInd = Ind;
                ServerIndLock.ReleaseMutex();
                Reporting.Say("Accepting individual. Reason: I have nothing.");
                Reporting.Say("Threshold = " + Parameters.IncreaseThreshold);
                Result = true;
            }
            else
            {
                if (Ind.Fitness.Error < CGPIP2CommunicationsService.ServerInd.Fitness.Error)
                {
                    ServerIndLock.WaitOne();
                    CGPIP2CommunicationsService.ServerInd = Ind;
                    ServerIndLock.ReleaseMutex();
                    Reporting.Say("Accepting individual. Reason: " + Ind.Fitness.Error + " is better than " + CGPIP2CommunicationsService.ServerInd.Fitness.Error);
                    // Reporting.Say("Threshold = " + IncreaseThreshold);
                    Result = true;
                }
            }


            if (Result)
            {
                ServerIndLock.WaitOne();
                CGPIndividual.SaveToXml(CGPIP2CommunicationsService.ServerInd, String.Format("{1}/ServerIndividual_{2}_{0:0000}.xml",
                    Ind.Fitness.TotalEvaluatedNodeCount,
                    Parameters.ServerOutputFolder,
                    Parameters.ExperimentName
                    ));
                ServerIndLock.ReleaseMutex();
            }

            TimeSpan TimeSinceLastIncrement =
                DateTime.Now.Subtract(LastIncrease);
            bool TimeOut = TimeSinceLastIncrement.TotalMinutes > 10;
            if ((Result && Parameters.ServerTestCasesCount < MaxTestCases) || TimeOut)
            {
                if (Parameters.ServerTestCasesCount < MaxTestCases && (ServerInd.Fitness.Error < Parameters.IncreaseThreshold || TimeOut))
                {
                    LastIncrease = DateTime.Now;
                    ServerIndLock.WaitOne();
                    int CurrentTestCasesCount = Parameters.ServerTestCasesCount;
                    Parameters.ServerTestCasesCount += Parameters.ServerTestCasesCountIncrement;
                    if (Parameters.ServerTestCasesCount > MaxTestCases)
                        Parameters.ServerTestCasesCount = MaxTestCases;

                    CGPIP2CommunicationsService.ServerInd = null;

                    if (Parameters.AddFitnessCasesInSequence)
                    {
                        for (int i = 0; i < Parameters.ServerTestCasesCount; i++)
                        {
                            int p = i;//
                            EvaluationMask[p] = true;
                        }
                    }
                    else
                    {
                    /*    if (EvaluationMask.Length < Parameters.ServerTestCasesCount)
                        {
                            Parameters.ServerTestCasesCount = EvaluationMask.Length;
                            CurrentTestCasesCount = Parameters.ServerTestCasesCount;
                        }
                        */
                        for (int i = 0; i < Parameters.ServerTestCasesCount - CurrentTestCasesCount; i++)
                        {
                            int p = RNG.Next(0, MaxTestCases);
                            int MaskAttempts = 0;
                            bool MaskSucess = true;
                            while (EvaluationMask[p] && MaskSucess)
                            {
                                p = RNG.Next(0, MaxTestCases);
                                MaskAttempts++;
                                if (MaskAttempts >= MaxTestCases * 10)
                                {
                                    MaskSucess = false;
                                }
                            }
                            if (MaskSucess)
                                EvaluationMask[p] = true;
                            else
                                break;
                        }
                    }
                    /*EvaluationMask = new bool[MaxTestCases];

                    for (int i = 0; i < TestCases; i++)
                    {
                        int p = RNG.Next(0, MaxTestCases);
                        while (EvaluationMask[p])
                            p = RNG.Next(0, MaxTestCases);
                        EvaluationMask[p] = true;
                    }
                    */


                    if (Parameters.ServerTestCasesCount > MaxTestCases)
                        Parameters.ServerTestCasesCount = MaxTestCases;
                    Reporting.Say("Increased number of test cases. Now " + Parameters.ServerTestCasesCount);
                    if (TimeOut)
                        Reporting.Say("Due to boredom");

                  
                    Reporting.Say("Threshold = " + Parameters.IncreaseThreshold);
                    ServerIndLock.ReleaseMutex();
                }
            }

            return Result;
        }
#endif

#if Pareto
        private CGPIndividual TournamentSelectFromPareto(int TournamentSize)
        {
            List<int> Keys = ServerInds.Keys.ToList();
            CGPIndividual Best = ServerInds[Keys[0]];

            for (int i = 0; i < TournamentSize; i++)
            {
                int Index = FastRandom.Rng.Next(Keys.Count);
                CGPIndividual Candidate = ServerInds[Keys[Index]];
                if (Candidate.Fitness.Error < Best.Fitness.Error)
                    Best = Candidate;
                else if (Candidate.Fitness.Error == Best.Fitness.Error && Best.Fitness.TotalEvaluatedNodeCount>Candidate.Fitness.TotalEvaluatedNodeCount)
                    Best = Candidate;
            }

            return Best;
        }
#endif


#if Pareto
        CGPIndividual iCGPIP2CommunicationsService.GetIndividual(out bool[] ClientEvaluationMask)
        {
            /*CGPIndividual R = null;
            ServerIndLock.WaitOne();
            if (ServerInd!=null)
                R = ServerInd.Clone();
            ClientEvaluationMask = new bool[CGPIP2CommunicationsService.EvaluationMask.Length];
            Array.Copy(CGPIP2CommunicationsService.EvaluationMask, ClientEvaluationMask, EvaluationMask.Length);
            ServerIndLock.ReleaseMutex();
            */

            CGPIndividual R = null;
            ServerIndLock.WaitOne();
            if (ServerInds.Count > 0)
            {
                int TS = ServerInds.Count / 4;
                if (TS<2) TS=2;
                R = TournamentSelectFromPareto(TS);
            }
            ClientEvaluationMask = new bool[CGPIP2CommunicationsService.EvaluationMask.Length];
            Array.Copy(CGPIP2CommunicationsService.EvaluationMask, ClientEvaluationMask, EvaluationMask.Length);
            ServerIndLock.ReleaseMutex();

            return R;
        }
#else
        CGPIndividual iCGPIP2CommunicationsService.GetIndividual(out bool[] ClientEvaluationMask)
        {
            CGPIndividual R = null;
            ServerIndLock.WaitOne();
            if (ServerInd!=null)
                R = ServerInd.Clone();
            ClientEvaluationMask = new bool[CGPIP2CommunicationsService.EvaluationMask.Length];
            Array.Copy(CGPIP2CommunicationsService.EvaluationMask, ClientEvaluationMask, EvaluationMask.Length);
            ServerIndLock.ReleaseMutex();
       

            return R;
        }
#endif

        void iCGPIP2CommunicationsService.Test()
        {
            Reporting.Say("Communications test");
        }


        double iCGPIP2CommunicationsService.GetError()
        {
            if (ServerInd == null)
                return double.NaN;
            else
                return ServerInd.Fitness.Error;
        }


        public static int ClientIndexCounter = 0;
        

        int iCGPIP2CommunicationsService.ClientIndex()
        {
            return ClientIndexCounter++;
        }


        int iCGPIP2CommunicationsService.TestCasesCount()
        {
            
            return Parameters.ServerTestCasesCount;
        }


        bool[] iCGPIP2CommunicationsService.GetEvaluationMask(int TestCases, int MaxTestCases)
        {
            if (EvaluationMask == null)
            {
                ServerIndLock.WaitOne();
                EvaluationMask = new bool[MaxTestCases];

                if (Parameters.AddFitnessCasesInSequence)
                {
                    for (int i = 0; i < Parameters.ServerTestCasesCount; i++)
                    {
                        int p = i;//
                        EvaluationMask[p] = true;
                    }
                }
                else
                {
                    if (EvaluationMask.Length < Parameters.ServerTestCasesCount)
                    {
                        Parameters.ServerTestCasesCount = EvaluationMask.Length;
                        Parameters.MaxFilesToToTest = Parameters.ServerTestCasesCount;
                    }
                    for (int i = 0; i < Parameters.ServerTestCasesCount; i++)
                    {
                        int p = RNG.Next(0, MaxTestCases);
                        
                        while (EvaluationMask[p])
                            p = RNG.Next(0, MaxTestCases);
                        EvaluationMask[p] = true;
                    }
                }

                ServerIndLock.ReleaseMutex();
            }
            return EvaluationMask;
        }


        public static Dictionary<string, ulong> EvaluationsPerClient = new Dictionary<string, ulong>();

        ulong iCGPIP2CommunicationsService.ReportEvaluations(string ProcessID, ulong Evaluations)
        {
            ulong Total = 0;
            ServerIndLock.WaitOne();

            if (EvaluationsPerClient.ContainsKey(ProcessID))
                EvaluationsPerClient[ProcessID] = Evaluations;
            else
                EvaluationsPerClient.Add(ProcessID, Evaluations);

            foreach (ulong Value in EvaluationsPerClient.Values)
            {
                Total += Value;
            }

            ServerIndLock.ReleaseMutex();

            return Total;
        }

        public static ulong TotalEvaluations()
        {
            ulong Total = 0;
            ServerIndLock.WaitOne();
            foreach (ulong Value in EvaluationsPerClient.Values)
            {
                Total += Value;
            }
            ServerIndLock.ReleaseMutex();
            return Total;
        }

    }



}
