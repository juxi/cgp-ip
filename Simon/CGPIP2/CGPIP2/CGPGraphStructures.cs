﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CGPIP2
{
    public class CGPFactory
    {
        public static CGPNode RandomNode(int MaxBack, int FunctionArity)
        {
            CGPNode N = new CGPNode();
            N.Function = FunctionArity >= 0 ? FunctionSetDefinition.RandomFunction(FunctionArity) : FunctionSetDefinition.RandomFunction();
            N.Connection0 = FastRandom.Rng.Next(1, MaxBack);
            N.Connection1 = FastRandom.Rng.Next(1, MaxBack);
            N.Connection2 = FastRandom.Rng.Next(1, MaxBack);
            N.GrayValue = (float)((FastRandom.Rng.NextDouble() * 512) - 256);
            N.Height = FastRandom.Rng.Next(1, 32);
            N.Width = FastRandom.Rng.Next(1, 32);
#if GABOR
            N.GaborRotation = (short)FastRandom.Rng.Next(0, 8);
            N.GaborFreq = (short)FastRandom.Rng.Next(0, 16);
#endif
            N.FeatureConfig = FastRandom.Rng.Next(0,4);
            N.Evaluated = false;
            N.Repeat = FastRandom.Rng.Next(1, 4);

            return N;
        }

        public static CGPNode oldMutateNode(CGPNode Node, int MaxBack, int FunctionArity)
        {
            CGPNode Mutant = Node.Clone();
            int GeneCount = Parameters.AllowRepeats ? 11 : 10;
            if (!FunctionSetDefinition.CGPFunctions.Contains(CGPFunction.FEATURE0))
                GeneCount--;

            int GeneToMutate = FastRandom.Rng.Next(Parameters.EvolveParametersOnly ? 4 : 0, GeneCount);
            switch (GeneToMutate)
            {
                case (0):
                    Mutant.Function = FunctionArity >= 0 ? FunctionSetDefinition.RandomFunction(FunctionArity) : FunctionSetDefinition.RandomFunction();
                    break;
                case (1):
                    Mutant.Connection0 = FastRandom.Rng.Next(1, MaxBack);
                    break;
                case (2):
                    Mutant.Connection1 = FastRandom.Rng.Next(1, MaxBack);
                    break;
                case (3):
                    Mutant.Connection2 = FastRandom.Rng.Next(1, MaxBack);
                    break;
                case (4):
                    if (FastRandom.Rng.NextBool())
                        Mutant.GrayValue = (float)((FastRandom.Rng.NextDouble() * 512) - 256);
                    else
                        Mutant.GrayValue = Node.GrayValue + (float)FastRandom.Rng.NextNormal(0, 10);
                    break;
                case (5):
                    Mutant.Height = FastRandom.Rng.Next(1, 32);
                    break;
                case (6):
                    Mutant.Width = FastRandom.Rng.Next(1, 32);
                    break;
#if GABOR
                case (7):
                    Mutant.GaborRotation = (short)FastRandom.Rng.Next(-8, 8);
                    break;
                case (8):
                    Mutant.GaborFreq = (short)FastRandom.Rng.Next(0, 16);
                    break;
#endif
                case (9):
                    Mutant.FeatureConfig = FastRandom.Rng.Next(0, 4);
                    break;
                case(10):
                    if (!Parameters.AllowRepeats)
                        throw new Exception("Should not be mutating this as option is not enabled");
                    Mutant.Repeat = FastRandom.Rng.Next(1, 4);
                    break;
                default:
                    throw new Exception("wtf");

            }

            Mutant.Width = Mutant.Height;
            return Mutant;
        }

        public static int Mutate(int OriginalValue, int MaxDeviation, int MinValue, int MaxValue)
        {
             int v = FastRandom.Rng.Next(OriginalValue - MaxDeviation, OriginalValue + MaxDeviation);
             if (v < MinValue) return MinValue;
             if (v > MaxValue) return MaxValue;
                return v;
        }

        public static CGPNode MutateNode(CGPNode Node, int MaxBack, int FunctionArity)
        {
            CGPNode Mutant = Node.Clone();
            int GeneCount = Parameters.AllowRepeats ? 11 : 10;
            if (!FunctionSetDefinition.CGPFunctions.Contains(CGPFunction.FEATURE0))
                GeneCount--;

            int[] Weights = new int[]
            {
                3, //FUNCTION
                1, // Connection0
                1, // Connection1
                1, // Connection2                
                3, // GrayValue
                3, // Height
                3, // Width

               0, // GaborRotation
                0, // GaborFreq

                3, // FeatureConfig
                0, // AllowRepeats,            
            };
            int WeightsSum = Weights.Sum();

            //int GeneToMutate = FastRandom.Rng.Next(Parameters.EvolveParametersOnly ? 4 : 0, GeneCount);
            int M = FastRandom.Rng.Next(0, WeightsSum);
            int ACC = 0;
            int WeightsIndex = 0;
            while (ACC <= M)
            {
                ACC += Weights[WeightsIndex];
                if ((ACC <= M))
                WeightsIndex++;
            }
                
            int GeneToMutate = WeightsIndex;


            switch (GeneToMutate)
            {
                case (0):
                    Mutant.Function = FunctionArity >= 0 ? FunctionSetDefinition.RandomFunction(FunctionArity) : FunctionSetDefinition.RandomFunction();
                    break;
                case (1):
                    Mutant.Connection0 = FastRandom.Rng.Next(1, MaxBack);
                    break;
                case (2):
                    Mutant.Connection1 = FastRandom.Rng.Next(1, MaxBack);
                    break;
                case (3):
                    Mutant.Connection2 = FastRandom.Rng.Next(1, MaxBack);
                    break;
                case (4):
                    if (FastRandom.Rng.NextDouble()<0.1)
                        Mutant.GrayValue = (float)((FastRandom.Rng.NextDouble() * 512) - 256);
                    else
                        Mutant.GrayValue = Node.GrayValue + (float)FastRandom.Rng.NextNormal(0, 10);
                    break;
                case (5):
                    if (FastRandom.Rng.NextDouble() < 0.1)
                        Mutant.Height = FastRandom.Rng.Next(1, 32);
                    else
                        Mutant.Height = Mutate(Mutant.Height, 2, 1, 32);
                    break;
                case (6):
                    if (FastRandom.Rng.NextDouble() < 0.1)
                        Mutant.Width = FastRandom.Rng.Next(1, 32);
                    else
                        Mutant.Width = Mutate(Mutant.Width, 2, 1, 32);
                    break;
#if GABOR
                case (7):
                    if (FastRandom.Rng.NextDouble() < 0.1)
                        Mutant.GaborRotation = (short)FastRandom.Rng.Next(-8, 8);
                    else
                        Mutant.GaborRotation = (short)Mutate(Mutant.GaborRotation, 2, -8, 8);
                    break;
                case (8):
                     if (FastRandom.Rng.NextDouble() < 0.1)
                        Mutant.GaborRotation = (short)FastRandom.Rng.Next(0, 16);
                    else
                        Mutant.GaborRotation = (short)Mutate(Mutant.GaborRotation, 2, 0, 16);
                    break;
#else
                case (7):
                case (8):
                    throw new Exception("Gabor not supported");
                    break;                        
#endif

                case (9):
                    Mutant.FeatureConfig = FastRandom.Rng.Next(0, 4);
                    break;
                case (10):
                    if (!Parameters.AllowRepeats)
                        throw new Exception("Should not be mutating this as option is not enabled");
                    Mutant.Repeat = FastRandom.Rng.Next(1, 4);
                    break;
              
                default:
                    throw new Exception("wtf");

            }
  
            return Mutant;
        }


        public static CGPGraph RandomGraph(int InputCount, int Width)
        {
            CGPGraph G = new CGPGraph(Width);
            G.GeneratedNodeCount = 0;
           
                for (int i = 0; i < G.Width; i++)
                {
                    G.GeneratedNodeCount++;
                    G.Nodes[i] = RandomNode(InputCount + i, -1);                     
                }

            return G;
        }
    }

    [Serializable]
    public class CGPGraph
    {
        public int Width;
        
        public CGPNode[] Nodes;
        public int GeneratedNodeCount = 0;

        public CGPGraph()
        {

        }

        public CGPGraph(int Width)
        {
            this.Width = Width;
            this.Nodes = new CGPNode[this.Width];            
        }

        public CGPGraph Clone()
        {
            CGPGraph G = new CGPGraph(this.Width);
           
            for (int i = 0; i < this.Width; i++)
                if (this.Nodes[i] != null)
                    G.Nodes[i] = this.Nodes[i].Clone();
            return G;
        }

        public void ClearEvaluatedFlags()
        {
          
                for (int i = 0; i < this.Width; i++)
                    if (this.Nodes[i] != null)
                        this.Nodes[i].Evaluated = false;
        }

        public void Resize(int NewSize)
        {
            CGPNode[] NewNodes = new CGPNode[NewSize];

            for (int i = 0; i < Math.Min(NewSize, this.Nodes.Length); i++)
                NewNodes[i] = this.Nodes[i].Clone();

            for (int i = 0; i < NewSize; i++)
            {
                if (NewNodes[i] == null)
                {
                    NewNodes[i] = CGPFactory.RandomNode(i + Parameters.InputCount + 1, -1);
                }
            }
            this.Width = NewSize;
            this.Nodes = NewNodes;
        }
    }

    [Serializable]
    public class CGPNode
    {
        public CGPFunction Function;
        public int Connection0;
        public int Connection1;
        public int Connection2;

        public float GrayValue = 0;
        public int Width = 0;
        public int Height = 0;
        
       public int FeatureConfig = 0;

#if GABOR
        public short GaborFreq = 0;
        public short GaborRotation = 0;
#endif

        public bool Evaluated = false;  //no need to copy, mutable

        public int Repeat = 1;

        public CGPNode()
        {
        }

        public int Arity
        {
            get
            {
                return FunctionSetDefinition.CGPFunctionArity[this.Function];
            }
        }

        public CGPNode Clone()
        {
            CGPNode N = new CGPNode();
            N.Function = this.Function;
            N.Connection0 = this.Connection0;
            N.Connection1 = this.Connection1;
            N.Connection2 = this.Connection2;
         
            N.GrayValue = this.GrayValue;
            N.Width = this.Width;
            N.Height = this.Height;
            N.Evaluated = this.Evaluated;
#if GABOR
            N.GaborFreq = this.GaborFreq;
            N.GaborRotation = this.GaborRotation;
#endif
            N.FeatureConfig = this.FeatureConfig;
            N.Repeat = this.Repeat;
            return N;
        }
    }
}