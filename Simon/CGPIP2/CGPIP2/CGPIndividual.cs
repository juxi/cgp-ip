﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CGPIP2
{
    [Serializable]
    public class CGPIndividual : IComparable<CGPIndividual>
    {
        public static bool operator <(CGPIndividual A, CGPIndividual B)
        {
            return A.CompareTo(B) < 0;             
        }

        public static bool operator >(CGPIndividual A, CGPIndividual B)
        {
            return A.CompareTo(B) > 0;
        }

        public static CGPIndividual RandomIndividual(PopulationParameters PopParameters)
        {
            CGPIndividual Ind = new CGPIndividual();
            Ind.Genotypes = new List<CGPGraph>();
            //for (int i = 0; i < Parameters.MaxGenotypes; i++)
                Ind.Genotypes.Add(CGPFactory.RandomGraph(Parameters.InputCount, PopParameters.GraphSize));

            Ind.Thresholds = new List<float>();
            //for (int i = 0; i < Parameters.MaxGenotypes; i++)
                Ind.Thresholds.Add((float)FastRandom.Rng.NextDouble() * 256);

            Ind.MixModes = new List<int>();
            //for (int i = 0; i < Parameters.MaxGenotypes; i++)
                Ind.MixModes.Add(FastRandom.Rng.Next(0, 5));

            Ind.Dirty = true;
            return Ind;
        }

        public List<CGPGraph> Genotypes;
        public List<float> Thresholds;
        public List<int> MixModes;

        public FitnessFunctionResult Fitness;
        public ulong EvaluationIndex = 0;
        public bool Evaluated = false;

        public bool Dirty = true;
        public FitnessFunctionResult ParentFitness;

        public CGPIndividual()
        {
        }

        public List<CGPIndividual> Crossover(CGPIndividual Other)
        {
            int minL = Math.Min(Other.Genotypes.Count, this.Genotypes.Count);
            int maxL = Math.Max(Other.Genotypes.Count, this.Genotypes.Count);
            int P = FastRandom.Rng.Next(0, minL);
            
            CGPIndividual A = new CGPIndividual();
            CGPIndividual B = new CGPIndividual();
            A.Dirty = true;
            B.Dirty = true;
            A.Genotypes = new List<CGPGraph>();
            B.Genotypes = new List<CGPGraph>();
            A.Thresholds = new List<float>();
            B.Thresholds = new List<float>();
            A.MixModes = new List<int>();
            B.MixModes = new List<int>();
            for (int i = 0; i < maxL; i++)
            {
                if (i < P)
                {
                    if (i < this.Genotypes.Count)
                    {
                        A.Genotypes.Add(this.Genotypes[i].Clone());
                        A.Thresholds.Add(this.Thresholds[i]);
                        A.MixModes.Add(this.MixModes[i]);
                    }
                    if (i < Other.Genotypes.Count)
                    { 
                        B.Genotypes.Add(Other.Genotypes[i].Clone());
                        B.Thresholds.Add(Other.Thresholds[i]);
                        B.MixModes.Add(this.MixModes[i]);
                    }
                }
                else
                {
                    if (i < this.Genotypes.Count)
                    {
                        B.Genotypes.Add(this.Genotypes[i].Clone());
                        B.Thresholds.Add(this.Thresholds[i]);
                        B.MixModes.Add(this.MixModes[i]);
                    }
                    if (i < Other.Genotypes.Count)
                    {
                        A.Genotypes.Add(Other.Genotypes[i].Clone());
                        A.Thresholds.Add(Other.Thresholds[i]);
                        A.MixModes.Add(Other.MixModes[i]);
                    }
                }
            }



            List<CGPIndividual> R = new List<CGPIndividual>(2);
            R.Add(A); R.Add(B);
            return R;
        }

        public CGPIndividual Clone()
        {
            CGPIndividual NewInd = new CGPIndividual();
            NewInd.Genotypes = new List<CGPGraph>();
            NewInd.Thresholds = new List<float>();
            NewInd.MixModes = new List<int>();
            NewInd.Dirty = this.Dirty;
            if (this.ParentFitness!=null)
                NewInd.ParentFitness = this.ParentFitness.Clone();

            for (int i = 0; i < this.Genotypes.Count; i++)
            {
                NewInd.Genotypes.Add(this.Genotypes[i].Clone());
                NewInd.Thresholds.Add(this.Thresholds[i]);
                NewInd.MixModes.Add(this.MixModes[i]);

            }

            if (this.Fitness != null)
                NewInd.Fitness = this.Fitness.Clone();

           

            return NewInd;
        }

     

        public void Mutate(PopulationParameters PopParameters, bool AllowIncreaseGenotypeCount)
        {
        double MutationRate = PopParameters.MutationRate;

            int MutationsToMake =
                (int)(MutationRate *  this.Genotypes[0].Width);
                //(int)(MutationRate * this.Genotypes.Count * this.Genotypes[0].Width);
            if (MutationsToMake < 0)
                MutationsToMake = 1;

            for (int m = 0; m < MutationsToMake; m++)
            {
                int g =  FastRandom.Rng.Next(0, this.Genotypes.Count); //
                int n = FastRandom.Rng.Next(this.Genotypes[g].Width);
                if (FastRandom.Rng.NextDouble() < 0.01)
                {
                    Dirty = true;
                    int n2 = FastRandom.Rng.Next(this.Genotypes[g].Width);
                    CGPNode T = this.Genotypes[g].Nodes[n];
                    this.Genotypes[g].Nodes[n] = this.Genotypes[g].Nodes[n2];
                    this.Genotypes[g].Nodes[n2] = T;
                }
                else
                {
                    this.Genotypes[g].Nodes[n] = CGPFactory.MutateNode(this.Genotypes[g].Nodes[n], n + Parameters.InputCount, -1);
                    if (this.Genotypes[g].Nodes[n].Evaluated)
                        Dirty = true;
                }
            }

            
            for (int g = 0; g < this.Genotypes.Count; g++)
                if (FastRandom.Rng.NextDouble() < 0.1)
                {
                    if (FastRandom.Rng.NextBool())
                        this.Thresholds[g] = (float)FastRandom.Rng.NextDouble() * 256;
                    else
                        this.Thresholds[g] += ((float)FastRandom.Rng.NextDouble() * 10) - 5;
                    Dirty = true;
                }


            for (int g = 0; g < this.Genotypes.Count; g++)
                if (FastRandom.Rng.NextDouble() < 0.1)
                {

                    this.MixModes[g] = FastRandom.Rng.Next(0, 5);
                    Dirty = true;
                    
                }

            if (this.Genotypes.Count > 1 && FastRandom.Rng.NextDouble() < 0.15)
            {
                int g = FastRandom.Rng.Next(0, this.Genotypes.Count);
                this.Genotypes.RemoveAt(g);
                this.MixModes.RemoveAt(g);
                this.Thresholds.RemoveAt(g);
                Dirty = true;
            }
            if (AllowIncreaseGenotypeCount && FastRandom.Rng.NextDouble() < 0.1)
            {
                int g = FastRandom.Rng.Next(0, this.Genotypes.Count);
                if (this.Genotypes.Count < Parameters.MaxGenotypes && FastRandom.Rng.NextBool())
                {
                    CGPGraph NewG = CGPFactory.RandomGraph(Parameters.InputCount, PopParameters.GraphSize);
                    this.Genotypes.Insert(g, NewG);
                    this.MixModes.Insert(g, FastRandom.Rng.Next(0, 5));
                    this.Thresholds.Insert(g, (float)FastRandom.Rng.NextDouble() * 256);
                }
                else
                {
                    int g2 = FastRandom.Rng.Next(0, this.Genotypes.Count);
                    CGPGraph NewGraph = this.Genotypes[g2];
                    this.Genotypes.RemoveAt(g2);
                    float NewT = this.Thresholds[g2];
                    int NewMM = this.MixModes[g2];
                    this.Genotypes.Insert(g, NewGraph);
                    this.MixModes.Insert(g, NewMM);
                    this.Thresholds.Insert(g, NewT);
                }
                Dirty = true;
            }

          

            //this.MixMode = 1;
        }

        //Based on "reducing wasted evaluations in cgp"
        public void Mutate2(PopulationParameters PopParameters, bool AllowIncreaseGenotypeCount)
        {
       
            int MaxAttemptsToFindActiveNode = this.Genotypes[0].Width;
            for (int m = 0; m < MaxAttemptsToFindActiveNode; m++)
            {
                int g = FastRandom.Rng.Next(0, this.Genotypes.Count);
                int n = FastRandom.Rng.Next(this.Genotypes[g].Width);

                this.Genotypes[g].Nodes[n] = CGPFactory.MutateNode(this.Genotypes[g].Nodes[n], 1 + n + Parameters.InputCount, -1);
             
                if (this.Genotypes[g].Nodes[n].Evaluated)
                {
                    Dirty = true;
                    goto AbortMutations;
                }

            }

        AbortMutations:

            for (int g = 0; g < this.Genotypes.Count; g++)
                if (FastRandom.Rng.NextDouble() < 0.1)
                {
                    if (FastRandom.Rng.NextBool())
                        this.Thresholds[g] = (float)FastRandom.Rng.NextDouble() * 256;
                    else
                        this.Thresholds[g] += ((float)FastRandom.Rng.NextNormal(0, 10));
                    Dirty = true;
                }


            for (int g = 0; g < this.Genotypes.Count; g++)
                if (FastRandom.Rng.NextDouble() < 0.1)
                {

                    this.MixModes[g] = FastRandom.Rng.Next(0, 5);
                    Dirty = true;
                }

            if (this.Genotypes.Count > 1 && FastRandom.Rng.NextDouble() < 0.015)
            {
                int g = FastRandom.Rng.Next(0, this.Genotypes.Count);
                this.Genotypes.RemoveAt(g);
                this.MixModes.RemoveAt(g);
                this.Thresholds.RemoveAt(g);
                Dirty = true;
            }
            if (AllowIncreaseGenotypeCount && FastRandom.Rng.NextDouble() < 0.01)
            {
                int g = FastRandom.Rng.Next(0, this.Genotypes.Count);
                if (this.Genotypes.Count < Parameters.MaxGenotypes && FastRandom.Rng.NextBool())
                {
                    CGPGraph NewG = CGPFactory.RandomGraph(Parameters.InputCount, PopParameters.GraphSize);
                    this.Genotypes.Insert(g, NewG);
                    this.MixModes.Insert(g, FastRandom.Rng.Next(0, 5));
                    this.Thresholds.Insert(g, (float)FastRandom.Rng.NextDouble() * 256);
                }
                else
                {
                    int g2 = FastRandom.Rng.Next(0, this.Genotypes.Count);
                    CGPGraph NewGraph = this.Genotypes[g2];
                    this.Genotypes.RemoveAt(g2);
                    float NewT = this.Thresholds[g2];
                    int NewMM = this.MixModes[g2];
                    this.Genotypes.Insert(g, NewGraph);
                    this.MixModes.Insert(g, NewMM);
                    this.Thresholds.Insert(g, NewT);
                }
                Dirty = true;
            }



            //this.MixMode = 1;
        }

        public void Mutate3(PopulationParameters PopParameters, bool AllowIncreaseGenotypeCount)
        {
            double MutationRate = PopParameters.MutationRate;

            int MutationsToMake =
                (int)(MutationRate * this.Genotypes[0].Width);
            //(int)(MutationRate * this.Genotypes.Count * this.Genotypes[0].Width);
            if (MutationsToMake < 0)
                MutationsToMake = 1;
            int MaxAttemptsToFindActiveNode = this.Genotypes[0].Width;
            for (int m = 0; m < MaxAttemptsToFindActiveNode; m++)
            {
                int g = FastRandom.Rng.Next(0, this.Genotypes.Count); //this.Genotypes.Count - 1;//
                int n = FastRandom.Rng.Next(this.Genotypes[g].Width-10,this.Genotypes[g].Width);

                this.Genotypes[g].Nodes[n] = CGPFactory.MutateNode(this.Genotypes[g].Nodes[n], 1 + n + Parameters.InputCount, -1);
                if (this.Genotypes[g].Nodes[n].Evaluated)
                {
                    Dirty = true;
                    goto AbortMutations;
                }

            }

        AbortMutations:

            for (int g = 0; g < this.Genotypes.Count; g++)
                if (FastRandom.Rng.NextDouble() < 0.1)
                {
                    if (FastRandom.Rng.NextBool())
                        this.Thresholds[g] = (float)FastRandom.Rng.NextDouble() * 256;
                    else
                        this.Thresholds[g] += ((float)FastRandom.Rng.NextNormal(0, 10));
                    Dirty = true;
                }


            for (int g = 0; g < this.Genotypes.Count; g++)
                if (FastRandom.Rng.NextDouble() < 0.1)
                {

                    this.MixModes[g] = FastRandom.Rng.Next(0, 5);
                    Dirty = true;

                }

            if (this.Genotypes.Count > 1 && FastRandom.Rng.NextDouble() < 0.15)
            {
                int g = FastRandom.Rng.Next(0, this.Genotypes.Count);
                this.Genotypes.RemoveAt(g);
                this.MixModes.RemoveAt(g);
                this.Thresholds.RemoveAt(g);
                Dirty = true;
            }
            if (AllowIncreaseGenotypeCount && FastRandom.Rng.NextDouble() < 0.1)
            {
                int g = FastRandom.Rng.Next(0, this.Genotypes.Count);
                if (this.Genotypes.Count < Parameters.MaxGenotypes && FastRandom.Rng.NextBool())
                {
                    CGPGraph NewG = CGPFactory.RandomGraph(Parameters.InputCount, PopParameters.GraphSize);
                    this.Genotypes.Insert(g, NewG);
                    this.MixModes.Insert(g, FastRandom.Rng.Next(0, 5));
                    this.Thresholds.Insert(g, (float)FastRandom.Rng.NextDouble() * 256);
                }
                else
                {
                    int g2 = FastRandom.Rng.Next(0, this.Genotypes.Count);
                    CGPGraph NewGraph = this.Genotypes[g2];
                    this.Genotypes.RemoveAt(g2);
                    float NewT = this.Thresholds[g2];
                    int NewMM = this.MixModes[g2];
                    this.Genotypes.Insert(g, NewGraph);
                    this.MixModes.Insert(g, NewMM);
                    this.Thresholds.Insert(g, NewT);
                }
                Dirty = true;
            }

          

            //this.MixMode = 1;
        }

        #region IComparable<CGPIndividual> Members

        public int CompareTo(CGPIndividual other)
        {
            if (other == this)
                return 0;

            if (other.Fitness.Error < this.Fitness.Error)
                return 1;
            if (other.Fitness.Error > this.Fitness.Error)
                return -1;

            if (Parameters.OptimizeSpeed && this.Fitness.Error < Parameters.OptimizationThreshold)
            {
                if (other.Fitness.Duration < this.Fitness.Duration)
                   return 1;
                if (other.Fitness.Duration > this.Fitness.Duration)
                   return -1;
            }

            if (Parameters.OptimizeLength && this.Fitness.Error < Parameters.OptimizationThreshold)
            {
                if (other.Fitness.TotalEvaluatedNodeCount < this.Fitness.TotalEvaluatedNodeCount)
                    return 1;
                if (other.Fitness.TotalEvaluatedNodeCount > this.Fitness.TotalEvaluatedNodeCount)
                    return -1;
               
            }

     /*       if (this.ParentFitness != null && other.ParentFitness != null)
            {
                double OtherFitnessImprovement = other.ParentFitness.Error - other.Fitness.Error;
                double FitnessImprovement = this.ParentFitness.Error - this.Fitness.Error;
                if (OtherFitnessImprovement>FitnessImprovement)
                    return 1;
                if (OtherFitnessImprovement < FitnessImprovement)
                    return -1;
            }
            */
            if (other.EvaluationIndex < this.EvaluationIndex)
                return -1;
            if (other.EvaluationIndex > this.EvaluationIndex)
                return 1;

            
            return 0;
        }

        #endregion IComparable<CGPIndividual> Members

        public static void SaveToXml(CGPIndividual Ind, string FileName)
        {
            if (Ind == null)
                return;

            try
            {
                //Reporting.Say("Saving to " + FileName);
                System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(Ind.GetType());
                StreamWriter s = new StreamWriter(FileName);
                x.Serialize(s, Ind);
                s.Close();
            }
            catch (Exception e)
            {
                Reporting.Say(e.ToString());
                Reporting.Say(e.StackTrace); ;

            }
            //Reporting.Say("Saved " + FileName);
        }
        public static CGPIndividual LoadFromXml(string FileName)
        {

            System.Xml.Serialization.XmlSerializer reader =
                    new System.Xml.Serialization.XmlSerializer(typeof(CGPIndividual));
            System.IO.StreamReader file = new System.IO.StreamReader(
                FileName);
            CGPIndividual Ind = new CGPIndividual();
            Ind = (CGPIndividual)reader.Deserialize(file);
     
            return Ind;
        }

    }


}