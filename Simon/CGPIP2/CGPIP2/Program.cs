﻿using System;
using System.ServiceModel;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Diagnostics;
using System.Collections.Generic;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Threading;

namespace CGPIP2
{
    internal class Program
    {
        private static double FitnessAtLastLocalSynch;
        private static double FitnessAtLastRemoteSynch;

        private static void TestPopulation(FitnessFunction FitFunc, CGPPopulation Pop)
        {
            for (int i = 0; i < Pop.Count; i++)
            {
                CGPIndividual Ind = Pop[i];
                if (Ind.Evaluated && !Ind.Dirty)
                    continue;
                if (Parameters.Visualize)
                {
                    Thread.Sleep(0);
                    Application.DoEvents();
                }
                if (DebuggerAttached)
                {
                    FitnessFunctionResult R = Parameters.FitnessIsClassification ? FitFunc.Test(Ind, Pop.Parameters) : FitFunc.TestMap(Ind, Pop.Parameters);
                    Ind.Fitness = R;
                }
                else
                {
                    try
                    {
                        FitnessFunctionResult R = Parameters.FitnessIsClassification ? FitFunc.Test(Ind, Pop.Parameters) : FitFunc.TestMap(Ind, Pop.Parameters);
                        Ind.Fitness = R;
                    }
                    catch (Exception e)
                    {
                        Reporting.Say("Problem evaluating individual: " + e.ToString());
                        Reporting.Say(e.StackTrace);
                        Reporting.Say(e.Source);
                        Ind.Fitness = new FitnessFunctionResult();
                        Ind.Fitness.Error = double.MaxValue;
                        Ind.EvaluationIndex = 0;
                    }
                }

                if (Ind.Fitness.TotalEvaluatedNodeCount == 0)
                    Ind.Fitness.Error = double.MaxValue;
            }
        }

        private static CGPIndividual BestIndividual(CGPPopulation[] Populations, out PopulationParameters BestIndividualPopParams)
        {
            foreach (CGPPopulation Pop in Populations)
                Pop.Sort();

            CGPIndividual Best = Populations[0][0];
            BestIndividualPopParams = Populations[0].Parameters;
            for (int i = 1; i < Populations.Length; i++)
            {
                if (Populations[i][0].Fitness.Error < Best.Fitness.Error)
                {
                    Best = Populations[i][0];
                    BestIndividualPopParams = Populations[i].Parameters;
                }
            }

            return Best;
        }

        public static void ServerStatus()
        {
            while (true)
            {
              
                Thread.Sleep(10000);
            }
        }

        public static void StartServer(bool SpawnClients)
        {

            if (File.Exists("ServerIndividual.xml"))
                CGPIP2CommunicationsService.ServerInd = CGPIndividual.LoadFromXml("ServerIndividual.xml");

    

            Uri baseAddress = new Uri("net.tcp://localhost:2204/CGPIP2CommunicationsService");
            Reporting.LogToFile(Parameters.TraceOutputFolder, "serverlog");
            NetTcpBinding binding = new NetTcpBinding();

            ServiceHost serviceHost = new ServiceHost(typeof(CGPIP2CommunicationsService), baseAddress);
            serviceHost.AddServiceEndpoint(typeof(iCGPIP2CommunicationsService), binding, baseAddress);
            serviceHost.Open();

            ServerStatusForm sf = new ServerStatusForm();
            sf.Show();

            if (SpawnClients)
            {
                int ClientCount = Environment.ProcessorCount - 2;
                if (ClientCount < 1)
                    ClientCount = 1;

                for (int i = 0; i < ClientCount; i++)
                {
                    ProcessStartInfo PSI = null;//
                    
                    if (i==0)
                        PSI = new ProcessStartInfo(Application.StartupPath + "/CGPIP2.exe", "client log viz");
                //    else if (i == 1)
               //         PSI = new ProcessStartInfo(Application.StartupPath + "/CGPIP2.exe", "client consts");
                    else
                        PSI = new ProcessStartInfo(Application.StartupPath + "/CGPIP2.exe", "client");

                    PSI.UseShellExecute = true;
                    Process.Start(PSI);
                }
            }

            while (sf.Visible)
            {
                Thread.Sleep(1000);
                Application.DoEvents();
            }

        }

        public static iCGPIP2CommunicationsService StartClient()
        {
            try
            {
                Uri baseAddress = new Uri("net.tcp://localhost:2204/CGPIP2CommunicationsService");
                EndpointAddress address = new EndpointAddress(baseAddress);
                NetTcpBinding binding = new NetTcpBinding();
                binding.ReceiveTimeout = new TimeSpan(1, 5, 0);
                binding.OpenTimeout = new TimeSpan(1, 5, 0);
                binding.SendTimeout = new TimeSpan(1, 5, 0);
                
                ChannelFactory<iCGPIP2CommunicationsService> Factory = new ChannelFactory<iCGPIP2CommunicationsService>(binding, address);
                iCGPIP2CommunicationsService Client = Factory.CreateChannel();
                Client.Test();
                
                return Client;
            }
            catch (Exception e)
            {
                Reporting.Say("Failed to connect to server. Going standalone?");
                Reporting.Say(e.ToString());
                Application.Exit();
                return null;
            }
        }


        public static bool DebuggerAttached = false;
        private static void Main(string[] args)
        {
            if (Parameters.Replay)
            {
                Parameters.ServerTestCasesCount = 500;
                
            }

            //Tests.Hog();
            //return;

            //Tests.RenderBlobs();
            //return;
            //Tests.Test();
            //return;
            //Tests.PenguinsNorm();
            //return;
            //Tests.TestNorm();
            //return;
            //Tests.BuildKNN();
            //return;



            //These are test cases that simon made. they are only temp. commented
            /*if (Array.IndexOf<string>(args, "RunFilterOnFolder") >= 0)
            {
                Tests.RunFilterOnFolder();
                return;
            }*/





            //Tests.TestFeatures();
            //return;
            //Tests.ProcessChimps();
            //return;
            //Tests.Test();
            //return;
            //DataConverter.Innervision();
            //return;
           // Thread.CurrentThread.Priority = ThreadPriority.BelowNormal;
            
            if (System.Diagnostics.Debugger.IsAttached)
            {
                DebuggerAttached = true;
                Go(args);
            }
            else
            {

                  try
                {
                    DebuggerAttached = false;
                    Go(args);
                }
                catch (Exception e)
                {
                       Reporting.Say(e.ToString());
                       Reporting.Say(e.StackTrace);
                       Reporting.Say(e.Source);
                      // Console.ReadLine();
                }
            }
        }


        public static void FindOptimalLayout(int Count, out int Rows, out int Cols)
        {
            Rows = (int)Math.Sqrt(Count);
            Cols = 0;
            while (Cols * Rows < Count)
                Cols++;

        }

        private static CGPIndividual Simplify(CGPIndividual Cand, FitnessFunction FitFunc, PopulationParameters PopParams)
        {
            CGPIndividual C2 = Cand.Clone();
            /*
            int MaxSimplificationAttempts = 1000;

            for (int SimplificationAttempt = 0; SimplificationAttempt < MaxSimplificationAttempts; SimplificationAttempt++)
            {
                Console.Write("..simplifying..");
                int g = FastRandom.Rng.Next(0, Cand.Genotypes.Count);
                int n = FastRandom.Rng.Next(0, Cand.Genotypes[g].Width);
                int Trials = 0;
                while (!Cand.Genotypes[g].Nodes[n].Evaluated)
                {
                    g = FastRandom.Rng.Next(0, Cand.Genotypes.Count);
                    n = FastRandom.Rng.Next(0, Cand.Genotypes[g].Width);
                    Trials++;
                    if (Trials > 1000)
                    {
                        SimplificationAttempt = MaxSimplificationAttempts + 1;
                        break;
                    }
                }

                CGPIndividual C3 = C2.Clone();
                C3.Fitness = null;
                int MutationType = FastRandom.Rng.Next(0, 5);


                switch (MutationType)
                {
                    case(0):
                        C3.Genotypes[g].Nodes[n].Function = CGPFunction.CONST;
                        C3.Genotypes[g].Nodes[n].GrayValue = 0;
                        break;
                    case (1):
                        C3.Genotypes[g].Nodes[n].Function = CGPFunction.CONST;
                        C3.Genotypes[g].Nodes[n].GrayValue = 255;
                        break;
                    case(2):
                        C3.Genotypes[g].Nodes[n].Function = CGPFunction.NOP;                        
                        break;
                    case (3):
                        C3.Genotypes[g].Nodes[n].Function = CGPFunction.NOP;
                        C3.Genotypes[g].Nodes[n].Connection0 = C3.Genotypes[g].Nodes[n].Connection1;
                        break;
                    case (4):
                        int Temp = C3.Genotypes[g].Nodes[n].Connection0;
                        C3.Genotypes[g].Nodes[n].Connection0 = C3.Genotypes[g].Nodes[n].Connection1;
                        C3.Genotypes[g].Nodes[n].Connection1 = Temp;
                        break;
                }

                FitnessFunctionResult Result = FitFunc.Test(C3, PopParams);
                C3.Fitness = Result;
                if (C3.Fitness.Error <= C2.Fitness.Error && C3.Fitness.TotalEvaluatedNodeCount < C2.Fitness.TotalEvaluatedNodeCount)
                {
                   
                    Console.WriteLine("Accepting mutation " + MutationType + " " + Result.Error + " vs " + C2.Fitness.Error + "  with pl " + Result.TotalEvaluatedNodeCount + " vs " + C2.Fitness.TotalEvaluatedNodeCount);
                    C2 = C3;
                }
                else
                {
                  //  Console.WriteLine("Rejected mutation " + MutationType + " " + Result.Error + " vs " + C2.Fitness.Error + "  with pl " + Result.TotalEvaluatedNodeCount + " vs " + C2.Fitness.TotalEvaluatedNodeCount);
                }

            }*/
            int MaxSimplificationAttempts = 1000;
            int SimplificationAttempt = 0;
           

            for (int g = 0; g < Cand.Genotypes.Count; g++)
            {
                
                for (int n = 0; n < Cand.Genotypes[g].Width; n++)
                {
                    if (!Cand.Genotypes[g].Nodes[n].Evaluated)
                        continue;
                    Console.Write("..simplifying..");
                    for (int MutationType = 0; MutationType <3; MutationType++)
                    {
                        CGPIndividual C3 = C2.Clone();
                        C3.Fitness = null;
                        //int MutationType = FastRandom.Rng.Next(0, 5);
                        SimplificationAttempt++;
                        int PreviousComplexity = 3;
                        if (C3.Genotypes[g].Nodes[n].Function == CGPFunction.CONST)
                            PreviousComplexity = 2;
                        else if (C3.Genotypes[g].Nodes[n].Function == CGPFunction.NOP)
                            PreviousComplexity = 1;
                        int ThisComplexity = 3;
                        switch (MutationType)
                        {
                            case (0):
                                C3.Genotypes[g].Nodes[n].Function = CGPFunction.CONST;
                                C3.Genotypes[g].Nodes[n].GrayValue = 0;
                                ThisComplexity = 2;
                                break;
                            case (1):
                                C3.Genotypes[g].Nodes[n].Function = CGPFunction.CONST;
                                C3.Genotypes[g].Nodes[n].GrayValue = 255;
                                ThisComplexity = 2;
                                break;
                            case (2):
                                C3.Genotypes[g].Nodes[n].Function = CGPFunction.NOP;
                                ThisComplexity = 1;
                                break;
                            case (3):
                                C3.Genotypes[g].Nodes[n].Function = CGPFunction.NOP;
                                C3.Genotypes[g].Nodes[n].Connection0 = C3.Genotypes[g].Nodes[n].Connection1;
                                ThisComplexity = 1;
                                break;
                            case (4):
                                int Temp = C3.Genotypes[g].Nodes[n].Connection0;
                                C3.Genotypes[g].Nodes[n].Connection0 = C3.Genotypes[g].Nodes[n].Connection1;
                                C3.Genotypes[g].Nodes[n].Connection1 = Temp;
                                ThisComplexity = PreviousComplexity;
                                break;
                        }

                        FitnessFunctionResult Result = FitFunc.Test(C3, PopParams);
                        C3.Fitness = Result;
                        if (C3.Fitness.Error < C2.Fitness.Error
                            ||
                            (C3.Fitness.TotalEvaluatedNodeCount < C2.Fitness.TotalEvaluatedNodeCount
                            &&
                            C3.Fitness.Error <= C2.Fitness.Error)
                            ||

                        (C3.Fitness.Error <= C2.Fitness.Error
                            &&
                            ThisComplexity < PreviousComplexity))
                        {

                            Console.WriteLine("\nAccepting mutation MT=" + MutationType + " F'=" + Result.Error + " vs F=" + C2.Fitness.Error + "  with LEN'=" + Result.TotalEvaluatedNodeCount + " vs LEN=" + C2.Fitness.TotalEvaluatedNodeCount + " COMP=" + PreviousComplexity+" vs "+ThisComplexity);
                            C2 = C3;
                            // g = 0; n = 0;
                        }

                        if (SimplificationAttempt > MaxSimplificationAttempts)
                            goto stopSimplifying;
                    }

                }
            }

            stopSimplifying:

            return C2;
        }

        private static void Go(string[] args)
        {
            string ClientID = Dns.GetHostName() + "_" + Process.GetCurrentProcess().Id;
            //
            bool IsServer = false;
            bool IsClient = false;
            bool Logging = false;
            bool Spawn = false;
            foreach (string s in args)
            {
                if (s == "server")
                    IsServer = true;
                else if (s == "client")
                    IsClient = true;
                else if (s == "viz")
                    Parameters.Visualize = true;
                else if (s == "consts")
                    Parameters.EvolveParametersOnly = true;
                else if (s == "spawn")
                    Spawn = true;
                else if (s == "log")
                {
                    Logging = true;
                }
            }

            Console.Title =
                "CGPIP2." +
                (IsServer ? "Server." : "Client.") +
                (Reporting.TextFile!=null ? "Log." : "")+
                (Parameters.Visualize ? "Viz." : "") +
                (Parameters.EvolveParametersOnly ? "Params." : "");

            //while (true) { }

            if (IsServer || !IsClient)
            {
                Reporting.Say("Setting up directories...");
                if (Directory.Exists(Parameters.TraceOutputFolder))
                    Directory.Delete(Parameters.TraceOutputFolder, true);
                if (Directory.Exists(Parameters.ServerOutputFolder))
                    Directory.Delete(Parameters.ServerOutputFolder, true);

                if (Directory.Exists(Parameters.PredictionsOutputFolder))
                    Directory.Delete(Parameters.PredictionsOutputFolder, true);

                Directory.CreateDirectory(Parameters.TraceOutputFolder);
                Directory.CreateDirectory(Parameters.ServerOutputFolder);
                Directory.CreateDirectory(Parameters.PredictionsOutputFolder);
                Reporting.Say("Setting up directories...done!");
            }

            if (Logging)
            {
                Reporting.LogToFile(Parameters.TraceOutputFolder, "client");
            }

            if (IsServer)
            {
                StartServer(Spawn);
                return;
            }

            if (Parameters.Replay)
                Thread.Sleep(1000);

            if (Parameters.Replay && !Parameters.Visualize)
                return;

            Reporting.Say("My ID is = " + ClientID);
            Thread.Sleep(FastRandom.Rng.Next(100,1000));
            FitnessFunction FitFunc = new FitnessFunction();


            CGPPopulation[] Populations = new CGPPopulation[Parameters.NumberOfPopulations];
            for (int i = 0; i < Parameters.NumberOfPopulations; i++)
            {
                Populations[i] = new CGPPopulation();

                if (Parameters.Replay)
                {
                    CGPIndividual FromStore = CGPIndividual.LoadFromXml(Parameters.ReplayFile);
                    FromStore.Evaluated = false;
                    Populations[i].Add(FromStore);
                }
                else
                {
                    for (int j = 0; j < Populations[i].Parameters.PopulationSize; j++)
                    {
                        CGPIndividual Ind = CGPIndividual.RandomIndividual(Populations[i].Parameters);
                        Populations[i].Add(Ind);
                    }
                }
                
            }

            iCGPIP2CommunicationsService Client = IsClient ? StartClient() : null;

            if (Client != null)
            {
                Parameters.MaxFilesToToTest = Client.TestCasesCount();
                Parameters.EvaluationMask = Client.GetEvaluationMask(Parameters.MaxFilesToToTest, Parameters.MaxFilesToLoad);
                CGPIndividual ServerInd = Client.GetIndividual(out Parameters.EvaluationMask);
                if (ServerInd != null)
                {
                    for (int i = 0; i < Parameters.NumberOfPopulations; i++)
                        Populations[i].Add(ServerInd.Clone());
                    for (int i = 0; i < Parameters.NumberOfPopulations; i++)
                        Populations[i].ResetFitnesses();
                }  
            }

          

            if (Parameters.Visualize)
            {
                //int Cols = FitnessFunction.TestSet.TestCases.Count / 4;// (int)Math.Sqrt(FitnessFunction.TestSet.TestCases.Count);
                //int Rows = FitnessFunction.TestSet.TestCases.Count / Cols;
                int Cols = 1440 / Parameters.VisualizationSize;
                int Rows = FitnessFunction.TestSet.TestCases.Count / Cols;
                if (Rows > Cols)
                {
                    int Temp = Rows;
                    Rows = Cols;
                    Cols = Temp;
                }
                while (Rows * Cols < FitnessFunction.TestSet.TestCases.Count)
                    Rows++;

                FindOptimalLayout(FitnessFunction.TestSet.TestCases.Count, out Rows, out Cols);

                ImageMatrix.ImageMatrixForm = new ImageMatrix(Rows, Cols, Parameters.VisualizationSize, Parameters.VisualizationSize, FitnessFunction.TestSet.TestCases.Count);
                ImageMatrix.ImageMatrixForm.Show();
            }


            

           

            double BestFitness = double.MaxValue;
            ulong TotalEvaluationsAllClients = 0;
            DateTime StartTime = DateTime.Now;

            if (Client != null)
            {
                Parameters.EvaluationMask = Client.GetEvaluationMask(Parameters.MaxFilesToToTest, Parameters.MaxFilesToLoad);
            }
            System.Diagnostics.PerformanceCounter ramCounter = new System.Diagnostics.PerformanceCounter("Memory", "Available MBytes"); 

            for (int Epoch = 0; Epoch < 10000000; Epoch++)
            {
                if (ramCounter.NextValue() < 1024)
                {
                    Reporting.Say("Not much memory left! " + ramCounter.NextValue() + "Mb");
                    System.GC.Collect();
                    Application.DoEvents();
                    Thread.Sleep(1000);
                    continue;
                }

                if (Client != null && Epoch%100==0)
                {
                //    Client = StartClient();
                }
              
                foreach (CGPPopulation Pop in Populations)
                {
                    TestPopulation(FitFunc, Pop);
                    Pop.Sort();
                    if (Epoch == 0)
                        Pop.MarkLearningRatePoint();
                }

                FitnessFunction.TestSet.CheckCheckSum();
                PopulationParameters BestIndPopParams;
                CGPIndividual BestInd = BestIndividual(Populations, out BestIndPopParams);

                if (BestInd.Fitness.Error != BestFitness)
                    Parameters.LastFitnessImprovement = BestInd.EvaluationIndex;

                if (Parameters.OptimizationThreshold>0 && BestInd.Fitness.Error < Parameters.OptimizationThreshold && Parameters.Visualize && Epoch % 10 == 0)
                {
                    CGPIndividual SimplifiedCand = Simplify(BestInd, FitFunc, BestIndPopParams);
                    if (SimplifiedCand.Fitness.Error <= BestInd.Fitness.Error)
                    {
                        Populations[0].Add(SimplifiedCand);
                        BestInd = SimplifiedCand;
                    }
                }
                
                if (BestInd.Fitness.Error != BestFitness || Epoch % 50 == 0)
                {

                    Reporting.Say("old Best: " + BestFitness + "\nNew Best: " + BestInd.Fitness.Error);

                    BestFitness = BestInd.Fitness.Error;
                    Reporting.Say(  "----------New Evaluation----------" + 
                                    "\nEPOCH: \t\t\t" + Epoch + 
                                    "\nTHIS EVAL COUNT: \t" + FitnessFunction.EvaluationCount + 
                                    "\nTOTAL EVAL COUNT: \t" + TotalEvaluationsAllClients + 
                                    "\nGeno x eval:\t\t" + ((int)FitnessFunction.EvaluationCount * BestInd.Genotypes.Count) + 
                                    "\nImage Evaluation \t\t" + FitnessFunction.ImageEvaluationCount + 
                                    "\nTiming\t\t\t" + CGPGraphRunner.ExecutionTimer.ElapsedMilliseconds
                                    
                                    );
                    /*
                     Reporting.Say("EPOCH:" + Epoch + "\t" + FitnessFunction.EvaluationCount + "\t" +
                        TotalEvaluationsAllClients +"\t"+
                        ((int)FitnessFunction.EvaluationCount * BestInd.Genotypes.Count ) + "\t" + FitnessFunction.ImageEvaluationCount);
                    Reporting.Say("\tTiming\t" +
                        CGPGraphRunner.ExecutionTimer.ElapsedMilliseconds                        
                        );
                     * Reporting.Say("Timing\t\t" +
                        CGPGraphRunner.ExecutionTimer.ElapsedMilliseconds                        
                        );*/
                    foreach (CGPPopulation Pop in Populations)
                        Reporting.Say(  "\neval index: \t\t" + Pop[0].EvaluationIndex + 
                                        "\nfitness error \t\t" + Pop[0].Fitness.Error + 
                                        "\nLearning Rate \t\t" + Pop.LearningRate() + 
                                        "\nMutation Rate: \t\t" + Pop.Parameters.MutationRate + 
                                        "," + Pop.Parameters.PopulationSize +
                                        "\nBest\t\t\t" + BestInd.Fitness.Error);
                    //Reporting.Say("Best\t\t" + BestInd.Fitness.Error);

                    if (Parameters.Visualize || Parameters.Replay)
                    {
                        BestFitness = BestInd.Fitness.Error;
                        Parameters.ShowTrace = true;
                        Parameters.ProcessValidation = true;
                        BestInd.Evaluated = false;
                        if (Parameters.FitnessIsClassification)
                            FitFunc.Test(BestInd, BestIndPopParams);
                        else
                            FitFunc.TestMap(BestInd, BestIndPopParams);

                      

                        Parameters.ShowTrace = false;
                        Parameters.ProcessValidation = false;
/*
                        CGPIndividual Simplified = Simplify(BestInd, FitFunc, BestIndPopParams);

                        File.Delete("simplified.xml");
                        if (Simplified.Fitness.TotalEvaluatedNodeCount < BestInd.Fitness.TotalEvaluatedNodeCount)
                        {
                            Reporting.Say("Simplified\t" +
                                Simplified.Fitness.TotalEvaluatedNodeCount + "\t" + BestInd.Fitness.TotalEvaluatedNodeCount);
                            CGPIndividual.SaveToXml(Simplified, "simplified.xml");
                        }*/

                        if (ImageMatrix.ImageMatrixForm!=null)
                        ImageMatrix.ImageMatrixForm.UpdateDisplayCb();
                    }
                    if (Parameters.Visualize || Parameters.Replay)
                    {
                        TimeSpan Elapsed = DateTime.Now.Subtract(StartTime);
                        ImageMatrix.ImageMatrixForm.SetCaption(String.Format("Evaluations:{0,10} Time:{1:000.00} mins Fitness:{2:0.0000} Nodes:{3,000}", TotalEvaluationsAllClients, Elapsed.TotalMinutes, BestFitness, BestInd.Fitness.TotalEvaluatedNodeCount));
                        ImageMatrix.ImageMatrixForm.Buffer.Save(String.Format("{1}/{0:000000000000}.png", Epoch, Parameters.TraceOutputFolder));
                        ImageMatrix.ImageMatrixForm.UpdateDisplayCb();

                        
                    }

                    if (Client != null )
                    {
                        try
                        {
                            int Temp = Client.TestCasesCount();

                        }
                        catch (Exception e)
                        {
                            Client = StartClient();
                            

                        }

                         double ServerBestFitness = Client.GetError();

                         if (!double.IsNaN(ServerBestFitness) || ServerBestFitness > BestFitness)
                         {

                             if (Client.SendIndividual(BestInd, Parameters.MaxFilesToToTest, Parameters.MaxFilesToLoad, Parameters.EvaluationMask))
                             {
                                 Reporting.Say("Sending individual to server. Reason: " + ServerBestFitness + " is worse than " + BestInd.Fitness.Error);
                             }
                         }
                    }
                }

                if (Epoch % Parameters.LocalSyncInterval == 0 && FitnessAtLastLocalSynch != BestInd.Fitness.Error)
                {
                    FitnessAtLastLocalSynch = BestInd.Fitness.Error;
                    
                        for (int i = 0; i < Parameters.NumberOfPopulations;i++ )
                            if (FastRandom.Rng.NextDouble() < 0.01+(double)(i+1) / Parameters.NumberOfPopulations)
                            Populations[i].Add(BestInd);
                }

                bool ResetFitnesses = false;

                if (Client!=null && Epoch % Parameters.RemoteSyncInterval == 0 )
                {

                    try
                    {
                        int Temp = Client.TestCasesCount();

                    }
                    catch (Exception e)
                    {
                        //Client = StartClient();
                        throw new Exception("Connection lost?");
                    }
                    
                    TotalEvaluationsAllClients = Client.ReportEvaluations(ClientID, FitnessFunction.EvaluationCount);


                    if (Parameters.MaxFilesToToTest < Parameters.MaxFilesToLoad)
                    {
                        int ServerTestCasesCount = Client.TestCasesCount();
                        
                        if (ServerTestCasesCount != Parameters.MaxFilesToToTest)
                        {
                            Parameters.EvaluationMask = Client.GetEvaluationMask(Parameters.MaxFilesToToTest, Parameters.MaxFilesToLoad);
                            foreach (CGPPopulation Pop in Populations)
                                Pop.ResetFitnesses();
                            Parameters.MaxFilesToToTest = ServerTestCasesCount;
                            Reporting.Say("Number of files to test has changed! Now:" + Parameters.MaxFilesToToTest);
                            ResetFitnesses = true;
                        }
                    }
                    
                    if (!ResetFitnesses)
                    {
                        FitnessAtLastRemoteSynch = BestInd.Fitness.Error;
                        double ServerBestFitness = Client.GetError();

                        if (!double.IsNaN(ServerBestFitness))
                        {
                            if (ServerBestFitness < BestInd.Fitness.Error)
                            {
                                CGPIndividual ServerInd = Client.GetIndividual(out Parameters.EvaluationMask);

                                Reporting.Say("Accepting server individual. Reason: " + ServerInd.Fitness.Error + " is better than " + BestInd.Fitness.Error);
                                /*for (int i = 0; i < Parameters.NumberOfPopulations; i++)
                                    if (FastRandom.Rng.NextDouble() < (double)(i + 1) / Parameters.NumberOfPopulations)
                                    {
                                        Populations[i].Add(ServerInd.Clone());
                                        Populations[i].MarkLearningRatePoint();
                                    }
                                 */
                                for (int i = 0; i < Parameters.NumberOfPopulations; i++)
                                    //if (FastRandom.Rng.NextDouble() < (double)(i + 1) / Parameters.NumberOfPopulations)
                                    {
                                       // Populations[i].Clear();
                                        CGPIndividual Ind = ServerInd.Clone();
                                   //     Ind.Evaluated = false;
                                        Populations[i].Add(Ind);
                                        Populations[i].MarkLearningRatePoint();
                                        
                                    }
                            }
                            else
                            {
                                if (Client.SendIndividual(BestInd, Parameters.MaxFilesToToTest, Parameters.MaxFilesToLoad, Parameters.EvaluationMask))
                                {
                                    Reporting.Say("Sending individual to server. Reason: " + ServerBestFitness + " is worse than " + BestInd.Fitness.Error);
                                }
                            }
                        }
                        else
                        {
                            if (Client.SendIndividual(BestInd, Parameters.MaxFilesToToTest, Parameters.MaxFilesToLoad, Parameters.EvaluationMask))
                                Reporting.Say("Sending individual to server. Reason: It appears to have nothing");
                        }
                    }
                }

                if (Parameters.EvolveParameters && Epoch % Parameters.ParametersEvolveInterval == 0)
                {
                    PopulationParameters BestPopulationParameters = Populations[0].Parameters;
                    double BestLearningRate = Populations[0].LearningRate();
                    foreach (CGPPopulation Pop in Populations)
                    {
                        if (Pop.LearningRate() > BestLearningRate)
                        {
                            BestLearningRate = Pop.LearningRate();
                            BestPopulationParameters = Pop.Parameters;
                        }
                    }

                    foreach (CGPPopulation Pop in Populations)
                    {
                        if (FastRandom.Rng.NextBool())
                        {
                            Pop.Parameters = BestPopulationParameters.Clone();
                        }
                        else
                        {
                            Pop.Parameters = BestPopulationParameters.Clone();
                            Pop.Parameters.Mutate();
                        }
                        Pop.MarkLearningRatePoint();
                    }
                }

                //Parameters.EvolveParametersOnly = Epoch % 2 == 0;

                if (!ResetFitnesses)
                foreach (CGPPopulation Pop in Populations)
                    Pop.MakeNext(Pop.Parameters.PopulationSize);

                /*
                                 if (!ResetFitnesses){
                                    foreach (CGPPopulation Pop in Populations)
                                    {
                                        Pop.MakeNext(Pop.Parameters.PopulationSize);
                                    }
                                }
                 * */
                /*
                if (Epoch>0 && Epoch % 1000 == 0)
                {

                    foreach (CGPPopulation Pop in Populations)
                    {
                        for (int ii = 0; ii < Pop.Count;ii++ )
                        {
                            CGPIndividual Ind = Pop[ii];
                            bool Grow = true;// FastRandom.Rng.NextBool();
                            for (int g = 0; g < Ind.Genotypes.Count; g++)
                            {
                               
                                 if (!Grow && Ind.Genotypes[g].Width > 20)
                                {
                                    Ind.Genotypes[g].Resize(Ind.Genotypes[g].Width - 10);
                                }
                                 else if (Ind.Genotypes[g].Width < 1000)
                                 {
                                     Ind.Genotypes[g].Resize(Ind.Genotypes[g].Width + 10);
                                 }
                            }
                            Ind.Dirty = true;
                        }
                    }
                }
                */
                if (Client==null && BestFitness < 0.4 && Parameters.MaxFilesToToTest<Parameters.MaxFilesToLoad)
                {
                    foreach (CGPPopulation Pop in Populations)
                        Pop.ResetFitnesses();
                    Parameters.MaxFilesToToTest++;
                }
                Application.DoEvents();

                if (Parameters.Replay)
                    return;
            }
        }

    }
}