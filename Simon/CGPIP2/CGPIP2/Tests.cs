﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.Structure;
using System.IO;
using Emgu.CV.Features2D;

namespace CGPIP2
{
    class Tests
    {

        public static CGPIndividual PickBestIndividualInFolder(string Folder)
        {
            CGPIndividual Best = null;
            string[] XmlFiles = Directory.GetFiles(Folder, "*.xml");
            foreach (string Xml in XmlFiles)
            {
                CGPIndividual Ind = CGPIndividual.LoadFromXml(Xml);
                if (Best == null || Ind.Fitness.Error < Best.Fitness.Error)
                    Best = Ind;
            }
            return Best;
        }

        public static void RunFilterOnFolder()
        {
            string[] IndFileNames = Directory.GetFiles("C:\\work\\csharp\\CGPIP2\\CGPIP2\\bin\\x64\\Release\\penguincounting5\\Server\\", "*.xml");

            foreach(string IndFileName in IndFileNames)
            {
                CGPIndividual Ind = CGPIndividual.LoadFromXml(IndFileName);

                //CGPIndividual.LoadFromXml("C:\\work\\csharp\\CGPIP2\\CGPIP2\\bin\\x64\\Release\\puffins3\\Server\\ServerIndividual_puffins3_0007.xml");//"PenguinSpotsServerIndividual.xml");


            string InputFolder = "C:\\work\\penguincounting\\";
            string OutputFolder = InputFolder+"\\verification5_"+Path.GetFileNameWithoutExtension(IndFileName)+"\\";
            //if (Directory.Exists(OutputFolder))
                //Directory.Delete(OutputFolder, true);
            if (!Directory.Exists(OutputFolder))
            Directory.CreateDirectory(OutputFolder);

            string[] Files = Directory.GetFiles(InputFolder, "*.jpg");

            Emgu.CV.VideoWriter Video = null;//new VideoWriter(OutputFolder+"//video2.avi", 10, 640, 480, true);

            foreach (float BrightnessAdjument in Parameters.InputImageBrightnesses)
            foreach (string F in Files)
            {
                Console.WriteLine("Loading " + F);
                CGPImageTestCase TestCase = null;//


                try
                {
                    TestCase = new CGPImageTestCase(
                     F,
                     F,
                 Parameters.ScaleInputs,
                 Parameters.SplitHSB,
                 Parameters.SplitBGR,
                 Parameters.FilterTargetsForRed,
                 Parameters.FilterTargetsForBlue,
                 Parameters.InvertTargetClasses, BrightnessAdjument
                     );
                }
                catch (Exception e)
                {

                }
                if (TestCase == null)
                    continue;

                CGPGraphRunner[] Runners = new CGPGraphRunner[Ind.Genotypes.Count];
                for (int g = 0; g < Ind.Genotypes.Count; g++)
                {
                    int EvaluatedNodeCount;
                    Runners[g] = new CGPGraphRunner();
                    Runners[g].FindNodesToProcess(Ind.Genotypes[g], Parameters.OutputCount, out EvaluatedNodeCount);
                }

                for (int g = 0; g < Ind.Genotypes.Count; g++)
                {
                    Ind.Thresholds[g] = 128;
                    if (g == 0)
                        Ind.MixModes[g] = 4;
                }


                Parameters.DefaultImageHeight = TestCase.Target.Height;
                Parameters.DefaultImageWidth = TestCase.Target.Width;
                Parameters.InputCount = TestCase.Inputs.Count;


                List<CGPImage> Outputs = new List<CGPImage>();
                List<CGPImage> ThresholdedOutputs = new List<CGPImage>();


                for (int g = 0; g < Ind.Genotypes.Count; g++)
                {
                    List<CGPImage> Inputs = new List<CGPImage>();

                    Inputs.AddRange(TestCase.Inputs);

                    if (Outputs.Count > 0)
                    {
                        Inputs.AddRange(Outputs);
                        Inputs.AddRange(ThresholdedOutputs);
                    }


                    Parameters.InputCount = Inputs.Count;
                    Runners[g].Run(Ind.Genotypes[g], Inputs.ToArray(), 0, g.ToString());

                    Runners[g].CleanUp();

                    CGPImage Output = Runners[g].GetOutput();
                    if (Parameters.MedianFilterOutput > 0)
                        Output = CGPImageFunctions.SmoothMedian(Output, Parameters.MedianFilterOutput);
                    //                    if (Parameters.ShowTrace && Parameters.Visualize)
                    CGPImage ThresholdedOutput = CGPImageFunctions.Threshold(Output, Ind.Thresholds[g]);

                    if (ThresholdedOutput.Width != TestCase.Target.Width || ThresholdedOutput.Height != TestCase.Target.Height)
                        ThresholdedOutput = CGPImageFunctions.Rescale(ThresholdedOutput, TestCase.Target.Width, TestCase.Target.Height, Emgu.CV.CvEnum.INTER.CV_INTER_NN);


                    ThresholdedOutputs.Add(ThresholdedOutput);

                    if (Output.Width != TestCase.Target.Width || Output.Height != TestCase.Target.Height)
                        Output = CGPImageFunctions.Rescale(Output, TestCase.Target.Width, TestCase.Target.Height);

                    Outputs.Add(Output);
                    Output = CGPImageFunctions.Erode(Output, 2);
                }


                bool[,] CombinedOutput = new bool[TestCase.Target.Height, TestCase.Target.Width];


                for (int x = 0; x < TestCase.Target.Width; x++)
                    for (int y = 0; y < TestCase.Target.Height; y++)
                        for (int g = 0; g < Ind.Genotypes.Count; g++)
                        {
                            float v = Outputs[g].Img.Data[y, x, 0];
                            if (float.IsNaN(v) || float.IsInfinity(v))
                                v = 0;

                            bool SingleOutput = v > Ind.Thresholds[g];
                            if (g == 0)
                                Ind.MixModes[g] = 4;
                            switch (Ind.MixModes[g])
                            {
                                case (0):
                                    CombinedOutput[y, x] = CombinedOutput[y, x] | SingleOutput;
                                    break;
                                case (1):
                                    CombinedOutput[y, x] = CombinedOutput[y, x] & SingleOutput;
                                    break;
                                case (2):
                                    CombinedOutput[y, x] = CombinedOutput[y, x] ^ SingleOutput;
                                    break;
                                case (3):
                                    CombinedOutput[y, x] = !(CombinedOutput[y, x] & SingleOutput);
                                    break;
                                case (4):
                                    CombinedOutput[y, x] = SingleOutput;
                                    break;
                            }

                        }





                double MCC = 1;
                //CGPImage ThresholdedOuput = MCC >= 0.00001 ? CGPImageFunctions.Threshold(Output, Ind.Threshold[0]) : CGPImageFunctions.ThresholdInv(Output, Ind.Threshold[0]); ;
                Image<Gray, byte> BinaryOutput = new Image<Gray, byte>(TestCase.Inputs[0].Img.Width, TestCase.Inputs[0].Img.Height);
                Image<Bgr, float> ThresholdedOuput = TestCase.Inputs[0].Img.Convert<Bgr, float>();
                if (ThresholdedOuput.Width != TestCase.Target.Width || ThresholdedOuput.Height != TestCase.Target.Height)
                    ThresholdedOuput = ThresholdedOuput.Resize(TestCase.Target.Width, TestCase.Target.Height, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                if (BinaryOutput.Width != TestCase.Target.Width || BinaryOutput.Height != TestCase.Target.Height)
                    BinaryOutput = BinaryOutput.Resize(TestCase.Target.Width, TestCase.Target.Height, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);



              for (int x = 0; x < ThresholdedOuput.Width; x++)
                    for (int y = 0; y < ThresholdedOuput.Height; y++)
                    {
                        float OriginalV = TestCase.Inputs[0].Img.Data[y, x, 0];

                        bool Predicted = CombinedOutput[y, x];

                        //Predicted = !Predicted;

                        if (Predicted)
                            BinaryOutput.Data[y, x, 0] = 255;

                        if (Predicted)
                        {
                            ThresholdedOuput.Data[y, x, 0] = OriginalV;
                            ThresholdedOuput.Data[y, x, 1] = OriginalV ;
                            ThresholdedOuput.Data[y, x, 2] = 255;
                        }
                    }
                
            //    Video.WriteFrame(ThresholdedOuput.Convert<Bgr, byte>());
              
                List<Rectangle> Blobs = CGPImageFunctions.Blobs(BinaryOutput, 5, false,100, 1);
                int BoxSizeMod = 0;
                foreach (Rectangle R in Blobs)
                {
                    //if (R.Width < 200 && R.Height < 200)
                    {
                        Rectangle R2 = new Rectangle(R.X - BoxSizeMod, R.Y - BoxSizeMod, R.Width + (BoxSizeMod * 2), R.Height + (BoxSizeMod * 2));
                        ThresholdedOuput.Draw(R2, new Bgr(Color.Blue), 2);
                    }
                }

                ThresholdedOuput.Save(OutputFolder + "//" + Path.GetFileNameWithoutExtension(TestCase.InputFileName)+".png");
            }
            }
        }

        public static void RunSpotFilterOnFolder()
        {
          
            string InputFolder = "C:\\work\\Bristol\\TrainingImagesForCGP\\Processed6_bodyonly\\detections\\";
            string OutputFolder = "C:\\work\\Bristol\\SpotDetection\\Results_spotsTEMP\\";
       //     if (Directory.Exists(OutputFolder))
         //       Directory.Delete(OutputFolder, true);
            Directory.CreateDirectory(OutputFolder);

            string[] Files = Directory.GetFiles(InputFolder, "*.png");

            foreach (string F in Files)
            {
                Console.WriteLine("Loading " + F);
                Image<Bgr, float> _Src = new Image<Bgr, float>(F);
                double[] Widths = new double[] { 150,200,250};

                foreach (double Width in Widths)
                {
                    double Scale = Width / (float)_Src.Width;
                  //  if (Scale > 1)
                   //     continue;
                    _Src = _Src.Resize(Scale, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);


                    Image<Bgr, float> ColorOutput = _Src.Clone();

                    Image<Gray, float>[] HsvComponents = _Src.Convert<Hsv, float>().Split();
                    CGPImage Gray = new CGPImage(_Src.Convert<Gray, float>());
                    CGPImage SrcH = new CGPImage(HsvComponents[0]);
                    CGPImage SrcS = new CGPImage(HsvComponents[1]);
                    CGPImage SrcV = new CGPImage(HsvComponents[2]);
                    SrcS.Img._Mul(255);
                    int WindowRadius = (21 - 1) / 2;
                    int StepSize = 2;
                    
                    for (int x = WindowRadius; x < _Src.Width - WindowRadius; x += StepSize)
                        for (int y = WindowRadius; y < _Src.Height - WindowRadius; y += StepSize)
                        {
                            CGPImage SROI = CGPImageFunctions.Extract(SrcS,
                                new Rectangle(x - WindowRadius, y - WindowRadius, 1 + (WindowRadius * 2), 1 + (WindowRadius * 2)));
                            CGPImage HROI = CGPImageFunctions.Extract(SrcH,
                              new Rectangle(x - WindowRadius, y - WindowRadius, 1 + (WindowRadius * 2), 1 + (WindowRadius * 2)));
                            CGPImage VROI = CGPImageFunctions.Extract(SrcV,
                              new Rectangle(x - WindowRadius, y - WindowRadius, 1 + (WindowRadius * 2), 1 + (WindowRadius * 2)));
                            CGPImage GROI = CGPImageFunctions.Extract(Gray,
                             new Rectangle(x - WindowRadius, y - WindowRadius, 1 + (WindowRadius * 2), 1 + (WindowRadius * 2)));
                            
                            /* //ROI = CGPImageFunctions.LocalNormalize(ROI, WindowRadius);
                             CGPImage step0 = CGPImageFunctions.Gradient(ROI, 7, 7, 10);
                             CGPImage step1 = CGPImageFunctions.Threshold(step0, 128);
                             bool Predicted =
                                 step1.Img.Data[step1.Height / 2, step1.Width / 2, 0] > 128;
                             if (Predicted)
                             {
                                 ColorOutput.Data[y, x, 2] += 255;
                             }*/

                            CGPImage[][] Inputs = new CGPImage[1][];
                            Inputs[0] = new CGPImage[4];
                            Inputs[0][0] = GROI;
                            Inputs[0][1] = HROI;
                            Inputs[0][2] = SROI;
                            Inputs[0][3] = VROI;

                            CGPImage Node_0_003 = CGPImageFunctions.Sub(Inputs[0][03], 164.0277f);
                            CGPImage Node_0_005 = CGPImageFunctions.IF2(Inputs[0][01], 34.97131f, Inputs[0][02], Node_0_003);
                            CGPImage Node_0_010 = CGPImageFunctions.IF(Node_0_003, -192.5642f, Node_0_005, Inputs[0][02]); 
                            CGPImage Node_0_049 = CGPImageFunctions.Close(Node_0_010, 22, 22, 10);
                            
                            CGPImage step2 = Node_0_049;
                            
                            bool Predicted =
                                step2.Img.Data[step2.Height / 2, step2.Width / 2, 0] > 128;

                            if (Predicted)
                            {
                                ColorOutput.Data[y, x, 2] += 255;
                            }
                        }

                                   
                                    /*CGPImage step0 = CGPImageFunctions.Gradient(SrcS, 23, 23, 10);
                                    CGPImage step1 = CGPImageFunctions.Threshold(step0, 128);
                                    for(int x=0;x<_Src.Width;x++)
                                        for (int y = 0; y < _Src.Height; y++)
                                        {
                                            if (step1.Img.Data[y, x, 0] > 128)
                                                ColorOutput.Data[y, x, 2] = 255;
                                        }*/
                 /*   CGPImage step0 = CGPImageFunctions.SmoothBilatral(SrcS, 126, 16);
                    CGPImage step1 = CGPImageFunctions.Close(step0, 6, 6, 10);
                    CGPImage step2 = CGPImageFunctions.Min(step1, 226.6232f);
                    
                    for (int x = 0; x < _Src.Width; x++)
                        for (int y = 0; y < _Src.Height; y++)
                        {
                            if (step2.Img.Data[y, x, 0] > 77)
                                ColorOutput.Data[y, x, 2] = 255;
                        }*/
                    string OutputFileName = OutputFolder + "/" + Path.GetFileNameWithoutExtension(F)+"_"+Width+".png";
                    ColorOutput.Save(OutputFileName);
                }
            }
        }


        public static void TestFeatures()
        {
            Parameters.Rescales = new double[] { 0.1d };
            Parameters.FitnessAggregationType = Parameters.FitnessAggregation.AVERAGE;
            Parameters.MaxFilesToToTest = 1;
            Parameters.MaxFilesToLoad = 256;
            Parameters.MaxImageDimension = 512;
            Parameters.IncreaseThreshold = 0.5;
            Parameters.ImageBorder = 1;
            CGPImageTestSet TestSet = new CGPImageTestSet(
                //  "C:\\work\\RealSteel\\binROI\\train\\cra\\input\\",
                // "C:\\work\\RealSteel\\binROI\\train\\cra\\target\\",
          "C:\\work\\csharp\\CGPIP2DataSets\\silver6\\Inputs\\",
          "C:\\work\\csharp\\CGPIP2DataSets\\silver6\\Targets\\",
           "*.png",
           true,
           true,
           true,
           true,
           false,
           false);

            string OutputFolder = "./testfeatures/";
            if (Directory.Exists(OutputFolder))
                Directory.Delete(OutputFolder, true);
            
            Directory.CreateDirectory(OutputFolder);
            int ImgIndex = 0;
            foreach (CGPImageTestCase tc in TestSet.TestCases)
            {
                for (int f = 0; f < 6; f++)
                {
                    CGPImage Output = CGPImageFunctions.Feature(tc.Inputs[0], f, false, false, 2, 0);
                    tc.Inputs[0].Save(String.Format("{0}/{1:000}_{2:00}_in.png", OutputFolder, ImgIndex, f));
                    Output.Save(String.Format("{0}/{1:000}_{2:00}_ot.png", OutputFolder, ImgIndex, f));
                    Console.WriteLine(String.Format("{0}/{1:000}_{2:00}_ot.png", OutputFolder, ImgIndex, f));
                }

                    ImgIndex++;
            }

            CGPFunction[] Funcs = new CGPFunction[] { 
                   CGPFunction.LMIN,
    CGPFunction.LMAX,
    CGPFunction.LAVERAGE,
    CGPFunction.LRANGE,
    CGPFunction.LCMPMIN,
    CGPFunction.LCMPMAX,
    CGPFunction.LCMPAVERAGE,
    CGPFunction.LCMPRANGE
            };

            CGPIP2.CGPImageFunctions.LocalStat[] S = new CGPIP2.CGPImageFunctions.LocalStat[] { CGPIP2.CGPImageFunctions.LocalStat.Min, CGPIP2.CGPImageFunctions.LocalStat.Max, CGPIP2.CGPImageFunctions.LocalStat.Average, CGPIP2.CGPImageFunctions.LocalStat.Range };

            foreach (CGPImageTestCase tc in TestSet.TestCases)
            {
                foreach (CGPIP2.CGPImageFunctions.LocalStat s in S)
                {
                    CGPImage Output = Output = CGPImageFunctions.LocalStats(tc.Inputs[0], 2, 2, s);
                    tc.Inputs[0].Save(String.Format("{0}/{1:000}_{2}_in.png", OutputFolder, ImgIndex, s.ToString()));
                    Output.Save(String.Format("{0}/{1:000}_{2}_ot.png", OutputFolder, ImgIndex, s.ToString()));
                    Console.WriteLine(String.Format("{0}/{1:000}_{2}_ot.png", OutputFolder, ImgIndex, s.ToString()));
                }

                ImgIndex++;
            }


        }

        public static string ProcessChimpTarget(Image<Bgr, byte> SrcImg,string ImageFile, string XMLFile, string OutputFolder)
        {
            string BaseImageFileName = Path.GetFileNameWithoutExtension(ImageFile);
            int tx = 0;
            int ty = 0;
            int bx = 0;
            int by = 0;

            Image<Gray, byte> TargetImg =  new Image<Gray, byte>(SrcImg.Width, SrcImg.Height);

            string DescFile = XMLFile;
            string[] Lines = File.ReadAllLines(DescFile);

            string temp;
            string tok;
            string EndTok;
            int start;
            string name = "";

            foreach (string Line in Lines)
            {

                if (Line.Contains("<name>"))
                {
                    tok = "<name>";
                    start = Line.IndexOf(tok) + tok.Length;
                    name = Line.Substring(start, Line.IndexOf("</") - start);
                    
                }
                if (Line.Contains("<xmin>"))
                {
                    tok = "<xmin>";
                    start = Line.IndexOf(tok)+tok.Length;
                    temp = Line.Substring(start,Line.IndexOf("</") -start);
                    tx = (int)float.Parse(temp);
                }
                if (Line.Contains("<xmax>"))
                {
                    tok = "<xmax>";
                    start = Line.IndexOf(tok) + tok.Length;
                    temp = Line.Substring(start, Line.IndexOf("</") - start);
                    bx = (int)float.Parse(temp);
                }
                if (Line.Contains("<ymin>"))
                {
                    tok = "<ymin>";
                    start = Line.IndexOf(tok) + tok.Length;
                    temp = Line.Substring(start, Line.IndexOf("</") - start);
                    ty = (int)float.Parse(temp);
                }
                if (Line.Contains("<ymax>"))
                {
                    tok = "<ymax>";
                    start = Line.IndexOf(tok) + tok.Length;
                    temp = Line.Substring(start, Line.IndexOf("</") - start);
                    by = (int)float.Parse(temp);

                    if (name == "chimp")
                    {
                        int w = bx - tx;
                        int h = by - ty;
                        int dx = (int)(0.25 * w);
                        int dy = (int)(0.25 * h);
                     //   tx += dx; ty += dy;
                     //   w -= (2 * dx); h -= (2 * dy);
                        TargetImg.Draw(new Rectangle(tx, ty, bx - tx, by - ty), new Gray(255), -1);
                    }
                }
            }

           
            TargetImg.Save(OutputFolder + "/" + BaseImageFileName + "_target.png");
            return OutputFolder + "/" + BaseImageFileName + "_target.png";
        }

        public static List<string> ProcessChimpDetectors(Image<Bgr, byte> SrcImg, string ImageFile, string DescFile, string OutputFolder)
        {
            string BaseImageFileName = Path.GetFileNameWithoutExtension(ImageFile);
            List<string> OutputFiles = new List<string>();

         
            string[] Lines = File.ReadAllLines(DescFile);
            int DetectorIndex = 0;

            Image<Gray, float> OutputImg = new Image<Gray, float>(SrcImg.Width, SrcImg.Height);
            for (int i = Lines.Length - 2; i >= 0; i--)
            {
                if (Lines[i].Trim() != "")
                {
                    string[] Toks = Lines[i].Split(' ');
                    if (Toks.Length == 5)
                    {
                        int tx = (int)float.Parse(Toks[0]);
                        int ty = (int)float.Parse(Toks[1]);
                        int bx = (int)float.Parse(Toks[2]);
                        int by = (int)float.Parse(Toks[3]);
                        float conf = float.Parse(Toks[4]);

                        int w = bx - tx;
                        int h = by - ty;

                        int dx = (int)(0.1 * w);
                        int dy = (int)(0.1 * h);
                      //  tx += dx; ty += dy;
                      //  w -= (2 * dx); h -= (2 * dy);

                        OutputImg.Draw(new Rectangle(tx, ty, bx - tx, by - ty), new Gray(255d * conf), -1);
                    }
                }
                if (i == 0 || Lines[i].Trim() == "")
                {
                    string OutputFileName = OutputFolder + "/" + BaseImageFileName + "_detector" + DetectorIndex + ".png";
                    for(int o=0;o<10;o++)
                        OutputImg._SmoothGaussian(7);
                    //OutputImg._EqualizeHist();
                    double[] MinValues;
                    double[] MaxValues;
                    Point[] minLocations;
                    Point[] MaxLocations;
                    OutputImg.MinMax(out MinValues, out MaxValues, out minLocations, out MaxLocations);

                    OutputImg = OutputImg.Sub(new Gray(MinValues[0]));
                    OutputImg = OutputImg.Mul(255d / (MaxValues[0] - MinValues[0]));

                    OutputImg.Save(OutputFileName);
                    OutputFiles.Add(OutputFileName);
                    DetectorIndex++;
                    OutputImg = new Image<Gray, float>(SrcImg.Width, SrcImg.Height);
                }
            }

            return OutputFiles;
        }

        public static void ProcessChimps()
        {
            string OutputFolder = "c:/work/Bristol/Chimps/cgp/unseen_withsrcimg_subset/";

            try
            {
                Directory.Delete(OutputFolder, true);
            }
            catch (Exception e)
            {
            }
            string[] Folders = { 
                                   //"c:/work/Bristol/Chimps/sourceData/chimps/originalImages/test/" 
                                   "c:/work/Bristol/Chimps/sourceData/chimps/originalImages/trainandvalidation",
                                //"c:/work/Bristol/Chimps/sourceData/chimps/originalImages/train/",
                               //"c:/work/Bristol/Chimps/sourceData/chimps/originalImages/validation/"
                               };
            string DataFolder = "c:/work/Bristol/Chimps/";

            List<string> ImageNames = new List<string>();

            foreach (string Folder in Folders)
            {
                string[] Files = Directory.GetFiles(Folder, "*.jpg");
                ImageNames.AddRange(Files);
            }
            ImageNames.Sort();
            List<string> ImageNames2 = new List<string>();

            Random rng = new Random();
            double s = 0;
            for (int i = 0; i < ImageNames.Count; i++)
                s += i;
            

            while (ImageNames2.Count != 64)
            {
                ImageNames2.Clear();
                
                for (int i = 0; i < ImageNames.Count; i++)
                {
                    if (rng.NextDouble() < 65 * ((double)i / s) && ImageNames2.Count<64)
                        ImageNames2.Add(ImageNames[i]);
             
                }
                Console.Write("Trying...");
                
            }
            
            ImageNames = ImageNames2;

            if (!Directory.Exists(OutputFolder))
                Directory.CreateDirectory(OutputFolder);

            int Index = 0;
            foreach (string ImageFile in ImageNames)
            {

                Index++;

                Console.WriteLine(ImageFile + "\t" + Index + " of " + ImageNames.Count);
                string BaseImageFileName = Path.GetFileNameWithoutExtension(ImageFile);
             
                
                string BundleFile = OutputFolder + "/" + BaseImageFileName + ".txt";
                TextWriter tw = new StreamWriter(BundleFile);
                tw.WriteLine("path\t" + OutputFolder);

                Image<Bgr, byte> SrcImg = new Image<Bgr, byte>(ImageFile);
               
                SrcImg.Convert<Gray, byte>().Save(OutputFolder + "/" + BaseImageFileName + "_gray.png");
                tw.WriteLine("input\t" + OutputFolder + "/" + BaseImageFileName + "_gray.png");

                Image<Hsv, float> HsvImg = SrcImg.Convert<Hsv, float>();
                Image<Gray, float>[] HsvComps = HsvImg.Split();
                for (int i = 0; i < HsvComps.Length; i++)
                {
                    HsvComps[i].Save(OutputFolder + "/" + BaseImageFileName + "_hsv"+i+".png");
                    tw.WriteLine("input\t" + OutputFolder + "/" + BaseImageFileName + "_hsv" + i + ".png");
                }
           
                Image<Gray, byte>[] RgbComps = SrcImg.Split();
                for (int i = 0; i < RgbComps.Length; i++)
                {
                    RgbComps[i].Save(OutputFolder + "/" + BaseImageFileName + "_rgb" + i + ".png");
                    tw.WriteLine("input\t" + OutputFolder + "/" + BaseImageFileName + "_rgb" + i + ".png");
                }
                
                

                List<string> Detectors = ProcessChimpDetectors(SrcImg, ImageFile, DataFolder+"/"+BaseImageFileName, OutputFolder);
                foreach (string D in Detectors)
                    tw.WriteLine("input\t" + D);

                string target = ProcessChimpTarget(SrcImg, ImageFile, DataFolder + "/" + BaseImageFileName+".xml", OutputFolder);

                tw.WriteLine("target\t" + target);

                tw.Close();
            }

        }

        public static double ScantronMinValue = double.MaxValue;
        public static double ScantronMaxValue = double.MinValue;

        public static CGPImageTestSet MakeScantronTestset()
        {            

            Parameters.FitnessAggregationType = Parameters.FitnessAggregation.WORST;
            Parameters.MaxFilesToToTest = 100;
            Parameters.MaxFilesToLoad = 100;
            Parameters.MaxImageDimension = 512;
            Parameters.IncreaseThreshold = 5;
            Parameters.VisualizationSize = 50;
            Parameters.ImageBorder = 4;
            Parameters.IncreaseThreshold = 6000;

            CGPImageTestSet TestSet = new CGPImageTestSet();
            Random RNG = new Random(1);
                 
            for (int i = 1; i < Parameters.MaxFilesToLoad+1; i++)
            {
                string Path = "C:\\work\\scantron\\PRSReader\\PRSReader\\bin\\Debug\\";
                string FileName = Path + "A_Unknown" + i + ".raw";
                string TargetFileName = Path+"average.raw";
                CGPImageTestCase TestCase = new CGPImageTestCase(FileName, TargetFileName, false, false, false, false, false, false,-1);
                int PatchSize = 200;
                //Rectangle ROI = new Rectangle(RNG.Next(0,TestCase.Target.Width-PatchSize), RNG.Next(0, TestCase.Target.Height-PatchSize), PatchSize, PatchSize);
                double Min, Max;
                
                Rectangle ROI = new Rectangle((TestCase.Target.Width / 2) - (PatchSize / 2), (TestCase.Target.Height / 2) - (PatchSize / 2), PatchSize, PatchSize);
                for (int k = 0; k < TestCase.Inputs.Count; k++)
                {
                    TestCase.Inputs[k] = CGPImageFunctions.Extract(TestCase.Inputs[k], ROI);
                }

                int ShiftRadius = 1;
                for(int sx = -ShiftRadius;sx<=ShiftRadius;sx++)
                    for (int sy = -ShiftRadius; sy <= ShiftRadius; sy++)
                    {
                        if (sx == 0 && sy == 0) continue;
                        CGPImage ShiftedImg = CGPImageFunctions.Shift(TestCase.Inputs[0], sx, sy);
                        TestCase.Inputs.Add(ShiftedImg);
                    }

                Parameters.InputCount = TestCase.Inputs.Count;
                TestCase.Target = CGPImageFunctions.Extract(TestCase.Target, ROI);

                CGPImageFunctions.MinMax(TestCase.Inputs[0], out Min, out Max);
                if (Min < ScantronMinValue) ScantronMinValue = Min;
                if (Max > ScantronMaxValue) ScantronMaxValue = Max;


                TestSet.TestCases.Add(TestCase);
                TestCase.Inputs[0].Save(i + "_input.png");
                TestCase.Target.Save(i + "_target.png");
            }
            TestSet.MakeCheckSums();
            Parameters.ValidationSplit = 0.6;
            TestSet.LabelTestAndTraining(Parameters.ValidationSplit);
            TestSet.TestCases[0].UseType = CGPImageTestCase.TestCaseUseType.Training;
            Parameters.MaxGenotypes = 1;

            return TestSet;
        }

        public static void HaarFeatures()
        {
            CGPImage I = new CGPImage("C:\\work\\Bristol\\Images\\tom\\5681644663_a688cc9a0a_o.jpg");
            I = CGPImageFunctions.Rescale(I, 256, 256);
            I.ComputeIntegralImages();

            for (int f = 0; f < 6; f ++)
            for (int s = 6; s <=6; s += 2)            
            {
                CGPImage R = CGPImageFunctions.Feature(I, f, true,false,s,1);
                R.Save(String.Format("feature{1}_size{0}.png", s,f));
            }
        }

        public static void RenderBlobs()
        {
            CGPImage Src = new CGPImage("C:\\work\\csharp\\CGPIP2\\CGPIP2\\bin\\x64\\Release\\rr_metal2\\Predictions\\12011731-2013-04-29-175051.png");
            Image<Bgr, byte> Out = new Image<Bgr, byte>("C:\\work\\csharp\\CGPIP2DataSets\\sheet0\\Inputs\\12011731-2013-04-29-175051.bmp");

            Src = CGPImageFunctions.Rescale(Src, Out.Width, Out.Height);
            //Src = CGPImageFunctions.Close(Src, 7, 7, 3);
         //   Src = CGPImageFunctions.ThresholdInv(Src, 128);
            List<Rectangle> Blobs = CGPImageFunctions.Blobs(Src,-1, false,-1, -1);
            int BoxSizeMod = 10;
            foreach (Rectangle R in Blobs)
            {
                Rectangle R2 = new Rectangle(R.X - BoxSizeMod, R.Y - BoxSizeMod, R.Width + (BoxSizeMod * 2), R.Height + (BoxSizeMod * 2));
                Out.Draw(R2, new Bgr(Color.Red), 2);
            }
            Out.Save("12011731-2013-04-29-175051_blobs.png");
         //   BlobImg.Save("1_blobs.png");
        }

        public static void Test()
        {
            string Root = "C:\\work\\csharp\\CGPIP2DataSets\\icubDataJan2013\\input\\";
            string Output = ".\\test\\";
            if (!Directory.Exists(Output))
                Directory.CreateDirectory(Output);

            string[] Files = Directory.GetFiles(Root, "*.png");

            foreach (string F in Files)
            {
                CGPImage InputImg = new CGPImage(F);
                InputImg = CGPImageFunctions.Rescale(InputImg, 0.25, false);

                for (int Levels = 0; Levels < 256; Levels += 16)
                {
                    CGPImage R = CGPImageFunctions.Quantize(InputImg, Levels);
                    R.Save(Output + "\\" + Path.GetFileNameWithoutExtension(F) + "_" + Levels+".png");
                }
            }
        }

        internal static void BuildKNN()
        {
            string Root = "C:/work/MI/EvoScan1c/EvoScan1c/bin/x64/Release/training/aluminium2/";//
            string InputFile = Root + "/Inputs/94abaf87-d54e-4140-86ce-9a0b9c64a2c0.png"; ;
            string TargetFile = Root + "/Targets/94abaf87-d54e-4140-86ce-9a0b9c64a2c0.png";

            TextWriter tw = new StreamWriter("knn.arff");

            int Radius = 5;
            int InputCount = ((Radius * 2) + 1) * ((Radius * 2) + 1);

            tw.WriteLine("@relation test");
            for (int i = 0; i < InputCount; i++)
                tw.WriteLine(string.Format("@attribute input{0:000} real", i));
            tw.WriteLine("@attribute ClassType {0,1}");

            tw.WriteLine("\n@data");

            Image<Gray, byte> InputImg = new Image<Gray, byte>(InputFile);
            Image<Bgr, byte> TargetImg = new Image<Bgr, byte>(TargetFile);

            Random RNG = new Random(123);

            for (int c = 0; c < 5000; c++)
            {
                int x = RNG.Next(Radius, TargetImg.Width - Radius);
                int y = RNG.Next(Radius, TargetImg.Height - Radius);
                bool Class =
                    TargetImg.Data[y, x, 0] < 25 &&
                    TargetImg.Data[y, x, 1] < 25 &&
                    TargetImg.Data[y, x, 2] > 250;

                for (int i = x - Radius; i <= x + Radius;i++ )
                    for (int j = y - Radius; j <= y + Radius; j++)
                    {
                        tw.Write(InputImg.Data[j,i, 0] + ",");
                    }

                    tw.WriteLine((Class ? "1" : "0"));

            }

            tw.Close();

        }

        internal static void TestNorm()
        {
            string Root = "C:/work/MI/EvoScan1c/EvoScan1c/bin/x64/Release/training/aluminium2/";//
            string InputFile = Root + "/Inputs/94abaf87-d54e-4140-86ce-9a0b9c64a2c0.png"; ;

            CGPImage Img = new CGPImage(InputFile);

            for(int r0=1;r0<20;r0+=2)
                for (int r1 = 1; r1 < 20; r1 += 2)
                {
                    CGPImage N = CGPImageFunctions.LocalNormalizeWithGauss(Img, r0, r1);
                    N.Save(string.Format("norm_{0:00}_{1:00}.png", r0, r1));
                    Console.WriteLine(string.Format("norm_{0:00}_{1:00}.png", r0, r1));
                }
        }

        internal static void PenguinsNorm()
        {
            string FPath = "C:\\work\\Bristol\\TrainingImagesForCGP\\Processed6_bodyonly\\detections\\";
            string OutPath = "C:\\work\\Bristol\\TrainingImagesForCGP\\Processed6_bodyonly\\detections\\test\\";
            string[] Files = Directory.GetFiles(FPath, "*.png");
            foreach (string F in Files)
            {
                
                CGPImage Img = new CGPImage(F);
                CGPImage Norm = CGPImageFunctions.LocalNormalizeWithGauss(Img, 2, 10);
                //CGPImage Threshold = CGPImageFunctions.OtsuThreshold(Norm, 120);
                Norm.Save(OutPath + "//" + Path.GetFileName(F));
            }
        }

        public static Image<Bgr, byte> get_hogdescriptor_visu(Image<Bgr, byte>  origImg, float[] descriptorValues, Size WinSize)
{   
    
 
    float zoomFac = 3;
    Image<Bgr, byte> visu = origImg.Resize(3, Emgu.CV.CvEnum.INTER.CV_INTER_NN);
    
 
    int blockSize       = 16;
    int cellSize        = 8;
    int gradientBinSize = 9;
    float radRangeForOneBin = (float)Math.PI/(float)gradientBinSize; // dividing 180° into 9 bins, how large (in rad) is one bin?
 
    // prepare data structure: 9 orientation / gradient strenghts for each cell
    int cells_in_x_dir = WinSize.Width / cellSize;
    int cells_in_y_dir = WinSize.Height / cellSize;
    int totalnrofcells = cells_in_x_dir * cells_in_y_dir;
    float[][][] gradientStrengths = new float[cells_in_y_dir][][];
    int[][] cellUpdateCounter   = new int[cells_in_y_dir][];
    for (int y=0; y<cells_in_y_dir; y++)
    {
        gradientStrengths[y] = new float[cells_in_x_dir][];
        cellUpdateCounter[y] = new int[cells_in_x_dir];
        for (int x=0; x<cells_in_x_dir; x++)
        {
            gradientStrengths[y][x] = new float[gradientBinSize];
            cellUpdateCounter[y][x] = 0;
 
            for (int bin=0; bin<gradientBinSize; bin++)
                gradientStrengths[y][x][bin] = 0.0f;
        }
    }
 
    // nr of blocks = nr of cells - 1
    // since there is a new block on each cell (overlapping blocks!) but the last one
    int blocks_in_x_dir = cells_in_x_dir - 1;
    int blocks_in_y_dir = cells_in_y_dir - 1;
 
    // compute gradient strengths per cell
    int descriptorDataIdx = 0;
    int cellx = 0;
    int celly = 0;
 
    for (int blockx=0; blockx<blocks_in_x_dir; blockx++)
    {
        for (int blocky=0; blocky<blocks_in_y_dir; blocky++)            
        {
            // 4 cells per block ...
            for (int cellNr=0; cellNr<4; cellNr++)
            {
                // compute corresponding cell nr
                 cellx = blockx;
                celly = blocky;
                if (cellNr==1) celly++;
                if (cellNr==2) cellx++;
                if (cellNr==3)
                {
                    cellx++;
                    celly++;
                }
 
                for (int bin=0; bin<gradientBinSize; bin++)
                {
                    if (descriptorDataIdx >= descriptorValues.Length)
                        continue;
                    float gradientStrength = descriptorValues[ descriptorDataIdx ];
                    descriptorDataIdx++;
 
                    gradientStrengths[celly][cellx][bin] += gradientStrength;
 
                } // for (all bins)
 
 
                // note: overlapping blocks lead to multiple updates of this sum!
                // we therefore keep track how often a cell was updated,
                // to compute average gradient strengths
                cellUpdateCounter[celly][cellx]++;
 
            } // for (all cells)
 
 
        } // for (all block x pos)
    } // for (all block y pos)
 
 
    // compute average gradient strengths
    for ( celly=0; celly<cells_in_y_dir; celly++)
    {
        for ( cellx=0; cellx<cells_in_x_dir; cellx++)
        {
 
            float NrUpdatesForThisCell = (float)cellUpdateCounter[celly][cellx];
 
            // compute average gradient strenghts for each gradient bin direction
            for (int bin=0; bin<gradientBinSize; bin++)
            {
                gradientStrengths[celly][cellx][bin] /= NrUpdatesForThisCell;
            }
        }
    }
 
 
 
    // draw cells
    for ( celly=0; celly<cells_in_y_dir; celly++)
    {
        for ( cellx=0; cellx<cells_in_x_dir; cellx++)
        {
            int drawX = cellx * cellSize;
            int drawY = celly * cellSize;
 
            int mx = drawX + cellSize/2;
            int my = drawY + cellSize/2;
 
            //rectangle(visu, Point(drawX*zoomFac,drawY*zoomFac), Point((drawX+cellSize)*zoomFac,(drawY+cellSize)*zoomFac), CV_RGB(100,100,100), 1);
            //RectangleF L = new RectangleF();
            //visu.Draw(L, new Bgr(100, 100, 100), 1);
            // draw in each cell all 9 gradient strengths
            for (int bin=0; bin<gradientBinSize; bin++)
            {
                float currentGradStrength = gradientStrengths[celly][cellx][bin];
 
                // no line to draw?
                if (currentGradStrength==0)
                    continue;

                float currRad = (float)bin * radRangeForOneBin + radRangeForOneBin / 2;
 
                float dirVecX = (float)Math.Cos( currRad );
                float dirVecY = (float)Math.Sin( currRad );
                float maxVecLen = cellSize/2;
                float scale = 2.5f; // just a visualization scale, to see the lines better
 
                // compute line coordinates
                float x1 = (float)mx - dirVecX * currentGradStrength * maxVecLen * scale;
                float y1 = (float)my - dirVecY * currentGradStrength * maxVecLen * scale;
                float x2 = (float)mx + dirVecX * currentGradStrength * maxVecLen * scale;
                float y2 = (float)my + dirVecY * currentGradStrength * maxVecLen * scale;
 
                // draw gradient visualization
                //line(visu, Point(x1*zoomFac,y1*zoomFac), Point(x2*zoomFac,y2*zoomFac), CV_RGB(0,255,0), 1);
                LineSegment2DF L2 = new LineSegment2DF(new PointF(x1 * zoomFac, y1 * zoomFac), new PointF(x2 * zoomFac, y2 * zoomFac));
                visu.Draw(L2, new Bgr(0,255,0), 1);
            } // for (all bins)
 
        } // for (cellx)
    } // for (celly)
 
 

 
    return visu;
 
} // get_hogdescriptor_visu

        internal static void Hog()
        {
            string Root = "C:\\work\\training_replica\\";
            string FileName = "12011731-2013-09-03-114124.bmp"; //"hogtest.bmp";// "12011731-2013-09-03-114124.bmp";
            Image<Bgr, byte> Img = new Image<Bgr, byte>(Root + "\\" + FileName);
            HOGDescriptor Hog = new HOGDescriptor();

            float[] Desc = Hog.Compute(Img, Img.Size, new Size(0, 0), null);
            Image<Bgr, byte> V = get_hogdescriptor_visu(Img, Desc, Img.Size);
            V.Save(Root + "\\hog.png");
            //Emgu.CV.
        }
    }
}
