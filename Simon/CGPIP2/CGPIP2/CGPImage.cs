﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.Structure;
using System.IO;

namespace CGPIP2
{
    public class CGPImage
    {
        public Image<Gray, float> Img = null;
        public string Label = "";
        public string AssociatedFileName = "";



        public double[] MinValues;
        public double[] MaxValues;
        public Point[] minLocations;
        public Point[] maxLocations;

        public int Width = 0;

        public int Height = 0;

        public CGPImage(int Width, int Height)
        {
            this.Img = new Image<Gray, float>(Width, Height);
            this.Width = Width; this.Height = Height;
        }

        public CGPImage(int Width, int Height, float Value)
        {
            this.Img = new Image<Gray, float>(Width, Height, new Gray(Value));
            this.Width = Width; this.Height = Height;
        }

        public CGPImage(int Width, int Height, string Label)
        {
            this.Label = Label;
            this.Img = new Image<Gray, float>(Width, Height);
            this.Width = Width; this.Height = Height;
        }

        public CGPImage(int Width, int Height, float Value, string Label)
        {
            this.Label = Label;
            this.Img = new Image<Gray, float>(Width, Height, new Gray(Value));
            this.Width = Width; this.Height = Height;
        }

        public CGPImage(Image<Gray, float> Img)
        {
            this.Img = Img;
            this.Width = Img.Width; this.Height = Img.Height;
        }

        public CGPImage(Image<Gray, float> Img, string Label)
        {
            this.Label = Label;
            this.Img = Img;
            this.Width = Img.Width; this.Height = Img.Height;
        }

        public CGPImage(Bitmap Bmp)
        {
            this.Img = new Image<Gray, float>(Bmp);
            this.Width = Img.Width; this.Height = Img.Height;
        }

        public CGPImage(Bitmap Bmp, string Label)
        {
            this.Label = Label;
            this.Img = new Image<Gray, float>(Bmp);
            this.Width = Img.Width; this.Height = Img.Height;
        }

        public CGPImage(string FileName)
        {
            this.AssociatedFileName = FileName;
            if (FileName.EndsWith(".dat"))
                this.LoadFromDat(FileName);
            else if (FileName.EndsWith(".raw"))
                this.LoadFromRaw(FileName);
            else
                this.Img = new Image<Gray, float>(FileName);

            this.Width = Img.Width; this.Height = Img.Height;
        }

        public void LoadFromDat(string FileName)
        {
            Reporting.Say("Loading " + FileName);
            int w, h;
            Stream s;
            if (File.Exists(FileName + ".bin") || FileName.EndsWith(".bin"))
            {
                //s = new FileStream(FileName + ".bin", FileMode.Open);

                byte[] FileBytes = File.ReadAllBytes(FileName + (FileName.EndsWith(".bin") ? "" : ".bin"));
                BinaryReader br = new BinaryReader(new MemoryStream(FileBytes));
                w = br.ReadInt32();
                h = br.ReadInt32();
                this.Img = new Image<Gray, float>(w, h);

                for (int i = 0; i < w; i++)
                    for (int j = 0; j < h; j++)
                    {
                        float v = br.ReadSingle();
                        this.Img.Data[j, i, 0] = v;
                    }
                br.Close();

                //this.Img.Save(FileName + ".png");
                return;
            }

            char[] Toks = { ' ' };
            string[] Lines = File.ReadAllLines(FileName);
            w = Lines[0].Split(Toks, StringSplitOptions.RemoveEmptyEntries).Length;
            h = Lines.Length;
            w = 1024;
            h = 1024;
            this.Img = new Image<Gray, float>(w, h);
            this.AssociatedFileName = FileName;

            float MinValue = float.MaxValue;
            float MaxValue = float.MinValue;
            for (int j = 0; j < Math.Min(h, Lines.Length); j++)
            {
                string[] LineToks = Lines[j].Split(Toks, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < w; i++)
                {
                    float v;
                    if (!float.TryParse(LineToks[i], out v))
                        v = 0;
                    if (v < MinValue)
                        MinValue = v;
                    if (v > MaxValue)
                        MaxValue = v;
                    this.Img.Data[j, i, 0] = v;
                }
            }

            //for (int i = 0; i < w; i++)
            //   for (int j = 0; j < h; j++)
            //         this.Img.Data[j, i, 0] = ((this.Img.Data[j, i, 0] - MinValue) / (MaxValue - MinValue)) * 255f;


            s = new FileStream(FileName + ".bin", FileMode.Create);
            BinaryWriter bw = new BinaryWriter(s);
            bw.Write(w);
            bw.Write(h);
            for (int i = 0; i < w; i++)
                for (int j = 0; j < h; j++)
                    bw.Write(this.Img.Data[j, i, 0]);

            s.Close();
        }

        public void LoadFromRaw(string FileName)
        {
            Reporting.Say("Loading " + FileName);
            int w, h;
            Stream s;

            byte[] FileBytes = File.ReadAllBytes(FileName);
            BinaryReader br = new BinaryReader(new MemoryStream(FileBytes));
            w = br.ReadUInt16();
            h = br.ReadUInt16();
            this.Img = new Image<Gray, float>(w, h);


            for (int j = 0; j < h; j++)
                for (int i = 0; i < w; i++)
                {
                    double v = br.ReadDouble();
                    this.Img.Data[j, i, 0] = (float)v;
                }

            br.Close();

            //this.Img.ROI = new Rectangle(20, 150, this.Img.Width-20, 250);

            // this.Img = this.Img.GetSubRect(new Rectangle(255, 160, 110,110));
            this.Width = this.Img.Width;
            this.Height = this.Img.Height;

        }



        public CGPImage(CGPImage OtherCGPImage)
        {
            this.Img = OtherCGPImage.Img.Clone();
            this.Width = Img.Width; this.Height = Img.Height;
        }

        public CGPImage Clone()
        {
            CGPImage I = new CGPImage(this);
            I.Img = this.Img.Clone();
            I.Width = this.Width;
            I.Height = this.Height;
            I.Label = this.Label;

            // I.IntSqSum = this.IntSqSum.Clone();
            //I.IntSum = this.IntSum.Clone();
            //I.IntTiltedSum = this.IntTiltedSum.Clone();
            I.AssociatedFileName = this.AssociatedFileName;
            return I;
        }

        public void Save(string FileName)
        {
            this.Img.Save(FileName);
        }

        public Image<Gray, double> IntSum = null;
        public Image<Gray, double> IntSqSum = null;
        public Image<Gray, double> IntTiltedSum = null;
        public double CheckSum = 0;

        public void CheckCheckSum()
        {
            double Value = this.Img.GetSum().Intensity;
            if (Value != CheckSum)
                throw new Exception("Image is corrupted!");
        }

        public void ComputeIntegralImages()
        {
            if (IntSum == null)
                this.Img.Integral(out IntSum, out IntSqSum, out IntTiltedSum);
        }

        public double GetIntegral(int X, int Y, int AreaWidth, int AreaHeight, Image<Gray, double> IntegralImage)
        {
            if (X < 0) X = 0;
            else if (X + AreaWidth >= this.Width) X = this.Width - AreaWidth - 1;

            if (Y < 0) Y = 0;
            else if (Y + AreaHeight >= this.Height) Y = this.Height - AreaHeight - 1;

            //From open cv http://docs.opencv.org/modules/imgproc/doc/miscellaneous_transformations.html
            return IntegralImage.Data[Y + AreaHeight + 1, X + AreaWidth + 1, 0] -
                IntegralImage.Data[Y + AreaHeight + 1, X, 0] -
                IntegralImage.Data[Y, X + AreaWidth + 1, 0] +
                IntegralImage.Data[Y, X, 0]
                ;
            //return IntegralImage.Data[Y + AreaHeight + 1, X + AreaWidth + 1, 0] + IntegralImage.Data[Y, X, 0] - IntegralImage.Data[Y, X + AreaWidth + 1, 0] - IntegralImage.Data[Y + AreaHeight + 1, X, 0];
            // return IntegralImage.Data[Y + Height - this.Img.ROI.Top, X + Width - this.Img.ROI.Left, 0] + IntegralImage.Data[Y - this.Img.ROI.Top, X - this.Img.ROI.Left, 0] - IntegralImage.Data[Y - this.Img.ROI.Top, X + Width - this.Img.ROI.Left, 0] - IntegralImage.Data[Y + Height - this.Img.ROI.Top, X - this.Img.ROI.Left, 0];
        }

        public float[,] GetNeighbourhood(int X, int Y, int Aperture)
        {
            float[,] Values = new float[(Aperture * 2) + 1, (Aperture * 2) + 1];

            if (X + Aperture >= this.Img.Width)
                X = this.Img.Width - 1;
            else if (X < Aperture)
                X = Aperture;
            if (Y + Aperture >= this.Img.Height)
                Y = this.Img.Height - 1;
            else if (Y > Aperture)
                Y = Aperture;


            for (int i = 0; i < (Aperture * 2) + 1; i++)
                for (int j = 0; j < (Aperture * 2) + 1; j++)
                    Values[j, i] = this.Img.Data[Y + j - Aperture, X + i - Aperture, 0];

            return Values;
        }

        public float[] Histogram = null;

        public void ComputeHistogram(int QuantizeLevel)
        {
            if (Histogram != null && Histogram.Length == QuantizeLevel)
                return;

            Histogram = new float[QuantizeLevel + 1];

            try
            {
                for (int i = 0; i < this.Img.Width; i++)
                    for (int j = 0; j < this.Img.Height; j++)
                    {
                        float Quantized = this.Img.Data[j, i, 0];
                        if (Quantized < 0) Quantized = 0;
                        else if (Quantized > 255) Quantized = 255;
                        Quantized /= (QuantizeLevel + 1);
                        Histogram[(int)Quantized]++;
                    }
            }
            catch (Exception e)
            {

            }
            float Sum = 0;
            for (int i = 0; i < Histogram.Length; i++)
                Sum += Histogram[i];
            for (int i = 0; i < Histogram.Length; i++)
                Histogram[i] /= Sum;
        }
    }

    public class CGPImageFunctions
    {

        public static CGPImage Extract(CGPImage A, Rectangle ROI)
        {
            // return new CGPImage(A.Img.GetSubRect(ROI));
            CGPImage R = new CGPImage(ROI.Width, ROI.Height);
            for (int i = ROI.X; i < ROI.X + ROI.Width; i++)
                for (int j = ROI.Y; j < ROI.Y + ROI.Height; j++)
                {
                    R.Img.Data[j - ROI.Y, i - ROI.X, 0] = A.Img.Data[j, i, 0];
                }
            return R;
        }

        public static CGPImage Rescale(CGPImage A, double Scale, bool Safe)
        {

            if (Safe)
            {
                if (Scale < 0.01)
                    return A.Clone();
                if (Scale * A.Width >= Parameters.MaxImageDimension || Scale * A.Height >= Parameters.MaxImageDimension)
                    return A.Clone();
                if (Scale * A.Width <= Parameters.MinImageDimension || Scale * A.Height <= Parameters.MinImageDimension)
                    return A.Clone();
            }
            if (Scale>=1)
            return new CGPImage(A.Img.Resize(Scale, Emgu.CV.CvEnum.INTER.CV_INTER_NN), "Rescaled by " + Scale + " " + A.Label);
            return new CGPImage(A.Img.Resize(Scale, Emgu.CV.CvEnum.INTER.CV_INTER_AREA), "Rescaled by " + Scale + " " + A.Label);
        }

        public static CGPImage ReSample(CGPImage A, double Scale)
        {
            if (Scale < 0.01)
                return A.Clone();
            if (Scale * A.Width >= Parameters.MaxImageDimension || Scale * A.Height >= Parameters.MaxImageDimension)
                return A.Clone();
            if (Scale * A.Width <= Parameters.MinImageDimension || Scale * A.Height <= Parameters.MinImageDimension)
                return A.Clone();
            Image<Gray, float> T = A.Img.Resize(Scale, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
            return new CGPImage(T.Resize(A.Width, A.Height, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR));
        }

        public static CGPImage Rescale(CGPImage A, int w, int h)
        {
            if (w>A.Width || h>A.Height)
                return new CGPImage(A.Img.Resize(w, h, Emgu.CV.CvEnum.INTER.CV_INTER_NN), "Rescaled " + A.Label);
            else
                return new CGPImage(A.Img.Resize(w, h, Emgu.CV.CvEnum.INTER.CV_INTER_AREA), "Rescaled " + A.Label);
        }


        public static CGPImage Rescale(CGPImage A, int w, int h, Emgu.CV.CvEnum.INTER InterpolationType)
        {
            return new CGPImage(A.Img.Resize(w, h, InterpolationType), "Rescaled " + A.Label);
        }

        public static CGPImage Add(CGPImage A, CGPImage B)
        {
            return new CGPImage(A.Img.Add(B.Img));
        }

        public static CGPImage Add(CGPImage A, CGPImage B, CGPImage C)
        {
            return new CGPImage(A.Img.Add(B.Img.Add(C.Img)));
        }

        public static CGPImage Avg(CGPImage A, CGPImage B, CGPImage C)
        {
            CGPImage R = new CGPImage(A.Img.Add(B.Img.Add(C.Img)));
            R.Img._Mul(1d / 3d);
            return R;
        }

        public static CGPImage Sub(CGPImage A, CGPImage B)
        {
            return new CGPImage(A.Img.Sub(B.Img));
        }

        public static CGPImage Mul(CGPImage A, CGPImage B)
        {
            return new CGPImage(A.Img.Mul(B.Img));
        }

        public static CGPImage Div(CGPImage A, CGPImage B)
        {
            Image<Gray, float> R = new Image<Gray, float>(A.Width, A.Height);
            CvInvoke.cvDiv(A.Img.Ptr, B.Img.Ptr, R.Ptr, 1);
            return new CGPImage(R);
        }

        public static CGPImage Add(CGPImage A, float V)
        {
            return new CGPImage(A.Img.Add(new Gray(V)));
        }

        public static CGPImage Sub(CGPImage A, float V)
        {
            return new CGPImage(A.Img.Sub(new Gray(V)));
        }

        public static CGPImage Mul(CGPImage A, float V)
        {
            return new CGPImage(A.Img.Mul(V));
        }

        public static CGPImage Shift(CGPImage A, int XShift, int YShift)
        {
            int w = A.Width;
            int h = A.Height;
            CGPImage R = new CGPImage(w, h);
            for (int x = 0; x < w; x++)
                for (int y = 0; y < h; y++)
                {
                    int x2 = Math.Abs(x + XShift) % w;
                    int y2 = Math.Abs(y + YShift) % h;
                    R.Img.Data[y, x, 0] = A.Img.Data[y2, x2, 0];
                }

            return R;
        }

        public static CGPImage Erode(CGPImage A, int Iterations)
        {
            return new CGPImage(A.Img.Erode(Iterations));
        }

        public static CGPImage Dilate(CGPImage A, int Iterations)
        {
            return new CGPImage(A.Img.Dilate(Iterations));
        }

        public static CGPImage Threshold(CGPImage A, float Threshold)
        {
            return new CGPImage(A.Img.ThresholdBinary(new Gray(Threshold), new Gray(255)));
        }

        public static CGPImage ThresholdInv(CGPImage A, float Threshold)
        {
            return new CGPImage(A.Img.ThresholdBinaryInv(new Gray(Threshold), new Gray(255)));
        }

        public static double Difference(CGPImage A, CGPImage B)
        {
            return A.Img.Sub(B.Img).GetSum().Intensity;
        }

        public static double SumAbsDifference(CGPImage A, CGPImage B)
        {
            return A.Img.AbsDiff(B.Img).GetSum().Intensity;
        }

        public static CGPImage AbsDifference(CGPImage A, CGPImage B)
        {
            return new CGPImage(A.Img.AbsDiff(B.Img));
        }

        public static Dictionary<int, IntPtr> StructuringElementCache = new Dictionary<int, IntPtr>();

        public static IntPtr CreateStructuringElement(int Aperture)
        {
            if (Aperture < 0)
                Aperture *= -1;
            else if (Aperture == 0)
                Aperture = 1;
            if (StructuringElementCache.ContainsKey(Aperture))
                return StructuringElementCache[Aperture];
            IntPtr StrucElem =
           Emgu.CV.CvInvoke.cvCreateStructuringElementEx(
           Aperture * 2 + 1, Aperture * 2 + 1,
           Aperture, Aperture,
           Emgu.CV.CvEnum.CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE,
           IntPtr.Zero);
            StructuringElementCache.Add(Aperture, StrucElem);

            return StrucElem;
        }

        public static IntPtr CreateStructuringElement(int Width, int Height)
        {
            int Key = (Width * 1024) + Height;
            if (StructuringElementCache.ContainsKey(Key))
                return StructuringElementCache[Key];
            IntPtr StrucElem =
           Emgu.CV.CvInvoke.cvCreateStructuringElementEx(
           Width, Height,
           Width / 2, Height / 2,
           Emgu.CV.CvEnum.CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE,
           IntPtr.Zero);

            StructuringElementCache.Add(Key, StrucElem);

            return StrucElem;
        }

        public static CGPImage Morphology(CGPImage A, int Width, int Height, int Iterations, Emgu.CV.CvEnum.CV_MORPH_OP MorphType)
        {
            Image<Gray, float> Dest = new Image<Gray, float>(A.Width, A.Height);
            Image<Gray, float> Temp = null;
            if (MorphType == Emgu.CV.CvEnum.CV_MORPH_OP.CV_MOP_GRADIENT ||
                MorphType == Emgu.CV.CvEnum.CV_MORPH_OP.CV_MOP_TOPHAT ||
                MorphType == Emgu.CV.CvEnum.CV_MORPH_OP.CV_MOP_BLACKHAT)
                Temp = new Image<Gray, float>(A.Width, A.Height);

            IntPtr StrucElement = CreateStructuringElement(Width, Height);

            CvInvoke.cvMorphologyEx(
                A.Img.Ptr,
                Dest.Ptr,
                Temp == null ? IntPtr.Zero : Temp.Ptr,
                StrucElement,
                MorphType,
                Iterations);

            return new CGPImage(Dest);
        }

        public static CGPImage Open(CGPImage A, int Width, int Height, int Iterations)
        {
            return Morphology(A, Width, Height, Iterations, Emgu.CV.CvEnum.CV_MORPH_OP.CV_MOP_OPEN);
        }

        public static CGPImage Close(CGPImage A, int Width, int Height, int Iterations)
        {
            return Morphology(A, Width, Height, Iterations, Emgu.CV.CvEnum.CV_MORPH_OP.CV_MOP_CLOSE);
        }

        public static CGPImage Gradient(CGPImage A, int Width, int Height, int Iterations)
        {

            return Morphology(A, Width, Height, Iterations, Emgu.CV.CvEnum.CV_MORPH_OP.CV_MOP_GRADIENT);
        }

        public static CGPImage BlackHat(CGPImage A, int Width, int Height, int Iterations)
        {
            return Morphology(A, Width, Height, Iterations, Emgu.CV.CvEnum.CV_MORPH_OP.CV_MOP_BLACKHAT);
        }

        public static CGPImage TopHat(CGPImage A, int Width, int Height, int Iterations)
        {
            return Morphology(A, Width, Height, Iterations, Emgu.CV.CvEnum.CV_MORPH_OP.CV_MOP_TOPHAT);
        }

        public static CGPImage AdaptiveThreshold(CGPImage A)
        {
            return new CGPImage(A.Img.Convert<Gray, byte>().ThresholdAdaptive(new Gray(255), Emgu.CV.CvEnum.ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_MEAN_C, Emgu.CV.CvEnum.THRESH.CV_THRESH_BINARY, 3, new Gray(5)).Convert<Gray, float>());
        }

        public static CGPImage SmoothBilatral(CGPImage A, int ColorSigma, int SpaceSigma)
        {
            if (SpaceSigma > 24) SpaceSigma = 24;
            Image<Gray, byte> Src = A.Img.Convert<Gray, byte>();
            Image<Gray, byte> Dest = new Image<Gray, byte>(A.Img.Width, A.Img.Height);
            CvInvoke.cvSmooth(Src.Ptr, Dest.Ptr, Emgu.CV.CvEnum.SMOOTH_TYPE.CV_BILATERAL, 0, 0, Math.Min(10, Math.Abs(ColorSigma)), SpaceSigma);
            // Src.SmoothBilatral(
            return new CGPImage(Dest.Convert<Gray, float>());
        }

        public static CGPImage Min(CGPImage A, CGPImage B)
        {
            return new CGPImage(A.Img.Min(B.Img));
        }

        public static CGPImage Max(CGPImage A, CGPImage B)
        {
            return new CGPImage(A.Img.Max(B.Img));
        }

        public static CGPImage Min(CGPImage A, float V)
        {
            return new CGPImage(A.Img.Min(V));
        }

        public static CGPImage Max(CGPImage A, float V)
        {
            return new CGPImage(A.Img.Max(V));
        }


#if GABOR
        public static CGPImage Gabor(CGPImage A, short Frequency, short SizeX, short SizeY, short Orientation)
        {
            return new CGPImage(GaborEmguImg.GaborTransform(A.Img, Frequency, SizeX, SizeY, Orientation));
        }
#endif


        public static CGPImage Log(CGPImage A)
        {
            return new CGPImage(A.Img.Log());
        }

        public static CGPImage Sqrt(CGPImage A)
        {
            Image<Gray, float> Dest = new Image<Gray, float>(A.Img.Width, A.Img.Height);
            CvInvoke.cvSqrt(A.Img.Ptr, Dest.Ptr);
            return new CGPImage(Dest);
        }

        public static CGPImage Canny(CGPImage A, double v)
        {
            Image<Gray, float> Value = A.Img.Convert<Gray, byte>().Canny(v, 10, 3).Convert<Gray, float>();
            return new CGPImage(Value);
        }



        public static CGPImage Laplace(CGPImage Input0, int Aperture)
        {
            return new CGPImage(Input0.Img.Laplace(Aperture));
        }

        public static CGPImage Sobel(CGPImage A, int X, int Y, int Size)
        {
            if (X < 0) X *= -1;
            if (Y < 0) Y *= -1;
            if (X == 0) X = 1;
            if (Y == 0) Y = 1;
            Size = FixRange(MakeOdd(Size), 3, 15);

            Image<Gray, float> Value = Value = A.Img.Sobel(X, Y, Size);

            return new CGPImage(Value);
        }


        public static int MakeEven(int Value)
        {
            if (Value % 2 != 0)
                return Value + 1;
            return Value;
        }

        public static int MakeOdd(int Value)
        {
            if (Value % 2 == 0)
                return Value + 1;
            return Value;
        }

        public static CGPImage Eq(CGPImage Input0)
        {
            if (Input0.MinValues == null)
                Input0.Img.MinMax(out Input0.MinValues, out Input0.MaxValues, out Input0.minLocations, out Input0.maxLocations);

            Image<Gray, float> Value = Input0.Img.Clone();
            Value = Value.Add(new Gray((float)(0f - Input0.MinValues[0])));
            Value._Mul(255d / (Input0.MaxValues[0] - Input0.MinValues[0]));
            return new CGPImage(Value);
        }

        public static CGPImage Min(CGPImage A)
        {
            if (A.MinValues == null)
                A.Img.MinMax(out A.MinValues, out A.MaxValues, out A.minLocations, out A.maxLocations);

            return new CGPImage(A.Width, A.Height, (float)A.MinValues[0]);
        }

        public static CGPImage Max(CGPImage A)
        {
            if (A.MinValues == null)
                A.Img.MinMax(out A.MinValues, out A.MaxValues, out A.minLocations, out A.maxLocations);

            return new CGPImage(A.Width, A.Height, (float)A.MaxValues[0]);
        }

        public static void MinMax(CGPImage A, out double MinValue, out double MaxValue)
        {
            if (A.MinValues == null)
                A.Img.MinMax(out A.MinValues, out A.MaxValues, out A.minLocations, out A.maxLocations);
            MinValue = A.MinValues[0];
            MaxValue = A.MaxValues[0];
        }

        public static CGPImage SmoothMedian(CGPImage A, int Aperture)
        {
            Aperture = MakeOdd(Aperture);
            Aperture = FixRange(Aperture, 1, 11);
            try
            {

                return new CGPImage(A.Img.Convert<Gray, byte>().SmoothMedian(Aperture).Convert<Gray, float>());
            }
            catch (Exception e)
            {
                Reporting.Say("Aperture = " + Aperture);
                Console.WriteLine(e.Message);
                //Console.ReadLine();
                return A.Clone();
            }
        }

        public static CGPImage SmoothGaussian(CGPImage A, int Width, int Height)
        {
            Width = MakeOdd(Width);
            Height = MakeOdd(Height);
            Width = FixRange(Width, 3, 15);
            Height = FixRange(Height, 3, 15);
            Image<Gray, float> R = A.Img.SmoothGaussian(Width, Height, 0, 0);

            return new CGPImage(R);
        }

        public static CGPImage Unsharpen(CGPImage A, int SizeX, int SizeY)
        {
            if (SizeX < 0) SizeX *= -1;
            if (SizeY < 0) SizeY *= -1;


            Image<Gray, float> blurred = A.Img.SmoothGaussian(MakeOdd(SizeX), MakeOdd(SizeY), 1d, 1d);
            Image<Gray, float> unsharpenmask = A.Img.Sub(blurred);
            Image<Gray, float> hicontrast = unsharpenmask.Mul(1.5);
            Image<Gray, float> Value = A.Img.Add(hicontrast);

            return new CGPImage(Value);
        }


        public enum LocalStat { Min, Max, Average, Range };
        /*
        public static CGPImage LocalStats(CGPImage A, int ApertureWidth, int ApertureHeight, LocalStat LocalStatType)
        {
            int w = A.Width;
            int h = A.Height;
            
            CGPImage R = new CGPImage(w, h);
            if (ApertureHeight < 1) ApertureHeight = 1;
            else if (ApertureHeight > 3) ApertureHeight = 3;
            if (ApertureWidth < 1) ApertureWidth = 1;
            else if (ApertureWidth > 3) ApertureWidth = 3;


            if (LocalStatType == LocalStat.Average)
            {
                float Sum = 0;
                int ValuesLength = ((ApertureWidth * 2) + 1) * ((ApertureHeight * 2) + 1);
                for (int i = ApertureWidth; i < w - ApertureWidth; i++)
                    for (int j = ApertureHeight; j < h - ApertureHeight; j++)
                    {
                        
                        Sum=0;
                        for (int x = i - ApertureWidth; x <= i + ApertureWidth; x++)
                            for (int y = j - ApertureHeight; y <= j + ApertureHeight; y++)
                            {                                
                                Sum+=A.Img.Data[y, x, 0];
                             
                            }
                        
                        R.Img.Data[j, i, 0] = Sum / ValuesLength;

                    }
            }
            else
            {
                for (int i = ApertureWidth; i < w - ApertureWidth; i++)
                    for (int j = ApertureHeight; j < h - ApertureHeight; j++)
                    {
                        float MinValue = float.MaxValue;
                        float MaxValue = float.MinValue;
                        float v = 0;
                        for (int x = i - ApertureWidth; x <= i + ApertureWidth; x++)
                            for (int y = j - ApertureHeight; y <= j + ApertureHeight; y++)
                            {
                                v = A.Img.Data[y, x, 0];
                                if (MinValue > v)
                                    MinValue = v;
                                else
                                    if (MaxValue < v)
                                        MaxValue = v;
                            }
                        switch (LocalStatType)
                        {
                            case(LocalStat.Max):
                                R.Img.Data[j, i, 0] = MaxValue;
                                break;
                            case (LocalStat.Min):
                                R.Img.Data[j, i, 0] = MinValue;
                                break;
                            case (LocalStat.Range):
                                R.Img.Data[j, i, 0] = MaxValue-MinValue;
                                break;
                        }
                        

                    }
                
            }

            return R;
        }
        */
        public static CGPImage LocalStats(CGPImage A, int ApertureWidth, int ApertureHeight, LocalStat LocalStatType)
        {
            return LocalStats2(A, ApertureWidth, ApertureHeight, LocalStatType);
        }
        public static CGPImage LocalStats2(CGPImage A, int ApertureWidth, int ApertureHeight, LocalStat LocalStatType)
        {
            int w = A.Width;
            int h = A.Height;



            IntPtr StrucElem = CreateStructuringElement(ApertureWidth, ApertureHeight);
            CGPImage R = null;
            if (LocalStatType == LocalStat.Min)
            {
                R = new CGPImage(w, h, A.Label + ",LocalStats size = " + ApertureWidth + "x" + ApertureHeight + " " + LocalStatType.ToString());
                CvInvoke.cvErode(
                    A.Img.Ptr,
                    R.Img.Ptr,
                    StrucElem,
                    1);
                return R;
            }
            else if (LocalStatType == LocalStat.Max)
            {
                R = new CGPImage(w, h, A.Label + ",LocalStats size = " + ApertureWidth + "x" + ApertureHeight + " " + LocalStatType.ToString());
                CvInvoke.cvDilate(
                   A.Img.Ptr,
                   R.Img.Ptr,
                   StrucElem,
                   1);
                return R;
            }
            else if (LocalStatType == LocalStat.Range)
            {
                CGPImage LMinImage = LocalStats2(A, ApertureWidth, ApertureHeight, LocalStat.Min);
                CGPImage LMaxImage = LocalStats2(A, ApertureWidth, ApertureHeight, LocalStat.Max);
                CGPImage R2 = CGPImageFunctions.AbsDifference(LMinImage, LMaxImage);
                R2.Label = A.Label + ",LocalStats size = " + ApertureWidth + "x" + ApertureHeight + " " + LocalStatType.ToString();
                return R2;
            }

            return null;
        }


        public static CGPImage CompareLocalStatsToGlobal(CGPImage A, int ApertureWidth, int ApertureHeight, LocalStat LocalStatType)
        {
            int w = A.Width;
            int h = A.Height;
            CGPImage R = new CGPImage(w, h, A.Label + ", CompareLocalStatsToGlobal size = " + ApertureWidth + "x" + ApertureHeight + " " + LocalStatType.ToString());
            if (ApertureHeight < 1) ApertureHeight = 1;
            else if (ApertureHeight > 20) ApertureHeight = 20;
            if (ApertureWidth < 1) ApertureWidth = 1;
            else if (ApertureWidth > 20) ApertureWidth = 20;
            double GlobalMin; double GlobalMax;

            CGPImageFunctions.MinMax(A, out GlobalMin, out GlobalMax);
            double GlobalRange = GlobalMax - GlobalMin;
            double GlobalMid = (GlobalMax + GlobalMin) / 2;

            if (LocalStatType == LocalStat.Average)
            {
                float Sum = 0;
                int ValuesLength = ((ApertureWidth * 2) + 1) * ((ApertureHeight * 2) + 1);
                for (int i = ApertureWidth; i < w - ApertureWidth; i++)
                    for (int j = ApertureHeight; j < h - ApertureHeight; j++)
                    {

                        Sum = 0;
                        for (int x = i - ApertureWidth; x <= i + ApertureWidth; x++)
                            for (int y = j - ApertureHeight; y <= j + ApertureHeight; y++)
                            {
                                Sum += A.Img.Data[y, x, 0];

                            }

                        R.Img.Data[j, i, 0] = Sum / ValuesLength;

                    }

            }
            else
            {
                for (int i = ApertureWidth; i < w - ApertureWidth; i++)
                    for (int j = ApertureHeight; j < h - ApertureHeight; j++)
                    {
                        float MinValue = float.MaxValue;
                        float MaxValue = float.MinValue;
                        float v = 0;
                        for (int x = i - ApertureWidth; x <= i + ApertureWidth; x++)
                            for (int y = j - ApertureHeight; y <= j + ApertureHeight; y++)
                            {
                                v = A.Img.Data[y, x, 0];
                                if (MinValue > v)
                                    MinValue = v;
                                else
                                    if (MaxValue < v)
                                        MaxValue = v;
                            }

                        switch (LocalStatType)
                        {
                            case (LocalStat.Max):
                                R.Img.Data[j, i, 0] = MaxValue;
                                break;
                            case (LocalStat.Min):
                                R.Img.Data[j, i, 0] = MinValue;
                                break;
                            case (LocalStat.Range):
                                R.Img.Data[j, i, 0] = MaxValue - MinValue;
                                break;
                        }


                    }

            }


            return R;
        }


        public static int FixRange(int Value, int Min, int Max)
        {
            if (Value < Min) return Min;
            if (Value > Max) return Max;
            return Value;
        }

        public static CGPImage Feature(CGPImage A, int FeatureType, bool Invert, bool Tilted, int Size, double Threshold)
        {
            CGPImage R = new CGPImage(A.Width, A.Height);
            Threshold = 0;
            Size = 6;
            //if (Size < 6) Size =6;
            //if (Size > 6) Size = 6;
            int HalfSize = Size / 2;
            int QuaterSize = Size / 4;
            int ThirdSize = Size / 3;
            int SixthSize = Size / 6;

            A.ComputeIntegralImages();

            int w = A.Width; int h = A.Height;

            double AreaA = 0; double AreaB = 0;

            // for (int x = HalfSize; x < w - HalfSize - 1; x++)
            for (int x = 0; x < w; x++)
            {
                //for (int y = HalfSize; y < h - HalfSize - 1; y++)
                for (int y = 0; y < h; y++)
                {

                    switch (FeatureType)
                    {
                        case (0):
                            AreaA = A.GetIntegral(x - HalfSize, y - HalfSize, Size, Size, Tilted ? A.IntTiltedSum : A.IntSum);
                            AreaB = A.GetIntegral(x - SixthSize, y - SixthSize, ThirdSize, ThirdSize, Tilted ? A.IntTiltedSum : A.IntSum);
                            AreaA -= AreaB;
                            if ((AreaB * 3) - AreaA > Threshold)
                                R.Img.Data[y, x, 0] = 255;
                            break;
                        case (1):
                            AreaA = A.GetIntegral(x - HalfSize, y - HalfSize, HalfSize, Size, Tilted ? A.IntTiltedSum : A.IntSum);
                            AreaB = A.GetIntegral(x, y - HalfSize, HalfSize, Size, Tilted ? A.IntTiltedSum : A.IntSum);
                            if ((AreaB * 1) - AreaA > Threshold)
                                R.Img.Data[y, x, 0] = 255;
                            break;
                        case (2):
                            AreaA = A.GetIntegral(x - HalfSize, y - HalfSize, Size, HalfSize, Tilted ? A.IntTiltedSum : A.IntSum);
                            AreaB = A.GetIntegral(x - HalfSize, y, Size, HalfSize, Tilted ? A.IntTiltedSum : A.IntSum);
                            if ((AreaB * 1) - AreaA > Threshold)
                                R.Img.Data[y, x, 0] = 255;
                            break;
                        case (3):
                            AreaA = A.GetIntegral(x - HalfSize, y - HalfSize, Size, Size, Tilted ? A.IntTiltedSum : A.IntSum);
                            AreaB = A.GetIntegral(x - HalfSize, y - HalfSize, ThirdSize * 2, Size, Tilted ? A.IntTiltedSum : A.IntSum);
                            if (AreaB - (AreaA * 2) > Threshold)
                                R.Img.Data[y, x, 0] = 255;
                            break;
                        case (4):
                            AreaA = A.GetIntegral(x - HalfSize, y - HalfSize, Size, Size, Tilted ? A.IntTiltedSum : A.IntSum);
                            AreaB = A.GetIntegral(x - HalfSize, y - SixthSize, Size, ThirdSize, Tilted ? A.IntTiltedSum : A.IntSum);
                            AreaA -= AreaB;
                            if ((AreaB * 2) - AreaA > Threshold)
                                R.Img.Data[y, x, 0] = 255;
                            break;
                        case (5):
                            AreaA = A.GetIntegral(x - HalfSize, y - HalfSize, Size, Size, Tilted ? A.IntTiltedSum : A.IntSum);
                            AreaB = A.GetIntegral(x - HalfSize, y - HalfSize, HalfSize, HalfSize, Tilted ? A.IntTiltedSum : A.IntSum) +
                                A.GetIntegral(x, y, HalfSize, HalfSize, Tilted ? A.IntTiltedSum : A.IntSum);
                            AreaA -= AreaB;
                            if (AreaB - AreaA > Threshold)
                                R.Img.Data[y, x, 0] = 255;
                            break;
                        default:
                            throw new Exception("Unknown feature type");
                    }
                }
            }
            if (Invert)
                R.Img._Not();
            return R;
        }

        public static CGPImage oldFeature(CGPImage A, int FeatureType, bool Invert, bool Tilted, int Size, double Threshold)
        {
            if (Size < 6) Size = 6;
            if (Size > 24) Size = 24;
            A.ComputeIntegralImages();
            CGPImage R = new CGPImage(A.Width, A.Height);
            int HalfSize = Size / 2;
            int QuaterSize = Size / 4;
            int ThirdSize = Size / 3;
            int SixthSize = Size / 6;
            // Threshold = 0;
            int w = A.Width; int h = A.Height;
            for (int x = HalfSize; x < w - HalfSize - 1; x++)
            {
                for (int y = HalfSize; y < h - HalfSize - 1; y++)
                {
                    double SumA = A.GetIntegral(x - HalfSize, y - HalfSize, Size, Size, Tilted ? A.IntTiltedSum : A.IntSum);

                    double SumB = 0;
                    double SumBScale = 0;

                    switch (FeatureType)
                    {
                        case (0):
                            SumB = A.GetIntegral(x - HalfSize, y - HalfSize, HalfSize, Size, Tilted ? A.IntTiltedSum : A.IntSum);
                            SumBScale = 2;
                            break;
                        case (1):
                            SumB = A.GetIntegral(x - HalfSize, y - HalfSize, Size, HalfSize, Tilted ? A.IntTiltedSum : A.IntSum);
                            SumBScale = 2;
                            break;
                        case (2):
                            if (SixthSize == 0 || ThirdSize == 0) throw new Exception("Filter is too small");
                            SumB = A.GetIntegral(x - SixthSize, y - HalfSize, ThirdSize, Size, Tilted ? A.IntTiltedSum : A.IntSum);
                            SumBScale = 3;
                            break;
                        case (3):
                            if (QuaterSize == 0) throw new Exception("Filter is too small");
                            SumB = A.GetIntegral(x - QuaterSize, y - HalfSize, HalfSize, Size, Tilted ? A.IntTiltedSum : A.IntSum);
                            SumBScale = 2;
                            break;
                        case (4):
                            if (QuaterSize == 0) throw new Exception("Filter is too small");
                            SumB = A.GetIntegral(x - HalfSize, y - QuaterSize, Size, HalfSize, Tilted ? A.IntTiltedSum : A.IntSum);
                            SumBScale = 2;
                            break;
                        case (5):

                            SumB = A.GetIntegral(x - HalfSize, y - HalfSize, HalfSize, HalfSize, Tilted ? A.IntTiltedSum : A.IntSum)
                                + A.GetIntegral(x, y, HalfSize, HalfSize, Tilted ? A.IntTiltedSum : A.IntSum);
                            SumBScale = 2;
                            break;
                        default:
                            throw new Exception("Unknown feature type");
                    }

                    double AreaA = SumBScale * (SumA - SumB);
                    double AreaB = SumB;

                    if ((!Invert && AreaA - AreaB > Threshold) || (Invert && AreaB - AreaA > Threshold))
                        R.Img.Data[y, x, 0] = 255;
                }
            }

            return R;
        }

        public static CGPImage IF(CGPImage Cond, float Threshold, CGPImage A, CGPImage B)
        {
            CGPImage R = B.Clone();

            for (int x = 0; x < Cond.Width; x++)
                for (int y = 0; y < Cond.Height; y++)
                {
                    if (Cond.Img.Data[y, x, 0] > Threshold)
                        R.Img.Data[y, x, 0] = A.Img.Data[y, x, 0];
                }

            return R;
        }

        public static CGPImage IF2(CGPImage Cond, float Threshold, CGPImage A, CGPImage B)
        {
            double Min;
            double Max;
            CGPImageFunctions.MinMax(Cond, out Min, out Max);
            if ((Min + Max) > Threshold * 2)
                return A.Clone();
            else
                return B.Clone();
        }

        public static CGPImage And(CGPImage A, CGPImage B)
        {
            return new CGPImage(A.Img.And(B.Img));
        }

        public static CGPImage Or(CGPImage A, CGPImage B)
        {
            return new CGPImage(A.Img.Or(B.Img));
        }

        public static CGPImage Xor(CGPImage A, CGPImage B)
        {
            return new CGPImage(A.Img.Xor(B.Img));
        }

        public static CGPImage Not(CGPImage A)
        {
            return new CGPImage(A.Img.Not());
        }

        public static CGPImage Pow(CGPImage A, float v)
        {
            return new CGPImage(A.Img.Pow(v));
        }

        public static CGPImage Rotate(CGPImage A, int Orientation)
        {
            if (Orientation == 0)
                return new CGPImage(A.Img);
            if (Orientation == 1)
                return new CGPImage(A.Img.Rotate(90, new Gray(0), true));
            if (Orientation == 2)
                return new CGPImage(A.Img.Rotate(180, new Gray(0), true));
            if (Orientation == 3)
                return new CGPImage(A.Img.Rotate(270, new Gray(0), true));
            throw new Exception("Unknown orientation");
        }

        public static CGPImage LocalNormalize(CGPImage A, int Apeture)
        {

            IntPtr StrucElem = CreateStructuringElement(Apeture);
            Image<Gray, float> MinImage = new Image<Gray, float>(A.Width, A.Height);
            CvInvoke.cvErode(
                A.Img.Ptr,
                MinImage.Ptr,
                StrucElem,
                1);
            Image<Gray, float> MaxImage = new Image<Gray, float>(A.Width, A.Height);
            CvInvoke.cvDilate(
                A.Img.Ptr,
                MaxImage.Ptr,
                StrucElem,
                1);

            Image<Gray, float> Diff = MaxImage - MinImage;
            Image<Gray, float> N = (A.Img - MinImage);

            CvInvoke.cvDiv(N.Ptr, Diff.Ptr, N.Ptr, 255d);

            return new CGPImage(N, A.Label + ", Local normalize, aperture " + Apeture);
        }

        public static CGPImage LocalNormalizeWithGauss(CGPImage A, int r0, int r1)
        {
            Image<Gray, float> G = A.Img.Mul(1d / 255d);
            Image<Gray, float> blur = G.SmoothGaussian(0, 0, r0, r0);
            Image<Gray, float> num = G - blur;
            blur = G.SmoothGaussian(0, 0, r1, r1);
            Image<Gray, float> den = blur.Pow(0.5);
            //Image<Gray, float> r = num. den;
            CvInvoke.cvDiv(num.Ptr, den.Ptr, G.Ptr, 1);
            CvInvoke.cvNormalize(G.Ptr, blur.Ptr, 0, 255, Emgu.CV.CvEnum.NORM_TYPE.CV_MINMAX, IntPtr.Zero);


            CGPImage R = new CGPImage(blur);
            R.Label = A.Label + ", LocalNormalizeWithGauss " + r0 + "," + r1 + " ";
            return R;
        }

        public static CGPImage SmoothBlur(CGPImage A, int Width, int Height)
        {
            return new CGPImage(A.Img.SmoothBlur(Width, Height));
        }



        public static CGPImage GrayLevelFeature(CGPImage A, int FeatureType, int DistanceX, int DistanceY)
        {
            int QuantizeLevel = 8;
            A.ComputeHistogram(QuantizeLevel);

            FeatureType = Math.Abs(FeatureType) % 5;

            CGPImage R = new CGPImage(A.Width, A.Height);

            for (int x = 0; x < A.Width; x++)
                for (int y = 0; y < A.Height; y++)
                {
                    if (x + DistanceX < 0 || x + DistanceX >= A.Width || y + DistanceY < 0 || y + DistanceY >= A.Height)
                        continue;

                    float Quantized1 = A.Img.Data[y, x, 0];
                    if (Quantized1 < 0) Quantized1 = 0;
                    else if (Quantized1 > 255) Quantized1 = 255;
                    Quantized1 /= (QuantizeLevel + 1);

                    float Quantized2 = A.Img.Data[y + DistanceY, x + DistanceX, 0];
                    if (Quantized2 < 0) Quantized2 = 0;
                    else if (Quantized2 > 255) Quantized2 = 255;
                    Quantized2 /= (QuantizeLevel + 1);

                    if (Quantized1 >= QuantizeLevel) Quantized1 = QuantizeLevel;
                    if (Quantized2 >= QuantizeLevel) Quantized2 = QuantizeLevel;

                    float p1 = A.Histogram[(int)Quantized1];
                    float p2 = A.Histogram[(int)Quantized2];

                    float v = 0;
                    switch (FeatureType)
                    {
                        case (0):
                            v = (Quantized1 - Quantized2) * (p1 * p2);
                            break;
                        case (1):
                            v = (p1 * p2) * (p1 * p2);
                            break;
                        case (2):
                            v = (p1 * p2) * (float)Math.Log((p1 * p2));
                            break;
                        case (3):
                            v = (Quantized1 - Quantized2) * QuantizeLevel;
                            break;
                        case (4):
                            v = (Quantized1 - Quantized2) * QuantizeLevel;
                            if (v < 0) v *= -1;
                            break;
                        default:
                            throw new Exception("Unknown feature type!");
                    }
                    R.Img.Data[y, x, 0] = v;
                }

            return R;
        }

        public static CGPImage Abs(CGPImage A)
        {
            CGPImage R = A.Clone();

            for (int i = 0; i < A.Width; i++)
                for (int j = 0; j < A.Height; j++)
                    if (A.Img.Data[j, i, 0] < 0)
                        R.Img.Data[j, i, 0] = -1f * A.Img.Data[j, i, 0];
            return R;
        }

        public static CGPImage AbsThreshold(CGPImage A, float V)
        {
            return CGPImageFunctions.Abs(CGPImageFunctions.Sub(A, V));
        }

        public static CGPImage OtsuThreshold(CGPImage A, float Threshold)
        {
            Image<Gray, byte> B = A.Img.Convert<Gray, byte>();
            Image<Gray, byte> R = new Image<Gray, byte>(A.Width, A.Height);
            CvInvoke.cvThreshold(B.Ptr, R.Ptr, Threshold, 255, Emgu.CV.CvEnum.THRESH.CV_THRESH_OTSU);
            return new CGPImage(R.Convert<Gray, float>());
        }

        public static CGPImage Convolve(CGPImage A, ConvolutionKernelF K)
        {
            return new CGPImage(A.Img.Convolution(K));
        }

        public static Dictionary<int, ConvolutionKernelF> Kernels = null;

        public static void LoadKernels()
        {
            Kernels = new Dictionary<int, ConvolutionKernelF>();

            string[] KernelFiles = Directory.GetFiles(".", "*.kernel");

            foreach (string F in KernelFiles)
            {
                string Lines = File.ReadAllText(F);
                string[] Tokens = Lines.Split(new char[] { ',', '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries
                    );
                int KWidth = int.Parse(Tokens[0]);
                int KHeight = int.Parse(Tokens[1]);
                int KID = int.Parse(Tokens[2]);
                ConvolutionKernelF K = new ConvolutionKernelF(KHeight, KWidth);
                int i = 0;

                float Min = float.MaxValue;
                float Max = float.MinValue; ;
                for (int x = 0; x < KWidth; x++)
                    for (int y = 0; y < KHeight; y++)
                    {
                        K[y, x] = float.Parse(Tokens[i + 3]);
                        if (K[y, x] > Max) Max = K[y, x];
                        if (K[y, x] < Min) Min = K[y, x];
                    }

                for (int x = 0; x < KWidth; x++)
                    for (int y = 0; y < KHeight; y++)
                    {
                        K[y, x] = (((K[y, x] - Min) / (Max - Min)) * 2f) - 1;
                    }


                K.Center = new Point(K.Width / 2, K.Height / 2);

                Kernels.Add(KID, K);
            }
            if (Kernels.Count == 0)
                throw new Exception("No kernels found");
        }

        public static CGPImage Convolve(CGPImage A, int ID)
        {
            if (Kernels == null)
                LoadKernels();
            return Convolve(A, Kernels[ID]);
        }

        public static CGPImage EdgeDirection(CGPImage A)
        {
            Image<Gray, float> dx = A.Img.Sobel(0, 1, 3);
            Image<Gray, float> dy = A.Img.Sobel(1, 0, 3);
            Image<Gray, float> d = new Image<Gray, float>(A.Width, A.Height);
            int w = A.Width;
            int h = A.Height;
            unchecked
            {
                for (int x = 0; x < w; x++)
                    for (int y = 0; y < h; y++)
                    {
                        d.Data[y, x, 0] = 128f * (float)Math.Atan2(dx.Data[y, x, 0], dy.Data[y, x, 0]);
                    }
            }
            return new CGPImage(d);
        }

        public static CGPImage EdgeMag(CGPImage A)
        {
            Image<Gray, float> dx = A.Img.Sobel(0, 1, 3);
            Image<Gray, float> dy = A.Img.Sobel(1, 0, 3);
            dx._Mul(dx);
            dy._Mul(dy);
            Image<Gray, float> d = dx + dy;
            CvInvoke.cvSqrt(d.Ptr, d.Ptr);
            return new CGPImage(d);
        }

        public static CGPImage Quantize(CGPImage A, int Levels)
        {
            if (Levels < 2) Levels = 2;
            else if (Levels > 16) Levels = 16;

            CGPImage R = new CGPImage(A.Width, A.Height);
            double[] Min; double[] Max;
            Point[] MinP; Point[] MaxP;
            A.Img.MinMax(out Min, out Max, out MinP, out MaxP);
            int w = A.Width;
            int h = A.Height;

            unchecked
            {
                for (int x = 0; x < w; x++)
                    for (int y = 0; y < h; y++)
                    {
                        float v = A.Img.Data[y, x, 0];
                        double v2 = (v - Min[0]) / (Max[0] - Min[0]);
                        float v3 = (int)((double)Levels * v2);
                        R.Img.Data[y, x, 0] = (256 / Levels) * (float)v3;
                    }
            }

            return R;
        }

        public static CGPImage DrawBlobs(CGPImage A, int DiscardSizeMin, bool PruneOverlaps, int MaxDrawing, double ScaleBlobs)
        {
            List<Rectangle> B = Blobs(A, DiscardSizeMin, PruneOverlaps,-1, ScaleBlobs);
            CGPImage R = A.Clone();// new CGPImage(A.Width, A.Height);

            Gray C = new Gray(255);
            for (int i = 0; i < B.Count; i++)
            {
                R.Img.Draw(B[i], C, 1);
                if (MaxDrawing >= 0 && i > MaxDrawing)
                    continue;
            }

            return R;
        }

        public static List<Rectangle> Blobs(CGPImage A, int DiscardSizeMin, bool FixOverlaps, int MaxSizeForOverlap, double ScaleBlobs)
        {
            Image<Gray, byte> B = A.Img.Convert<Gray, byte>();
            return Blobs(B, DiscardSizeMin, FixOverlaps,MaxSizeForOverlap, ScaleBlobs);
        }

        public static List<Rectangle> Blobs(Image<Gray, byte> A, int DiscardSizeMin, bool FixOverlaps, int MaxSizeForOverlap, double ScaleBlobs)
        {
            List<Rectangle> B = new List<Rectangle>();

            using (MemStorage storage = new MemStorage())
            {
                for (Contour<Point> contours = A.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_LIST, storage); contours != null; contours = contours.HNext)
                {
                    Contour<Point> currentContour = contours.ApproxPoly(contours.Perimeter * 0.05, storage);
                    Rectangle BB = currentContour.BoundingRectangle;

                    if (DiscardSizeMin > 1 && (BB.Width < DiscardSizeMin && BB.Height < DiscardSizeMin))
                        continue;

                    if (ScaleBlobs > 0)
                    {
                        double ScaleROI = ScaleBlobs;// Parameters.SmartChunkScale;
                        int NewWidth = (int)(ScaleROI * BB.Width);
                        int NewHeight = (int)(ScaleROI * BB.Height);
                        int NewX = BB.X - ((NewWidth - BB.Width) / 2);
                        int NewY = BB.Y - ((NewHeight - BB.Height) / 2);
                        if (NewX < 0) NewX = 0;
                        if (NewY < 0) NewY = 0;
                        if (NewX + NewWidth >= A.Width)
                            NewWidth = A.Width - 1 - NewX;
                        if (NewY + NewHeight >= A.Height)
                            NewHeight = A.Height - 1 - NewY;
                        B.Add(new Rectangle(NewX, NewY, NewWidth, NewHeight));
                    }
                    else
                    {
                        B.Add(BB);
                    }
                }
            }

            if (FixOverlaps)
            {
                for (int i = 0; i < B.Count; i++)
                    for (int j = 0; j < B.Count; j++)
                    {
                        if (i == j)
                            continue;
                        if (B[i].IntersectsWith(B[j]))
                        {
                            Rectangle N = new Rectangle(
                                Math.Min(B[i].X, B[j].X),
                                Math.Min(B[i].Y, B[j].Y),
                                Math.Max(B[i].Right, B[j].Right) - Math.Min(B[i].X, B[j].X),
                                Math.Max(B[i].Bottom, B[j].Bottom) - Math.Min(B[i].Y, B[j].Y));

                            if (MaxSizeForOverlap >= 0 && N.Width < MaxSizeForOverlap && N.Height < MaxSizeForOverlap)
                            {
                                B.RemoveAt(i);
                                if (j > i) B.RemoveAt(j - 1); else B.RemoveAt(j);
                                i = 0; j = 0;
                                B.Add(N);
                            }
                        }
                    }
            }

            return B;
        }


        public static CGPImage Rotate(CGPImage A, double Angle)
        {
            CGPImage R = new CGPImage(A.Img.Rotate(Angle, new Gray(0), true));
            R.Label += ",Rotate=" + Angle + "°";
            return R;
        }

        public static CGPImage Crop(CGPImage A, int TX, int TY, int BX, int BY)
        {
            CGPImage B = new CGPImage(BX - TX, BY - TY);
            for (int x = TX; x < BX;x++ )
                for (int y = TY; y < BY; y++)
                {
                    B.Img.Data[y - TY, x - TX, 0] = A.Img.Data[y, x, 0];
                }
                return B;
        }

        public enum SurfaceMeasurementType { Ra, Rq, Rv, Rp, Rt, Rsk, Rku, Rz , Rent};

        public static CGPImage SurfaceMeasurement(CGPImage A, int WindowSize, SurfaceMeasurementType MeasurementType)
        {
            if (WindowSize < 2) WindowSize = 2;
            if (MeasurementType == SurfaceMeasurementType.Rz && WindowSize < 4) WindowSize = 4;
            if (WindowSize > 16) WindowSize = 16;
            if (WindowSize % 2 == 0) WindowSize++;
            int StepSize = WindowSize/2;

            CGPImage R = new CGPImage(A.Width , A.Height , MeasurementType.ToString());
            float[] Values = new float[WindowSize * WindowSize];

            int w = A.Width;
            int h = A.Height;

            int ValuesLength = WindowSize * WindowSize;
            for (int x = (WindowSize / 2); x < w - (WindowSize / 2); x += StepSize)
                for (int y = (WindowSize / 2); y < h - (WindowSize / 2); y += StepSize)
                {
                    int p = 0;
                    for (int i = x - (WindowSize / 2); i < x + (WindowSize / 2); i++)
                        for (int j = y - (WindowSize / 2); j < y + (WindowSize / 2); j++)
                        {
                            Values[p] = A.Img.Data[j % h, i % w,0];
                            p++;
                        }

                    float V = 0;

                    switch(MeasurementType)
                    {
                        case(SurfaceMeasurementType.Ra):
                            for (int i = 0; i < ValuesLength; i++)
                                V += Math.Abs(Values[i]);
                            V /= ValuesLength;
                            break;
                        case (SurfaceMeasurementType.Rq):
                            for (int i = 0; i < ValuesLength; i++)
                                V += Values[i] * Values[i];
                            V = (float)Math.Sqrt(V/ValuesLength);
                            break;
                        case (SurfaceMeasurementType.Rv):
                            float M = Values.Average();
                            for (int i = 0; i < ValuesLength; i++)
                                if (M - Values[i] > V)
                                    V = M - Values[i];
                            break;
                        case (SurfaceMeasurementType.Rp):
                            float M2 = Values.Average();
                            for (int i = 0; i < ValuesLength; i++)
                                if ( Values[i]-M2 > V)
                                    V =Values[i] -  M2;
                            break;
                        case (SurfaceMeasurementType.Rt):
                            float M3 = Values.Average();
                            float rv = 0;
                            float rp = 0;
                            for (int i = 0; i < ValuesLength; i++)
                            {
                                if (Values[i] - M3 > rp)
                                    rp = Values[i] - M3;
                                if (M3 - Values[i] > rv)
                                    rv = M3- Values[i];
                            }
                            V = rp + rv;
                            break;
                        case(SurfaceMeasurementType.Rsk):
                            double AccRSK = 0;
                            double rq = 0;
                             for (int i = 0; i < ValuesLength; i++)
                                 rq += Values[i] * Values[i];
                             rq = Math.Sqrt(V / ValuesLength);
                            for (int i = 0; i < ValuesLength; i++)
                                {
                                AccRSK += Math.Pow(Values[i], 3);
                                }
                            AccRSK *= 1d / (Math.Pow(rq, 3) * ValuesLength);
                            V = (float)AccRSK;
                            break;
                        case (SurfaceMeasurementType.Rku):
                            double AccRku = 0;
                            double rq2 = 0;
                            for (int i = 0; i < ValuesLength; i++)
                                rq2 += Values[i] * Values[i];
                            rq2 = Math.Sqrt(V / ValuesLength);
                            for (int i = 0; i < ValuesLength; i++)
                            {
                                AccRku += Math.Pow(Values[i], 4);
                            }
                            AccRku *= 1d / (Math.Pow(rq2, 4) * ValuesLength);
                            V = (float)AccRku;
                            break;
                        case(SurfaceMeasurementType.Rz):
                            if (ValuesLength < 10)
                            {
                                V = 0;
                            }                                
                            else
                            {
                                Array.Sort(Values);
                                V = 0;
                                for (int i = 0; i < 5; i++)
                                {
                                    V += Values[ValuesLength - 1 - i] - Values[i];
                                }

                                V /= 5;
                            }
                            break;
                        case(SurfaceMeasurementType.Rent):
                            Dictionary<int, double> Hist = new Dictionary<int, double>();
                            for (int i = 0; i < ValuesLength; i++)
                                if (Hist.ContainsKey((int)Values[i]))
                                    Hist[(int)Values[i]]++;
                                else
                                    Hist.Add((int)Values[i], 1);
                                double Total = Hist.Values.Sum();
                                double ent = 0;
                                foreach (int k in Hist.Keys)
                                {
                                    ent += (Hist[k] / Total) * Math.Log(Hist[k] / Total, 2);
                                }
                                V = -1 * (float)ent;
                                break;
                        default:
                            throw new NotImplementedException();
                    }

                    //R.Img.Draw(new Rectangle(x, y, WindowSize, WindowSize), new Gray(V), -1);
                    R.Img.Draw(new Rectangle(x - (StepSize / 2), y - (StepSize / 2), StepSize, StepSize), new Gray(V), -1);
                    //if (y / CellSize<R.Height && x/CellSize<R.Width)
                   // R.Img.Data[y / CellSize, x / CellSize, 0] = V;

                }

                return R;

        }

        public static CGPImage LocalMajority(CGPImage A, int WindowSize, int StepSize, float Threshold)
        {
            if (WindowSize < 2) WindowSize = 2;
            
            if (WindowSize > 16) WindowSize = 16;
            if (WindowSize % 2 == 0) WindowSize++;
            
            if (StepSize > WindowSize) StepSize = WindowSize;

            CGPImage R = new CGPImage(A.Width, A.Height, "LocalMajority");

            int Count = 0;

            int w = A.Width;
            int h = A.Height;

            int ValuesLength = WindowSize * WindowSize;
            for (int x = (WindowSize / 2); x < w - (WindowSize / 2); x += StepSize)
                for (int y = (WindowSize / 2); y < h - (WindowSize / 2); y += StepSize)
                {
                    int p = 0;
                    for (int i = x - (WindowSize / 2); i < x + (WindowSize / 2); i++)
                        for (int j = y - (WindowSize / 2); j < y + (WindowSize / 2); j++)
                        {
                            float v  = A.Img.Data[j % h, i % w, 0];
                            if (v >= Threshold)
                                Count++;
                        }

                    float V = 0;
                    if (Count > (WindowSize * WindowSize) / 2)
                        V = 255;
                    
                    R.Img.Draw(new Rectangle(x - (StepSize / 2), y - (StepSize / 2), StepSize, StepSize), new Gray(V), -1);
                    
                }

            return R;

        }
    }

    public class ColorAndFrequency : IComparable<ColorAndFrequency>
    {
        public int Color;
        public double Frequency;

        public ColorAndFrequency() { }
        public ColorAndFrequency(int Color, double Frequency) { this.Color = Color; this.Frequency = Frequency; }

        int IComparable<ColorAndFrequency>.CompareTo(ColorAndFrequency other)
        {
            if (this.Frequency == other.Frequency) return 0;
            if (this.Frequency > other.Frequency) return -1;
            return 1;
        }
    }
}