﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.IO;


namespace CGPIP2
{
    public class CGPGraphRunner
    {
        public static Stopwatch ExecutionTimer = new Stopwatch();
        
        public void FindNodesToProcess(CGPGraph Graph,
           int OutputCount,
           out int EvaluatedNodeCount

           )
        {
            EvaluatedNodeCount = 0;


            for (int n = 0; n < Graph.Width; n++)
                Graph.Nodes[n].Evaluated = false;

            for (int i = 0; i < OutputCount; i++)
            {


                Graph.Nodes[Graph.Width - 1 - i].Evaluated = true;
                

            }

            for (int i = Graph.Width - 1; i >= 0; i--)
            {
                CGPNode N = Graph.Nodes[i];

                if (N.Evaluated)
                {

                    int Arity = N.Arity;
                    int ConnectTo0 = i - N.Connection0;
                    int ConnectTo1 = i - N.Connection1;
                    int ConnectTo2 = i - N.Connection2;

                    if (ConnectTo0 >= 0 && Arity > 0)
                        if (Graph.Nodes[ConnectTo0] != null)
                        {
                            Graph.Nodes[ConnectTo0].Evaluated = true;
                            
                        }
                    if (ConnectTo1 >= 0 && Arity > 1)
                        if (Graph.Nodes[ConnectTo1] != null)
                        {
                            Graph.Nodes[ConnectTo1].Evaluated = true;
                            
                        }
                    if (ConnectTo2 >= 0 && Arity > 2)
                        if (Graph.Nodes[ConnectTo2] != null)
                        {
                            Graph.Nodes[ConnectTo2].Evaluated = true;
                           
                        }

                }
            }

            for (int i = Graph.Width - 1; i >= 0; i--)
            {
                CGPNode N = Graph.Nodes[i];
                if (N.Evaluated)
                    EvaluatedNodeCount++;
            }
        }

        public CGPImage[] NodeCache = null;
        public CGPImage[] Inputs = null;
        private int GraphWidth;
        public double Duration;

        public CGPImage GetOutput()
        {
            return this.NodeCache[this.GraphWidth - 1];
        }

        public CGPImage GetNodeCacheValue(int NodeIndex, string Tag, out int InputIndexUsed, out string TraceText)
        {
            if (NodeIndex < 0)
            {
                int InputIndex = ((NodeIndex * -1) - 1) % Parameters.InputCount;
                InputIndexUsed = InputIndex;
                TraceText = String.Format("Inputs[{1}][{0:00}]", InputIndex, Tag);
                return this.Inputs[InputIndex];
            }

            InputIndexUsed = -1;
            TraceText = String.Format("Node_" + Tag + "_{0:000}", NodeIndex);
            return NodeCache[NodeIndex];
        }
        int RunCounter = 0;
        public void Run(CGPGraph Graph, CGPImage[] Inputs, int TestCaseIndex, string Tag)
        {
            this.GraphWidth = Graph.Width;
            this.NodeCache = new CGPImage[Graph.Width];
            this.Inputs = Inputs;
            this.Duration = 0;

        
            if (Parameters.ShowTrace && RunCounter == 0)
            {
                Reporting.Say("*******************");
            }

            if (Parameters.ShowTrace && RunCounter == 0)
            {
                if (!Directory.Exists(Parameters.TraceOutputFolder))
                    Directory.CreateDirectory(Parameters.TraceOutputFolder);

                for (int i = 0; i < Inputs.Length; i++)
                {
                    string InputFileName = String.Format("{2}//Inputs[{1}][{0:00}].png", i, Tag, Parameters.TraceOutputFolder);
                    InputFileName = InputFileName.Replace("[","_").Replace("]","_");
                    Inputs[i].Save(InputFileName);
                }
            }

            ExecutionTimer.Start();

            Stopwatch Timer = new Stopwatch();
            Timer.Start();

            List<string> NodesUsed = new List<string>();


            for (int NodeIndex = 0; NodeIndex < Graph.Width; NodeIndex++)
            {


                CGPNode CurrentNode = Graph.Nodes[NodeIndex];

                if (CurrentNode == null || !CurrentNode.Evaluated)
                    continue;
                if (!Parameters.AllowRepeats)
                 CurrentNode.Repeat = 1;
                for (int Repeat = 0; Repeat < CurrentNode.Repeat; Repeat++)
                {

                    string TraceText = "";
                    if (Parameters.ShowTrace)
                    {
                        TraceText = String.Format("CGPImage Node_" + Tag + "_{0:000} = ", NodeIndex);
                    }


                    NodesUsed.Add(String.Format("Node_" + Tag + "_{0:000}", NodeIndex));

                    CGPNode CurrentSuperNode = Graph.Nodes[NodeIndex];

                    int ConnectTo0 = NodeIndex - CurrentSuperNode.Connection0;
                    int ConnectTo1 = NodeIndex - CurrentSuperNode.Connection1;
                    int ConnectTo2 = NodeIndex - CurrentSuperNode.Connection2;
                    int Arity = CurrentSuperNode.Arity;

                    CGPFunction Func = CurrentNode.Function;

                    CGPImage Input0 = null;
                    CGPImage Input1 = null;
                    CGPImage Input2 = null;

                    int InputIndexUsedInput0 = -1;
                    int InputIndexUsedInput1 = -1;
                    int InputIndexUsedInput2 = -1;
                    string Input0Text = "";
                    string Input1Text = "";
                    string Input2Text = "";

                    if (Arity > 0)
                    {
                        Input0 = this.GetNodeCacheValue(ConnectTo0, Tag, out InputIndexUsedInput0, out Input0Text);
                        NodesUsed.Add(Input0Text);
                    }
                    if (Arity > 1)
                    {
                        Input1 = this.GetNodeCacheValue(ConnectTo1, Tag, out InputIndexUsedInput1, out Input1Text);
                        NodesUsed.Add(Input1Text);
                    }
                    if (Arity > 2)
                    {
                        Input2 = this.GetNodeCacheValue(ConnectTo2, Tag, out InputIndexUsedInput2, out Input2Text);
                        NodesUsed.Add(Input2Text);
                    }

                    if (Parameters.AllowRepeats && Repeat > 0)
                    {
                        Input1 = NodeCache[NodeIndex];
                        ConnectTo1 = NodeIndex;
                        Input1Text = String.Format("Node_" + Tag + "_{0:000}", NodeIndex);
                        if (Arity < 2)
                            break;
                    }

                    /*if (Input1 != null && Input0 != null && (Input1.Width != Input0.Width || Input1.Height != Input0.Height))
                    {
                        Input1 = CGPImageFunctions.Rescale(Input1, Math.Max(Input1.Width, Input0.Width), Math.Max(Input1.Height,Input0.Height));
                    }
                    if (Input2 != null && Input0 != null && (Input2.Width != Input0.Width || Input2.Height != Input0.Height))
                    {
                        //Input2 = CGPImageFunctions.Rescale(Input2, Input0.Width, Input0.Height);
                        Input1 = CGPImageFunctions.Rescale(Input2, Math.Max(Input1.Width, Input2.Width), Math.Max(Input1.Height, Input2.Height));
                    }*/
                    if (Arity > 0)
                    {
                        if (Parameters.ResizeInputsToLargest)
                        {

                            int MaxHeight = Input0.Height;
                            if (Input1 != null && Input1.Height > MaxHeight) MaxHeight = Input1.Height;
                            if (Input2 != null && Input2.Height > MaxHeight) MaxHeight = Input2.Height;
                            int MaxWidth = Input0.Width;
                            if (Input1 != null && Input1.Width > MaxWidth) MaxWidth = Input1.Width;
                            if (Input2 != null && Input2.Width > MaxWidth) MaxWidth = Input2.Width;


                            if (Input0 != null && Input0.Width != MaxWidth && Input0.Height != MaxHeight)
                                Input0 = CGPImageFunctions.Rescale(Input0, MaxWidth, MaxHeight);
                            if (Input1 != null && Input1.Width != MaxWidth && Input1.Height != MaxHeight)
                                Input1 = CGPImageFunctions.Rescale(Input1, MaxWidth, MaxHeight);
                            if (Input2 != null && Input2.Width != MaxWidth && Input2.Height != MaxHeight)
                                Input2 = CGPImageFunctions.Rescale(Input2, MaxWidth, MaxHeight);
                        }
                        else
                        {
                            int MinHeight = Input0.Height;
                            if (Input1 != null && Input1.Height > MinHeight) MinHeight = Input1.Height;
                            if (Input2 != null && Input2.Height > MinHeight) MinHeight = Input2.Height;
                            int MinWidth = Input0.Width;
                            if (Input1 != null && Input1.Width > MinWidth) MinWidth = Input1.Width;
                            if (Input2 != null && Input2.Width > MinWidth) MinWidth = Input2.Width;


                            if (Input0 != null && Input0.Width != MinWidth && Input0.Height != MinHeight)
                                Input0 = CGPImageFunctions.Rescale(Input0, MinWidth, MinHeight);
                            if (Input1 != null && Input1.Width != MinWidth && Input1.Height != MinHeight)
                                Input1 = CGPImageFunctions.Rescale(Input1, MinWidth, MinHeight);
                            if (Input2 != null && Input2.Width != MinWidth && Input2.Height != MinHeight)
                                Input2 = CGPImageFunctions.Rescale(Input2, MinWidth, MinHeight);
                        }
                    }



                    if (Parameters.ShowTrace && RunCounter == 0)
                    {
                        //   Reporting.Say("// v" + v + "n" + i + "\t" + Func + "\t" + ConnectTo0 + "," + ConnectTo1+
                        //     "\t"+CurrentNode.GrayValue+","+CurrentNode.Width+"x"+CurrentNode.Height+","+CurrentNode.GaborFreq+","+CurrentNode.GaborRotation
                        //   + "\t" + InputIndexUsedInput0 + "," + InputIndexUsedInput1);
                    }

                    CGPImage Result = null;

                    switch (Func)
                    {
                        case (CGPFunction.NOP):
                            Result = Input0.Clone();
                            if (Parameters.ShowTrace)
                            {
                                TraceText += Input0Text;
                            }
                            break;
                        case (CGPFunction.CONST):
                            Result = new CGPImage(Parameters.DefaultImageWidth, Parameters.DefaultImageHeight, CurrentNode.GrayValue);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "new CGPImage(" + Parameters.DefaultImageWidth + ", " + Parameters.DefaultImageHeight + ", " + CurrentNode.GrayValue + ");";
                            }
                            break;
                        case (CGPFunction.ADDConst):
                            Result = CGPImageFunctions.Add(Input0, CurrentNode.GrayValue);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Add(" + Input0Text + ", " + CurrentNode.GrayValue + ");";
                            }
                            break;
                        case (CGPFunction.SUBTRACTConst):
                            Result = CGPImageFunctions.Sub(Input0, CurrentNode.GrayValue);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Sub(" + Input0Text + ", " + CurrentNode.GrayValue + ");";
                            }
                            break;
                        case (CGPFunction.MULTIPLYConst):
                            Result = CGPImageFunctions.Mul(Input0, CurrentNode.GrayValue);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Mul(" + Input0Text + ", " + CurrentNode.GrayValue + ");";
                            }
                            break;
                        case (CGPFunction.ADD):
                            Result = CGPImageFunctions.Add(Input0, Input1);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Add(" + Input0Text + ", " + Input1Text + ");";
                            }
                            break;
                        case (CGPFunction.SUBTRACT):
                            Result = CGPImageFunctions.Sub(Input0, Input1);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Sub(" + Input0Text + ", " + Input1Text + ");";
                            }
                            break;
                        case (CGPFunction.MULTIPLY):
                            Result = CGPImageFunctions.Mul(Input0, Input1);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Mul(" + Input0Text + ", " + Input1Text + ");";
                            }
                            break;
                        case (CGPFunction.DIVIDE):
                            Result = CGPImageFunctions.Div(Input0, Input1);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Div(" + Input0Text + ", " + Input1Text + ");";
                            }
                            break;
                        case (CGPFunction.SHIFT):
                            Result = CGPImageFunctions.Shift(Input0, CurrentNode.Width - 16, CurrentNode.Height - 16);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Shift(" + Input0Text + ", " + (CurrentNode.Width - 16) + ", " + (CurrentNode.Height - 16) + ");";
                            }
                            break;
                        case (CGPFunction.SHIFTDOWN):
                            Result = CGPImageFunctions.Shift(Input0, 0, -1);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Shift(" + Input0Text + ", 0, -1);";
                            }
                            break;
                        case (CGPFunction.SHIFTUP):
                            Result = CGPImageFunctions.Shift(Input0, 0, 1);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Shift(" + Input0Text + ", 0, 1);";
                            }
                            break;
                        case (CGPFunction.SHIFTLEFT):
                            Result = CGPImageFunctions.Shift(Input0, -1, 0);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Shift(" + Input0Text + ", -1, 0);";
                            }
                            break;
                        case (CGPFunction.SHIFTRIGHT):
                            Result = CGPImageFunctions.Shift(Input0, 1, 0);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Shift(" + Input0Text + ", 1, 0);";
                            }
                            break;
                        case (CGPFunction.DILATE):
                            Result = CGPImageFunctions.Dilate(Input0, this.IterationsFromNode(CurrentNode));
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Dilate(" + Input0Text + ", " + this.IterationsFromNode(CurrentNode) + ");";
                            }
                            break;
                        case (CGPFunction.ERODE):
                            Result = CGPImageFunctions.Erode(Input0, this.IterationsFromNode(CurrentNode));
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Erode(" + Input0Text + ", " + this.IterationsFromNode(CurrentNode) + ");";
                            }
                            break;
                        case (CGPFunction.THRESHOLD):
                            Result = CGPImageFunctions.Threshold(Input0, CurrentNode.GrayValue);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Threshold(" + Input0Text + ", " + CurrentNode.GrayValue + ");";
                            }
                            break;
                        case (CGPFunction.THRESHOLDINV):
                            Result = CGPImageFunctions.ThresholdInv(Input0, CurrentNode.GrayValue);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.ThresholdInv(" + Input0Text + ", " + CurrentNode.GrayValue + ");";
                            }
                            break;
                        case (CGPFunction.OPEN):
                            Result = CGPImageFunctions.Open(Input0, CurrentNode.Width, CurrentNode.Height, this.IterationsFromNode(CurrentNode));
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Open(" + Input0Text + ", " + CurrentNode.Width + ", " + CurrentNode.Height + ", " + this.IterationsFromNode(CurrentNode) + ");";
                            }
                            break;
                        case (CGPFunction.CLOSE):
                            Result = CGPImageFunctions.Close(Input0, CurrentNode.Width, CurrentNode.Height, this.IterationsFromNode(CurrentNode));
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Close(" + Input0Text + ", " + CurrentNode.Width + ", " + CurrentNode.Height + ", " + this.IterationsFromNode(CurrentNode) + ");";
                            }
                            break;
                        case (CGPFunction.TOPHAT):
                            Result = CGPImageFunctions.TopHat(Input0, CurrentNode.Width, CurrentNode.Height, this.IterationsFromNode(CurrentNode));
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.TopHat(" + Input0Text + ", " + CurrentNode.Width + ", " + CurrentNode.Height + ", " + this.IterationsFromNode(CurrentNode) + ");";
                            }
                            break;
                        case (CGPFunction.BLACKHAT):
                            Result = CGPImageFunctions.BlackHat(Input0, CurrentNode.Width, CurrentNode.Height, this.IterationsFromNode(CurrentNode));
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.BlackHat(" + Input0Text + ", " + CurrentNode.Width + ", " + CurrentNode.Height + ", " + this.IterationsFromNode(CurrentNode) + ");";
                            }
                            break;
                        case (CGPFunction.GRADIENT):
                            Result = CGPImageFunctions.Gradient(Input0, CurrentNode.Width, CurrentNode.Height, this.IterationsFromNode(CurrentNode));
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Gradient(" + Input0Text + ", " + CurrentNode.Width + ", " + CurrentNode.Height + ", " + this.IterationsFromNode(CurrentNode) + ");";
                            }
                            break;
                        case (CGPFunction.ADAPTIVETHRESHOLD):
                            Result = CGPImageFunctions.AdaptiveThreshold(Input0);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.AdaptiveThreshold(" + Input0Text + ");";
                            }
                            break;
                        case (CGPFunction.RESIZE):
                            double Scale = (double)CurrentNode.Width / CurrentNode.Height;
                            Result = CGPImageFunctions.Rescale(Input0, Scale, true);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Rescale(" + Input0Text + ", " + Scale + ");";
                            }
                            break;
                        case (CGPFunction.SMOOTHBILATRAL):
                            Result = CGPImageFunctions.SmoothBilatral(Input0, (int)Math.Abs(CurrentNode.GrayValue) % 16, CurrentNode.Width < 16 ? CurrentNode.Width : 16);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.SmoothBilatral(" + Input0Text + ", " + (int)CurrentNode.GrayValue + ", " + (CurrentNode.Width < 16 ? CurrentNode.Width : 16) + ");";
                            }
                            break;
                        case (CGPFunction.MAX):
                            Result = CGPImageFunctions.Max(Input0, Input1);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Max(" + Input0Text + ", " + Input1Text + ");";
                            }
                            break;
                        case (CGPFunction.MIN):
                            Result = CGPImageFunctions.Min(Input0, Input1);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Min(" + Input0Text + ", " + Input1Text + ");";
                            }
                            break;
                        case (CGPFunction.MAXConst):
                            Result = CGPImageFunctions.Max(Input0, CurrentNode.GrayValue);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Max(" + Input0Text + ", " + CurrentNode.GrayValue + ");";
                            }
                            break;
                        case (CGPFunction.MINConst):
                            Result = CGPImageFunctions.Min(Input0, CurrentNode.GrayValue);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Min(" + Input0Text + ", " + CurrentNode.GrayValue + ");";
                            }
                            break;
#if GABOR
                        case (CGPFunction.GABOR):
                            Result = CGPImageFunctions.Gabor(Input0, CurrentNode.GaborFreq, (short)CurrentNode.Width, (short)CurrentNode.Height, CurrentNode.GaborRotation);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.SmoothGaborBilatral(" + Input0Text + ", " + CurrentNode.GaborFreq + ", " + (short)CurrentNode.Width + ", " + (short)CurrentNode.Height + ", " + CurrentNode.GaborRotation + ");";
                            }
                            break;
#endif
                        case (CGPFunction.ABSDIFF):
                            Result = CGPImageFunctions.AbsDifference(Input0, Input1);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.AbsDifference(" + Input0Text + ", " + CurrentNode.GrayValue + "f);";
                            }
                            break;
                        case (CGPFunction.CANNY):
                            Result = CGPImageFunctions.Canny(Input0, 0.1d);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Canny(" + Input0Text + ", 0.1d);";
                            }
                            break;
                        case (CGPFunction.LAPLACE):
                            Result = CGPImageFunctions.Laplace(Input0, GetApeture(CurrentNode));
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Laplace(" + Input0Text + ", " + GetApeture(CurrentNode) + ");";
                            }
                            break;
                        case (CGPFunction.LOG):
                            Result = CGPImageFunctions.Log(Input0);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Log(" + Input0Text + ");";
                            }
                            break;
                        case (CGPFunction.POW):
                            Result = CGPImageFunctions.Pow(Input0, CurrentNode.GrayValue);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Pow(" + Input0Text + ", " + CurrentNode.GrayValue + "f);";
                            }
                            break;
                        case (CGPFunction.SQRT):
                            Result = CGPImageFunctions.Sqrt(Input0);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Log(" + Input0Text + ");";
                            }
                            break;
                        case (CGPFunction.SOBEL):
                            int SobelXOrder = 0;
                            int SobelYOrder = 0;
                            int SobelOperation = (int)Math.Abs(CurrentNode.GrayValue) % 8;
                            switch (SobelOperation)
                            {
                                case (0):
                                    SobelXOrder = -1;
                                    SobelYOrder = -1;
                                    break;
                                case (1):
                                    SobelXOrder = 0;
                                    SobelYOrder = -1;
                                    break;
                                case (2):
                                    SobelXOrder = 1;
                                    SobelYOrder = -1;
                                    break;
                                case (3):
                                    SobelXOrder = -1;
                                    SobelYOrder = 0;
                                    break;
                                case (4):
                                    SobelXOrder = 1;
                                    SobelYOrder = 0;
                                    break;
                                case (5):
                                    SobelXOrder = 0;
                                    SobelYOrder = 1;
                                    break;
                                case (6):
                                    SobelXOrder = -1;
                                    SobelYOrder = 1;
                                    break;
                                case (7):
                                    SobelXOrder = 1;
                                    SobelYOrder = 1;
                                    break;
                                default:
                                    throw new Exception("Sobel order not known");
                            }

                            Result = CGPImageFunctions.Sobel(Input0, SobelXOrder, SobelYOrder, CurrentNode.Width + CurrentNode.Height);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Sobel(" + Input0Text + ", " + SobelXOrder + "," + SobelYOrder + ", " + (CurrentNode.Width + CurrentNode.Height) + ");";
                            }
                            break;
                        case (CGPFunction.SCALE):
                            Result = CGPImageFunctions.Rescale(Input0, (double)CurrentNode.Width / CurrentNode.Height, true);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Rescale(" + Input0Text + ", " + (double)CurrentNode.Width / CurrentNode.Height + ", true);";
                            }
                            break;
                        case (CGPFunction.EQ):
                            Result = CGPImageFunctions.Eq(Input0);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Eq(" + Input0Text + ");";
                            }
                            break;
                        case (CGPFunction.MINVALUE):
                            Result = CGPImageFunctions.Min(Input0);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Min(" + Input0Text + ");";
                            }
                            break;
                        case (CGPFunction.MAXVALUE):
                            Result = CGPImageFunctions.Max(Input0);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Max(" + Input0Text + ");";
                            }
                            break;
                        case (CGPFunction.RESAMPLE):
                            Result = CGPImageFunctions.ReSample(Input0, (double)CurrentNode.Width / CurrentNode.Height);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.ReSample(" + Input0Text + ", " + (double)CurrentNode.Width / CurrentNode.Height + ");";
                            }
                            break;
                        case (CGPFunction.FEATURE0):
                            Result = CGPImageFunctions.Feature(Input0,
                                0,
                                CurrentNode.FeatureConfig == 3 || CurrentNode.FeatureConfig == 1,
                                CurrentNode.FeatureConfig == 2 || CurrentNode.FeatureConfig == 3,
                                CurrentNode.Width,
                                CurrentNode.GrayValue
                                );
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Feature(" + Input0Text + ", " +
                                     "0," +
                                (CurrentNode.FeatureConfig == 3 || CurrentNode.FeatureConfig == 1) + ", " +
                                (CurrentNode.FeatureConfig == 2 || CurrentNode.FeatureConfig == 3) + ", " +
                                CurrentNode.Width + ", " +
                                CurrentNode.GrayValue + ", " +
                                    ");";
                            }
                            break;
                        case (CGPFunction.FEATURE1):
                            Result = CGPImageFunctions.Feature(Input0,
                                1,
                               CurrentNode.FeatureConfig == 3 || CurrentNode.FeatureConfig == 1,
                                CurrentNode.FeatureConfig == 2 || CurrentNode.FeatureConfig == 3,
                                CurrentNode.Width,
                                CurrentNode.GrayValue
                                );
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Feature(" + Input0Text + ", " +
                                     "1," +
                                (CurrentNode.FeatureConfig == 3 || CurrentNode.FeatureConfig == 1) + ", " +
                                (CurrentNode.FeatureConfig == 2 || CurrentNode.FeatureConfig == 3) + ", " +
                                CurrentNode.Width + ", " +
                                CurrentNode.GrayValue + ", " +
                                    ");";
                            }
                            break;
                        case (CGPFunction.FEATURE2):
                            Result = CGPImageFunctions.Feature(Input0,
                                2,
                               CurrentNode.FeatureConfig == 3 || CurrentNode.FeatureConfig == 1,
                                CurrentNode.FeatureConfig == 2 || CurrentNode.FeatureConfig == 3,
                                CurrentNode.Width,
                                CurrentNode.GrayValue
                                );
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Feature(" + Input0Text + ", " +
                                     "2," +
                                (CurrentNode.FeatureConfig == 3 || CurrentNode.FeatureConfig == 1) + ", " +
                                (CurrentNode.FeatureConfig == 2 || CurrentNode.FeatureConfig == 3) + ", " +
                                CurrentNode.Width + ", " +
                                CurrentNode.GrayValue + ", " +
                                    ");";
                            }
                            break;
                        case (CGPFunction.FEATURE3):
                            Result = CGPImageFunctions.Feature(Input0,
                                3,
                              CurrentNode.FeatureConfig == 3 || CurrentNode.FeatureConfig == 1,
                                CurrentNode.FeatureConfig == 2 || CurrentNode.FeatureConfig == 3,
                                CurrentNode.Width,
                                CurrentNode.GrayValue
                                );
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Feature(" + Input0Text + ", " +
                                     "3," +
                                (CurrentNode.FeatureConfig == 3 || CurrentNode.FeatureConfig == 1) + ", " +
                                (CurrentNode.FeatureConfig == 2 || CurrentNode.FeatureConfig == 3) + ", " +
                                CurrentNode.Width + ", " +
                                CurrentNode.GrayValue + ", " +
                                    ");";
                            }
                            break;
                        case (CGPFunction.FEATURE4):
                            Result = CGPImageFunctions.Feature(Input0,
                                4,
                              CurrentNode.FeatureConfig == 3 || CurrentNode.FeatureConfig == 1,
                                CurrentNode.FeatureConfig == 2 || CurrentNode.FeatureConfig == 3,
                                CurrentNode.Width,
                                CurrentNode.GrayValue
                                );
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Feature(" + Input0Text + ", " +
                                     "4," +
                                (CurrentNode.FeatureConfig == 3 || CurrentNode.FeatureConfig == 1) + ", " +
                                (CurrentNode.FeatureConfig == 2 || CurrentNode.FeatureConfig == 3) + ", " +
                                CurrentNode.Width + ", " +
                                CurrentNode.GrayValue + ", " +
                                    ");";
                            }
                            break;
                        case (CGPFunction.FEATURE5):
                            Result = CGPImageFunctions.Feature(Input0,
                                5,
                               CurrentNode.FeatureConfig == 3 || CurrentNode.FeatureConfig == 1,
                                CurrentNode.FeatureConfig == 2 || CurrentNode.FeatureConfig == 3,
                                CurrentNode.Width,
                                CurrentNode.GrayValue
                                );
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Feature(" + Input0Text + ", " +
                                     "5," +
                                (CurrentNode.FeatureConfig == 3 || CurrentNode.FeatureConfig == 1) + ", " +
                                (CurrentNode.FeatureConfig == 2 || CurrentNode.FeatureConfig == 3) + ", " +
                                CurrentNode.Width + ", " +
                                CurrentNode.GrayValue + ", " +
                                    ");";
                            }
                            break;
                        case (CGPFunction.SMOOTHMEDIAN):
                            Result = CGPImageFunctions.SmoothMedian(Input0, CurrentNode.Width);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.SmoothMedian(" + Input0Text + ", " + CurrentNode.Width + ");";
                            }
                            break;
                        case (CGPFunction.SMOOTH):
                            Result = CGPImageFunctions.SmoothBlur(Input0, CurrentNode.Width, CurrentNode.Height);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.SmoothBlur(" + Input0Text + ", " + CurrentNode.Width + "," + CurrentNode.Height + ");";
                            }
                            break;
                        case (CGPFunction.SMOOTHGAUSSIAN):
                            Result = CGPImageFunctions.SmoothGaussian(Input0, CurrentNode.Width, CurrentNode.Height);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.SmoothGaussian(" + Input0Text + ", " + CurrentNode.Width + "," + CurrentNode.Height + ");";
                            }
                            break;
                        case (CGPFunction.UNSHARPEN):
                            Result = CGPImageFunctions.Unsharpen(Input0, CurrentNode.Width, CurrentNode.Height);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Unsharpen(" + Input0Text + ", " + CurrentNode.Width + "," + CurrentNode.Height + ");";
                            }
                            break;
                        case (CGPFunction.COMP):
                            Result = CGPImageFunctions.IF(Input0, CurrentNode.GrayValue, Input1, Input2);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.IF(" + Input0Text + ", " + CurrentNode.GrayValue + "f, " + Input1Text + ", " + Input2Text + ");";
                            }
                            break;
                        case (CGPFunction.COMP2):
                            Result = CGPImageFunctions.IF2(Input0, CurrentNode.GrayValue, Input1, Input2);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.IF2(" + Input0Text + ", " + CurrentNode.GrayValue + "f, " + Input1Text + ", " + Input2Text + ");";
                            }
                            break;
                        case (CGPFunction.ADD3):
                            Result = CGPImageFunctions.Add(Input0, Input1, Input2);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Add(" + Input0Text + ", " + Input1Text + ", " + Input2Text + ");";
                            }
                            break;
                        case (CGPFunction.AVG):
                            Result = CGPImageFunctions.Avg(Input0, Input1, Input2);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Avg(" + Input0Text + ", " + Input1Text + ", " + Input2Text + ");";

                            }
                            break;
                        case (CGPFunction.AND):
                            Result = CGPImageFunctions.And(Input0, Input1);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.And(" + Input0Text + ", " + Input1Text + ");";
                            }
                            break;
                        case (CGPFunction.OR):
                            Result = CGPImageFunctions.Or(Input0, Input1);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Or(" + Input0Text + ", " + Input1Text + ");";
                            }
                            break;
                        case (CGPFunction.XOR):
                            Result = CGPImageFunctions.Xor(Input0, Input1);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Xor(" + Input0Text + ", " + Input1Text + ");";
                            }
                            break;
                        case (CGPFunction.NOT):
                            Result = CGPImageFunctions.Not(Input0);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Not(" + Input0Text + ");";
                            }
                            break;
                        case (CGPFunction.ROTATE):
                            Result = CGPImageFunctions.Rotate(Input0, Math.Abs(CurrentNode.FeatureConfig) % 4);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Rotate(" + Input0Text + ", " + Math.Abs(CurrentNode.FeatureConfig) % 4 + ");";
                            }
                            break;
                        case (CGPFunction.LMIN):
                            Result = CGPImageFunctions.LocalStats(Input0, CurrentNode.Width, CurrentNode.Height, CGPImageFunctions.LocalStat.Min);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.LocalStats(" + Input0Text + ", " + CurrentNode.Width + ", " + CurrentNode.Height + ", CGPImageFunctions.LocalStat.Min);";
                            }
                            break;
                        case (CGPFunction.LMAX):
                            Result = CGPImageFunctions.LocalStats(Input0, CurrentNode.Width, CurrentNode.Height, CGPImageFunctions.LocalStat.Max);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.LocalStats(" + Input0Text + ", " + CurrentNode.Width + ", " + CurrentNode.Height + ", CGPImageFunctions.LocalStat.Max);";
                            }
                            break;
                        case (CGPFunction.LAVERAGE):
                            Result = CGPImageFunctions.LocalStats(Input0, CurrentNode.Width, CurrentNode.Height, CGPImageFunctions.LocalStat.Average);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.LocalStats(" + Input0Text + ", " + CurrentNode.Width + ", " + CurrentNode.Height + ", CGPImageFunctions.LocalStat.Median);";
                            }
                            break;
                        case (CGPFunction.LRANGE):
                            Result = CGPImageFunctions.LocalStats(Input0, CurrentNode.Width, CurrentNode.Height, CGPImageFunctions.LocalStat.Range);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.LocalStats(" + Input0Text + ", " + CurrentNode.Width + ", " + CurrentNode.Height + ", CGPImageFunctions.LocalStat.Range);";
                            }
                            break;
                        case (CGPFunction.LCMPMIN):
                            Result = CGPImageFunctions.CompareLocalStatsToGlobal(Input0, CurrentNode.Width, CurrentNode.Height, CGPImageFunctions.LocalStat.Min);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.CompareLocalStatsToGlobal(" + Input0Text + ", " + CurrentNode.Width + ", " + CurrentNode.Height + ", CGPImageFunctions.LocalStat.Min);";
                            }
                            break;
                        case (CGPFunction.LCMPMAX):
                            Result = CGPImageFunctions.CompareLocalStatsToGlobal(Input0, CurrentNode.Width, CurrentNode.Height, CGPImageFunctions.LocalStat.Max);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.CompareLocalStatsToGlobal(" + Input0Text + ", " + CurrentNode.Width + ", " + CurrentNode.Height + ", CGPImageFunctions.LocalStat.Max);";
                            }
                            break;
                        case (CGPFunction.LCMPAVERAGE):
                            Result = CGPImageFunctions.CompareLocalStatsToGlobal(Input0, CurrentNode.Width, CurrentNode.Height, CGPImageFunctions.LocalStat.Average);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.CompareLocalStatsToGlobal(" + Input0Text + ", " + CurrentNode.Width + ", " + CurrentNode.Height + ", CGPImageFunctions.LocalStat.Median);";
                            }
                            break;
                        case (CGPFunction.LCMPRANGE):
                            Result = CGPImageFunctions.CompareLocalStatsToGlobal(Input0, CurrentNode.Width, CurrentNode.Height, CGPImageFunctions.LocalStat.Range);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.CompareLocalStatsToGlobal(" + Input0Text + ", " + CurrentNode.Width + ", " + CurrentNode.Height + ", CGPImageFunctions.LocalStat.Range);";
                            }
                            break;
                        case (CGPFunction.GrayLevelFeature):
                            Result = CGPImageFunctions.GrayLevelFeature(Input0, CurrentNode.FeatureConfig % 5, CurrentNode.Width, CurrentNode.Height);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.GrayLevelFeature(" + Input0Text + ", " + (CurrentNode.FeatureConfig % 5) + ", " + CurrentNode.Width + ", " + CurrentNode.Height + ");";
                            }
                            break;
                        case (CGPFunction.NORMGUASS):
                            Result = CGPImageFunctions.LocalNormalizeWithGauss(Input0, CurrentNode.Width, CurrentNode.Height);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.LocalNormalizeWithGauss(" + Input0Text + ", " + CurrentNode.Width + ", " + CurrentNode.Height + ");";
                            }
                            break;
                        case (CGPFunction.ABS):
                            Result = CGPImageFunctions.Abs(Input0);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Abs(" + Input0Text + ");";
                            }
                            break;
                        case (CGPFunction.ABSTHRESHOLD):
                            Result = CGPImageFunctions.AbsThreshold(Input0, CurrentNode.GrayValue);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.AbsThreshold(" + Input0Text + ", " + CurrentNode.GrayValue + "f);";
                            }
                            break;
                        case (CGPFunction.OTSU):
                            Result = CGPImageFunctions.OtsuThreshold(Input0, CurrentNode.GrayValue);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.OtsuThreshold(" + Input0Text + ", " + CurrentNode.GrayValue + "f);";
                            }
                            break;
                        case (CGPFunction.CONVOLVE):
                            Result = CGPImageFunctions.Convolve(Input0, KernelFromNode(CurrentNode));
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Convole(" + Input0Text + ", " + KernelFromNode(CurrentNode) + ");";
                            }
                            break;
                        case (CGPFunction.EDGEANGLE):
                            Result = CGPImageFunctions.EdgeDirection(Input0);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.EdgeDirection(" + Input0Text + ");";
                            }
                            break;
                        case (CGPFunction.EDGEMAG):
                            Result = CGPImageFunctions.EdgeMag(Input0);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.EdgeMag(" + Input0Text + ");";
                            }
                            break;
                        case (CGPFunction.QUANTIZE):
                            Result = CGPImageFunctions.Quantize(Input0, CurrentNode.FeatureConfig);
                            if (Parameters.ShowTrace)
                            {
                                TraceText += "CGPImageFunctions.Quantize(" + Input0Text + "," + CurrentNode.FeatureConfig + ");";
                            }
                            break;
                     

                        case (CGPFunction.Ra):
                            {
                                Result = CGPImageFunctions.SurfaceMeasurement(Input0, CurrentNode.Width, CGPImageFunctions.SurfaceMeasurementType.Ra);
                                if (Parameters.ShowTrace)
                                    TraceText += "CGPImageFunctions.SurfaceMeasurement(" + Input0Text + "," + CurrentNode.Width + ",CGPImageFunctions.SurfaceMeasurementType.Ra); ";
                                break;
                            }
                        case (CGPFunction.Rq):
                            {
                                Result = CGPImageFunctions.SurfaceMeasurement(Input0, CurrentNode.Width, CGPImageFunctions.SurfaceMeasurementType.Rq);
                                if (Parameters.ShowTrace)
                                    TraceText += "CGPImageFunctions.SurfaceMeasurement(" + Input0Text + "," + CurrentNode.Width + ",CGPImageFunctions.SurfaceMeasurementType.Rq); ";
                                break;
                            }
                        case (CGPFunction.Rv):
                            {
                                Result = CGPImageFunctions.SurfaceMeasurement(Input0, CurrentNode.Width, CGPImageFunctions.SurfaceMeasurementType.Rv);
                                if (Parameters.ShowTrace)
                                    TraceText += "CGPImageFunctions.SurfaceMeasurement(" + Input0Text + "," + CurrentNode.Width + ",CGPImageFunctions.SurfaceMeasurementType.Rv); ";
                                break;
                            }
                        case (CGPFunction.Rp):
                            {
                                Result = CGPImageFunctions.SurfaceMeasurement(Input0, CurrentNode.Width, CGPImageFunctions.SurfaceMeasurementType.Rp);
                                if (Parameters.ShowTrace)
                                    TraceText += "CGPImageFunctions.SurfaceMeasurement(" + Input0Text + "," + CurrentNode.Width + ",CGPImageFunctions.SurfaceMeasurementType.Rp); ";
                                break;
                            }
                        case (CGPFunction.Rt):
                            {
                                Result = CGPImageFunctions.SurfaceMeasurement(Input0, CurrentNode.Width, CGPImageFunctions.SurfaceMeasurementType.Rt);
                                if (Parameters.ShowTrace)
                                    TraceText += "CGPImageFunctions.SurfaceMeasurement(" + Input0Text + "," + CurrentNode.Width + ",CGPImageFunctions.SurfaceMeasurementType.Rt); ";
                                break;
                            }
                        case (CGPFunction.Rsk):
                            {
                                Result = CGPImageFunctions.SurfaceMeasurement(Input0, CurrentNode.Width, CGPImageFunctions.SurfaceMeasurementType.Rsk);
                                if (Parameters.ShowTrace)
                                    TraceText += "CGPImageFunctions.SurfaceMeasurement(" + Input0Text + "," + CurrentNode.Width + ",CGPImageFunctions.SurfaceMeasurementType.Rsk); ";
                                break;
                            }
                        case (CGPFunction.Rku):
                            {
                                Result = CGPImageFunctions.SurfaceMeasurement(Input0, CurrentNode.Width, CGPImageFunctions.SurfaceMeasurementType.Rku);
                                if (Parameters.ShowTrace)
                                    TraceText += "CGPImageFunctions.SurfaceMeasurement(" + Input0Text + "," + CurrentNode.Width + ",CGPImageFunctions.SurfaceMeasurementType.Rku); ";
                                break;
                            }
                        case (CGPFunction.Rz):
                            {
                                Result = CGPImageFunctions.SurfaceMeasurement(Input0, CurrentNode.Width, CGPImageFunctions.SurfaceMeasurementType.Rz);
                                if (Parameters.ShowTrace)
                                    TraceText += "CGPImageFunctions.SurfaceMeasurement(" + Input0Text + "," + CurrentNode.Width + ",CGPImageFunctions.SurfaceMeasurementType.Rz); ";
                                break;
                            }
                        case (CGPFunction.Rent):
                            {
                                Result = CGPImageFunctions.SurfaceMeasurement(Input0, CurrentNode.Width, CGPImageFunctions.SurfaceMeasurementType.Rent);
                                if (Parameters.ShowTrace)
                                    TraceText += "CGPImageFunctions.SurfaceMeasurement(" + Input0Text + "," + CurrentNode.Width + ",CGPImageFunctions.SurfaceMeasurementType.Rent); ";
                                break;
                            }
                        case (CGPFunction.LMAJ):
                            {
                                Result = CGPImageFunctions.LocalMajority(Input0, CurrentNode.Width, CurrentNode.Height, CurrentNode.GrayValue);
                                if (Parameters.ShowTrace)
                                    TraceText += "CGPImageFunctions.LocalMajority(" + Input0Text + "," + CurrentNode.Width + ","+CurrentNode.Height+","+CurrentNode.GrayValue+"); ";
                                break;
                            }
                        default:
                            throw new Exception(Func + " is not implemented yet!");
                    }

                    if (Result == null)
                        throw new Exception("Node failed to output");
                    this.NodeCache[NodeIndex] = Result;

                    if (Parameters.ShowTrace && RunCounter == 0)
                    {
                        TraceText += " // Repeat = " + Repeat;
                        Reporting.Say(TraceText);
                    }

                    //FitnessFunction.TestSet.CheckCheckSum();
                    if (Parameters.ShowTrace && RunCounter == 0)
                    {
                        if (!Directory.Exists(Parameters.TraceOutputFolder))
                            Directory.CreateDirectory(Parameters.TraceOutputFolder);
                        string NodeFileName = String.Format("{2}//Node_{1}_{0:000}.png", NodeIndex, Tag, Parameters.TraceOutputFolder);
                        Result.Save(NodeFileName);
                    }
                }
            }
            Timer.Stop();


            ExecutionTimer.Stop();


            if (Parameters.ShowTrace && RunCounter == 0)
            {
                foreach (string N in NodesUsed)
                    Reporting.Say("Used node " + N);

                RenderGraph(Graph, Inputs, TestCaseIndex, Tag);
            }

            RunCounter++;

        }
        public string GraphText = "";
        public void RenderGraph(CGPGraph Graph, CGPImage[] Inputs, int TestCaseIndex, string Tag)
        {
            StringBuilder sb = new StringBuilder();

            //sb.AppendLine("Graph G"+Tag+" {");
           /* for (int i = 0; i < Inputs.Length; i++)
            {
                string InputFileName = String.Format("Inputs[{1}][{0:00}]", i, Tag, TraceOutputFolder);
                InputFileName = InputFileName.Replace("[","_").Replace("]","_");
                string Line = String.Format("{0} [shape=box,color=blue,style=bold,label=\"{1}\",image=\"{0}.png\",labelloc=b,fontcolor=red];", InputFileName, InputFileName);
                tw.WriteLine(Line);
            }*/

            Dictionary<int, string> NodesAndTags = new Dictionary<int, string>();
            for (int i = 0; i < Graph.Width; i++)
            {

                string ThisNodeText = String.Format("Node_" + Tag + "_{0:000}", i);
                NodesAndTags.Add(i, ThisNodeText);
                CGPNode CurrentNode = Graph.Nodes[i];

                if (CurrentNode == null || !CurrentNode.Evaluated)
                    continue;

                string Line = String.Format("{0} [shape=box,color=blue,style=bold,label=\"{1}\",image=\"{0}.png\",labelloc=b,fontcolor=red];", ThisNodeText, CurrentNode.Function.ToString());
                sb.AppendLine(Line);
            }

            sb.AppendLine(String.Format("{0} [shape=circle,color=blue,style=bold,label=\"{1}\",image=\"{1}.png\",labelloc=b,fontcolor=red];", "Output_" + Tag, "Output_" + Tag.ToString()));
            

            List<int> InputsUsed = new List<int>();
            string LastNode = "";
            for (int i = 0; i < Graph.Width; i++)
            {
                
                string ThisNodeText = String.Format("Node_" + Tag + "_{0:000}", i);
                LastNode = ThisNodeText;
                CGPNode CurrentNode = Graph.Nodes[i];

                if (CurrentNode == null || !CurrentNode.Evaluated)
                    continue;

                CGPNode CurrentSuperNode = Graph.Nodes[i];

                int ConnectTo0 = i - CurrentSuperNode.Connection0;
                int ConnectTo1 = i - CurrentSuperNode.Connection1;
                int ConnectTo2 = i - CurrentSuperNode.Connection2;
                int Arity = CurrentSuperNode.Arity;

                CGPFunction Func = CurrentNode.Function;

                CGPImage Input0 = null;
                CGPImage Input1 = null;
                CGPImage Input2 = null;

                int InputIndexUsedInput0 = -1;
                int InputIndexUsedInput1 = -1;
                int InputIndexUsedInput2 = -1;
                string Input0Text = "";
                string Input1Text = "";
                string Input2Text = "";

                if (Arity > 0)
                {
                    Input0 = this.GetNodeCacheValue(ConnectTo0, Tag, out InputIndexUsedInput0, out Input0Text);
                    Input0Text = Input0Text.Replace("[", "_").Replace("]", "_");
                    sb.AppendLine(ThisNodeText + " -- " + Input0Text + "  [style=bold,color=red];");
                    if (InputIndexUsedInput0 >= 0 && !InputsUsed.Contains(InputIndexUsedInput0))
                        InputsUsed.Add(InputIndexUsedInput0);
                }
                if (Arity > 1)
                {
                    Input1 = this.GetNodeCacheValue(ConnectTo1, Tag, out InputIndexUsedInput1, out Input1Text);
                    Input1Text = Input1Text.Replace("[", "_").Replace("]", "_");
                    sb.AppendLine(ThisNodeText + " -- " + Input1Text + "  [style=bold,color=red];");
                    if (InputIndexUsedInput1 >= 0 && !InputsUsed.Contains(InputIndexUsedInput1))
                        InputsUsed.Add(InputIndexUsedInput1);
                }
                if (Arity > 2)
                {
                    Input2 = this.GetNodeCacheValue(ConnectTo2, Tag, out InputIndexUsedInput2, out Input2Text);
                    Input2Text = Input2Text.Replace("[", "_").Replace("]", "_");
                    sb.AppendLine(ThisNodeText + " -- " + Input2Text + "  [style=bold,color=red];");
                    if (InputIndexUsedInput2 >= 0 && !InputsUsed.Contains(InputIndexUsedInput2))
                        InputsUsed.Add(InputIndexUsedInput2);
                }

                
            }

            for (int i = 0; i < Inputs.Length; i++)
            {
                if (!InputsUsed.Contains(i))
                    continue;

                string InputFileName = String.Format("Inputs[{1}][{0:00}]", i, Tag, Parameters.TraceOutputFolder);
                InputFileName = InputFileName.Replace("[", "_").Replace("]", "_");
                string Line = String.Format("{0} [shape=box,color=blue,style=bold,label=\"{1}\",image=\"{0}.png\",labelloc=b,fontcolor=red];", InputFileName, InputFileName);
                sb.AppendLine(Line);
            }

            for (int i = 0; i < InputsUsed.Count; i++)
            {
                Reporting.Say(String.Format("PROGRAM INPUT {0,2} = INPUT {1,2} = {2}", i, InputsUsed[i], Inputs[InputsUsed[i]].Label));
            }

                // sb.AppendLine("}");

                sb.AppendLine("Output_" + Tag + " -- " + LastNode + " [style=bold,color=red];");

            GraphText = sb.ToString();

            /*string ImageFileName = GraphFileName.Replace(".gv", ".png");
            ProcessStartInfo PSI = new ProcessStartInfo("dot", "-Tpng " + Path.GetFileName(GraphFileName) + " -o " + Path.GetFileName(ImageFileName));
            PSI.WorkingDirectory = TraceOutputFolder;
            PSI.UseShellExecute = true;
            PSI.CreateNoWindow = true;                
            Process.Start(PSI);
            */

        }

        public static int[] ValidApetures = { 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21 };
        
        private int GetApeture(CGPNode N)
        {
            return ValidApetures[(int)Math.Abs(N.Width) % ValidApetures.Length];
        }

        private int IterationsFromNode(CGPNode CurrentNode)
        {
            int it = (int)Math.Round(CurrentNode.GrayValue);
            if (it < 0) it *= -1;
            if (it < 1) it = 1;
            if (it > 10) it = 10;
            return it;
        }

        private int KernelFromNode(CGPNode CurrentNode)
        {
            int it = (int)Math.Round(CurrentNode.GrayValue);
            if (it < 0) it *= -1;
            if (CGPImageFunctions.Kernels == null)
                CGPImageFunctions.LoadKernels();
            return it % CGPImageFunctions.Kernels.Count;
        }

        public void CleanUp()
        {
            return;
            FitnessFunction.TestSet.CheckCheckSum();

            for (int i = 0; i < this.NodeCache.Length - 1; i++)
            {
                if (this.NodeCache[i] != null)
                {
                    this.NodeCache[i].Img.Dispose();
                    this.NodeCache[i].Img = null;
                    this.NodeCache[i] = null;
                }
            }

            FitnessFunction.TestSet.CheckCheckSum();
        }
    }
}