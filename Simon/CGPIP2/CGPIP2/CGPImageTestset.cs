﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Emgu.CV;
using Emgu.CV.Structure;

namespace CGPIP2
{
    public class CGPImageTestCase
    {
        public enum TestCaseUseType
        {
            Training,
            Validation
        };

        public TestCaseUseType UseType = TestCaseUseType.Training;

        public List<CGPImage> Inputs = null;
        public CGPImage Target = null;

        public bool[,] IgnoreMask = null;
        public CGPImage ValidationImage;
        public string InputFileName;
        public string TargetFileName;

        public Image<Bgr, float> LoadBin(string FileName)
        {
            CGPImage Img = new CGPImage(FileName);
            return Img.Img.Convert<Bgr, float>();
        }

        public CGPImageTestCase(
           string InputFile,
           string TargetFile,
           bool ScaleImages,
           bool SplitRGB,
           bool SplitHSB,
            bool FilterRed,
            bool FilterBlueForIgnore,
            bool InvertTarget,
            float BrightnessAdjust)
        {
            Reporting.Say("CGPImageTestCase:\t" + InputFile + "\t" + TargetFile + "\t" + SplitHSB + "\t" + SplitRGB);

            Image<Bgr, float> OriginalInputImage =
                InputFile.EndsWith(".bin") || InputFile.EndsWith(".raw") ? LoadBin(InputFile) : new Image<Bgr, float>(InputFile);

            if (Parameters.FilterImagesWithSmallDimensions > 0 &&
                (OriginalInputImage.Width <= Parameters.FilterImagesWithSmallDimensions ||
                OriginalInputImage.Height <= Parameters.FilterImagesWithSmallDimensions)
                )
            {
                Reporting.Say("Image too small to be useful");
                throw new Exception("Image too small");
            }

            if (Parameters.FilterImagesWithLargeDimensions > 0 &&
                (OriginalInputImage.Width >= Parameters.FilterImagesWithLargeDimensions ||
                OriginalInputImage.Height >= Parameters.FilterImagesWithLargeDimensions)
                )
            {
                Reporting.Say("Image too large to be useful");
                throw new Exception("Image too large");
            }

            this.Inputs = new List<CGPImage>();

            if (BrightnessAdjust > 0)
                OriginalInputImage._Mul(BrightnessAdjust);

            this.Inputs.Add(new CGPImage(OriginalInputImage.Convert<Gray, float>(), "GrayLevel"));




            if (SplitHSB)
            {
                Image<Hsv, float> HsvImg = new Image<Hsv, float>(InputFile);
                Image<Gray, float>[] HsvComps = HsvImg.Split();
                HsvComps[1]._Mul(255);
                this.Inputs.Add(new CGPImage(HsvComps[0], "H"));
                this.Inputs.Add(new CGPImage(HsvComps[1], "S"));
                this.Inputs.Add(new CGPImage(HsvComps[2], "B"));
            }

            if (SplitRGB)
            {
                Image<Gray, float>[] RgbComps = OriginalInputImage.Split();
                this.Inputs.Add(new CGPImage(RgbComps[0], "B"));
                this.Inputs.Add(new CGPImage(RgbComps[1], "G"));
                this.Inputs.Add(new CGPImage(RgbComps[2], "R"));
            }



            if (FilterRed)
            {
                Image<Bgr, float> ColorTarget = new Image<Bgr, float>(TargetFile);
                Target = new CGPImage(ColorTarget.Width, ColorTarget.Height);
                for (int x = 0; x < ColorTarget.Width; x++)
                    for (int y = 0; y < ColorTarget.Height; y++)
                    {
                        /*if (ColorTarget.Data[y, x, 0] < 5 &&
                            ColorTarget.Data[y, x, 1] < 5 &&
                            ColorTarget.Data[y, x, 2] > 200)*/
                        if (ColorTarget.Data[y, x, 0] < 40 &&
                       ColorTarget.Data[y, x, 1] < 40 &&
                       ColorTarget.Data[y, x, 2] > 200)
                        {
                            Target.Img.Data[y, x, 0] = 255;
                        }
                        else
                        {
                            Target.Img.Data[y, x, 0] = 0;
                        }
                    }
            }
            else
            {
                Target = new CGPImage(TargetFile);
            }



            if (InvertTarget)
                Target = CGPImageFunctions.ThresholdInv(Target, 128);




            this.IgnoreMask = new bool[this.Target.Height, this.Target.Width];
            if (FilterBlueForIgnore)
            {
                Image<Bgr, float> ColorTarget = new Image<Bgr, float>(TargetFile);
                ColorTarget = ColorTarget.Resize(Target.Width, Target.Height, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                for (int x = 0; x < ColorTarget.Width; x++)
                    for (int y = 0; y < ColorTarget.Height; y++)
                    {
                        if (ColorTarget.Data[y, x, 0] > 200 &&
                            ColorTarget.Data[y, x, 1] < 50 &&
                            ColorTarget.Data[y, x, 2] < 50)
                        {
                            this.IgnoreMask[y, x] = true;
                        }
                        else
                        {
                            this.IgnoreMask[y, x] = false;
                        }
                    }
            }

            if (Parameters.LocalNormalizeInputs)
            {
                List<CGPImage> AdditionalProperties = new List<CGPImage>();
                foreach (CGPImage Cmp in Inputs)
                {
                    AdditionalProperties.Add(CGPImageFunctions.LocalNormalizeWithGauss(Cmp, 2, 20));
                    //   AdditionalProperties.Add(CGPImageFunctions.LocalNormalizeWithGauss(Cmp, 4, 20));
                    // AdditionalProperties.Add(CGPImageFunctions.LocalNormalizeWithGauss(Cmp, 2, 40));
                    //AdditionalProperties.Add(CGPImageFunctions.LocalNormalizeWithGauss(Cmp, 10, 40));
                }
                if (Parameters.UseOnlyLocalNormalizeInputs)
                    Inputs.Clear();
                Inputs.AddRange(AdditionalProperties);
            }

            if (Parameters.PreComputeAdditionalInputs)
            {
                List<CGPImage> AdditionalProperties = new List<CGPImage>();
                foreach (CGPImage Cmp in Inputs)
                    AdditionalProperties.AddRange(ImageProperties.GetProperties(Cmp));

                Inputs.AddRange(AdditionalProperties);
            }

            foreach (CGPImage Cmp in Inputs)
                Cmp.AssociatedFileName = InputFile;

            if (IgnoreMask != null)
            {
                if (Parameters.CropToIgnoreMask)
                {
                    int MaskTX = this.Target.Width;
                    int MaskTY = Target.Height;
                    int MaskBX = 0;
                    int MaskBY = 0;
                    for (int x = 0; x < Target.Width; x++)
                        for (int y = 0; y < Target.Height; y++)
                        {
                            if (!IgnoreMask[y, x])
                            {
                                if (y > MaskBY) MaskBY = y;
                                if (y < MaskTY) MaskTY = y;
                                if (x > MaskBX) MaskBX = x;
                                if (x < MaskTX) MaskTX = x;
                            }
                        }
                    Reporting.Say("Can crop image from " + this.Target.Width + "x" + this.Target.Height + " to " + MaskTX + "," + MaskTY + " - " + (MaskBX - MaskTX) + "x" + (MaskBY - MaskTY));

                    Target = CGPImageFunctions.Extract(Target,
                                new System.Drawing.Rectangle(MaskTX, MaskTY, MaskBX - MaskTX, MaskBY - MaskTY));
                    for (int i = 0; i < Inputs.Count; i++)
                        Inputs[i] = CGPImageFunctions.Extract(Inputs[i],
                              new System.Drawing.Rectangle(MaskTX, MaskTY, MaskBX - MaskTX, MaskBY - MaskTY));

                    bool[,] NewIgnoreMask = new bool[this.Target.Height, this.Target.Width];
                    for (int x = 0; x < Target.Width; x++)
                        for (int y = 0; y < Target.Height; y++)
                        {
                            NewIgnoreMask[y, x] = this.IgnoreMask[y + MaskTY, x + MaskTX];
                        }

                    this.IgnoreMask = NewIgnoreMask;
                }

                for (int i = 0; i < Inputs.Count; i++)
                {
                    CGPImage Cmp = Inputs[i];

                    for (int x = 0; x < Cmp.Width; x++)
                        for (int y = 0; y < Cmp.Height; y++)
                        {
                            if (IgnoreMask[y, x])
                                Cmp.Img.Data[y, x, 0] = 0;
                        }
                }


            }
            if (Parameters.CropInput)
            {
                List<CGPImage> CroppedImages = new List<CGPImage>();
                foreach (CGPImage Orig in Inputs)
                {
                    CroppedImages.Add(CGPImageFunctions.Crop(Orig, Parameters.Crop_TX, Parameters.Crop_TY, Parameters.Crop_BX, Parameters.Crop_BY));
                }
                Inputs = CroppedImages;
                Target = CGPImageFunctions.Crop(Target, Parameters.Crop_TX, Parameters.Crop_TY, Parameters.Crop_BX, Parameters.Crop_BY);
            }


            if (ScaleImages)
            {
                List<CGPImage> ScaledImages = new List<CGPImage>();
                if (Parameters.Rescales != null)
                {
                    foreach (CGPImage Orig in Inputs)
                    {
                        foreach (double S in Parameters.Rescales)
                        {
                            CGPImage t = CGPImageFunctions.Rescale(Orig, S, false);
                            ScaledImages.Add(t);
                            Reporting.Say("Image resized by " + S + " to " + t.Width + "x" + t.Height);
                        }
                    }
                    Target = CGPImageFunctions.Rescale(Target, ScaledImages[0].Width, ScaledImages[0].Height);
                    Target = CGPImageFunctions.Threshold(Target, 128);
                }
                if (Parameters.RescaleToFit != null)
                {
                    foreach (CGPImage Orig in Inputs)
                    {
                        foreach (Size S in Parameters.RescaleToFit)
                        {
                            double XScale = double.MaxValue;
                            double YScale = double.MaxValue;

                            if (S.Width > 1)
                            {
                                XScale = (double)S.Width / (double)(Orig.Width);
                            }
                            else if (S.Width > 1)
                            {
                                YScale = (double)S.Height / (double)(Orig.Height);
                            }
                            else
                                throw new Exception("One of the resize dimensions must be above 1");

                            CGPImage t = CGPImageFunctions.Rescale(Orig, Math.Min(XScale, YScale), true);
                            ScaledImages.Add(t);
                            Reporting.Say("Image resized by " + S + " to " + t.Width + "x" + t.Height + " with scale " + XScale + "x" + YScale);
                        }
                    }
                    Target = CGPImageFunctions.Rescale(Target, ScaledImages[0].Width, ScaledImages[0].Height);
                    Target = CGPImageFunctions.Threshold(Target, 128);
                }

                Inputs = ScaledImages;

                bool[,] ScaledIgnoreMask = new bool[this.Target.Height, this.Target.Width];
                double IgnoreMaskXScale = (double)this.IgnoreMask.GetLength(1) / this.Target.Width;
                double IgnoreMaskYScale = (double)this.IgnoreMask.GetLength(0) / this.Target.Height;

                for (int x = 0; x < this.Target.Width; x++)
                    for (int y = 0; y < this.Target.Height; y++)
                    {
                        int SrcX = (int)(IgnoreMaskXScale * x);
                        int SrcY = (int)(IgnoreMaskYScale * y);
                        /* if (SrcX >= this.IgnoreMask.GetLength(1))
                             SrcX = this.IgnoreMask.GetLength(1) - 1;
                         if (SrcY >= this.IgnoreMask.GetLength(0))
                             SrcY = this.IgnoreMask.GetLength(0) - 1;*/
                        ScaledIgnoreMask[y, x] = this.IgnoreMask[SrcY, SrcX];
                    }

                this.IgnoreMask = ScaledIgnoreMask;
            }


            //this.IgnoreMask = null;
            this.ValidationImage = this.Inputs[0].Clone();

            this.InputFileName = InputFile;
            this.TargetFileName = TargetFile;
        }

        public void CheckCheckSum()
        {/*
            foreach (CGPImage I in this.Inputs)
                I.CheckCheckSum();
            this.Target.CheckCheckSum();*/
        }

        public CGPImageTestCase(string InputFile, bool FilterRed, bool ScaleImages, bool InvertTarget)
        {
            if (!File.Exists(InputFile))
            {
                throw new Exception("Cannot file ImageSetEntry definition file " + InputFile);
            }
            Reporting.Say("Loading\t" + InputFile);
            TextReader tr = new StreamReader(InputFile);
            string Line = tr.ReadLine();
            string PathName = Path.GetDirectoryName(InputFile);

            List<CGPImage> Components = new List<CGPImage>();


            int LineIndex = 0;
            while (Line != null)
            {
                LineIndex++;
                if (Line.StartsWith("%") || Line.StartsWith("//") || Line.StartsWith("#") || Line.Trim().Length == 0)
                {
                    Line = tr.ReadLine();
                    continue;
                }

                string[] Tokens = Line.Split('\t', ' ', ',');

                switch (Tokens[0].ToLower())
                {
                    case ("path"):
                        PathName = Line.Substring("Path".Length + 1).Trim();
                        break;
                    case ("target"):
                        string TargetFileName = PathName + "\\" + Tokens[1];
                        if (!File.Exists(TargetFileName))
                            TargetFileName = Path.GetDirectoryName(InputFile) + "\\" + Tokens[1];
                        if (!File.Exists(TargetFileName))
                            TargetFileName
                                 = Tokens[1];
                        if (FilterRed)
                        {
                            Image<Bgr, float> ColorTarget = new Image<Bgr, float>(TargetFileName);
                            Target = new CGPImage(ColorTarget.Width, ColorTarget.Height);
                            for (int x = 0; x < ColorTarget.Width; x++)
                                for (int y = 0; y < ColorTarget.Height; y++)
                                {
                                    if (ColorTarget.Data[y, x, 0] < 50 &&
                                        ColorTarget.Data[y, x, 1] < 50 &&
                                        ColorTarget.Data[y, x, 2] > 200)
                                    {
                                        Target.Img.Data[y, x, 0] = 255;
                                    }
                                    else
                                    {
                                        Target.Img.Data[y, x, 0] = 0;
                                    }
                                }
                        }
                        else
                        {
                            Target = new CGPImage(TargetFileName);
                        }
                        break;
                    case ("input"):
                        string InputFileName = PathName + "\\" + Tokens[1];
                        if (!File.Exists(InputFileName))
                            InputFileName
                                 = Path.GetDirectoryName(InputFile) + "\\" + Tokens[1];
                        if (!File.Exists(InputFileName))
                            InputFileName
                                 = Tokens[1];
                        string[] IgnoreList = { "_11.dat", "_12.dat", "_13.dat", "_14.dat", "_15.dat", "_18.dat" };
                        if (Array.IndexOf(IgnoreList, Path.GetFileName(InputFileName)) < 0)
                        {
                            if (!File.Exists(InputFileName))
                                throw new Exception("Did not find file " + InputFileName);
                            CGPImage Img = new CGPImage(InputFileName);
                            Components.Add(Img);
                        }
                        break;
                    case ("training"):
                        this.UseType = TestCaseUseType.Training;
                        break;
                    case ("validation"):
                        this.UseType = TestCaseUseType.Validation;
                        break;
                    default:
                        throw new Exception("Unable to understand '" + Tokens[0] + "' on line " + LineIndex + " in " + InputFile + ". I assume this is not a valid ImageSetEntry file");
                }
                if (PathName == "")
                    throw new Exception("First line of ImageSetEntry must contain path.  Error in:" + InputFile);
                Line = tr.ReadLine();
            }
            tr.Close();

            Inputs = new List<CGPImage>();
            if (ScaleImages)
            {



                foreach (CGPImage Orig in Components)
                {
                    foreach (double S in Parameters.Rescales)
                    {
                        CGPImage t = CGPImageFunctions.Rescale(Orig, S, false);
                        Inputs.Add(t);
                        Reporting.Say("Image resized by " + S + " to " + t.Width + "x" + t.Height);
                    }
                }
                // Target = CGPImageFunctions.Rescale(Target, Parameters.Rescales[0], false);

                Target = CGPImageFunctions.Rescale(Target, Inputs[0].Width, Inputs[0].Height);
                Target = CGPImageFunctions.Dilate(Target, 1);
                Target = CGPImageFunctions.Threshold(Target, 128);
            }
            else
            {
                Inputs.AddRange(Components);
            }

            //  if (Math.Max(Components[0].Width, Components[0].Height) > Parameters.MaxImageDimension)
            //      throw new Exception("Image is too large");
            this.ValidationImage = this.Inputs[0].Clone();
            //this.Inputs = Components;//.ToArray();

            foreach (CGPImage Cmp in Components)
                Cmp.AssociatedFileName = InputFile;

            this.InputFileName = InputFile;

            if (this.Target == null)
                Console.WriteLine("No target?");
        }

        public ConfusionMatrix Compare(CGPImage Img, float ImgThreshold, int Border, int Offset, int Stride)
        {
            int w = this.Target.Width - Border;
            int h = this.Target.Height - Border;

            ConfusionMatrix CM = new ConfusionMatrix();

            for (int x = Offset + Border; x < w; x += Stride)
                for (int y = Offset + Border; y < h; y += Stride)
                {
                    bool Expected = this.Target.Img.Data[y, x, 0] > 128f;// this.TargetAsBools[y, x];
                    bool Predicted = Img.Img.Data[y, x, 0] > ImgThreshold;

                    if (Expected && Predicted)
                        CM.TP++;
                    else if (!Expected && !Predicted)
                        CM.TN++;
                    else if (!Expected && Predicted)
                        CM.FP++;
                    else if (Expected && !Predicted)
                        CM.FN++;
                }

            return CM;
        }



        public ConfusionMatrix Compare(CGPImage AcceptImg, CGPImage RejectImg, float ImgThresholdAccept, float ImgThresholdReject, int Border, int Offset, int Stride, int MixMode)
        {
            int w = this.Target.Width - (2 * Border);
            int h = this.Target.Height - (2 * Border);

            ConfusionMatrix CM = new ConfusionMatrix();

            for (int x = Offset + Border; x < w; x += Stride)
                for (int y = Offset + Border; y < h; y += Stride)
                {
                    bool Ignore = this.IgnoreMask != null ? this.IgnoreMask[y, x] : false;
                    if (Ignore)
                        continue;

                    bool Expected = this.Target.Img.Data[y, x, 0] > 128f;// this.TargetAsBools[y, x];
                    bool PredictedA = AcceptImg != null && AcceptImg.Img.Data[y, x, 0] > ImgThresholdAccept;
                    bool PredictedR = RejectImg != null && RejectImg.Img.Data[y, x, 0] > ImgThresholdReject;
                    bool Predicted = false;
                    if (MixMode == 0)
                        Predicted = PredictedA && !PredictedR;
                    else if (MixMode == 1)
                        Predicted = PredictedA || PredictedR;
                    else if (MixMode == 2)
                        Predicted = PredictedA ^ PredictedR;
                    else if (MixMode == 3)
                        Predicted = PredictedA && PredictedR;
                    else if (MixMode == 4)
                        Predicted = PredictedA;
                    else if (MixMode == 5)
                        Predicted = PredictedR;


                    if (Expected && Predicted)
                        CM.TP++;
                    else if (!Expected && !Predicted)
                        CM.TN++;
                    else if (!Expected && Predicted)
                        CM.FP++;
                    else if (Expected && !Predicted)
                        CM.FN++;
                }

            return CM;
        }



        internal ConfusionMatrix Compare(bool[,] CombinedOutput, int w, int h, int Border, int Offset, int Stride)
        {

            ConfusionMatrix CM = new ConfusionMatrix();

            for (int x = Offset + Border; x < w - Border; x += Stride)
                for (int y = Offset + Border; y < h - Border; y += Stride)
                {
                    bool Ignore = this.IgnoreMask != null ? this.IgnoreMask[y, x] : false;
                    if (Ignore)
                        continue;

                    bool Expected = this.Target.Img.Data[y, x, 0] > 128f;// this.TargetAsBools[y, x];
                    bool Predicted = CombinedOutput[y, x];

                    if (Expected && Predicted)
                        CM.TP++;
                    else if (!Expected && !Predicted)
                        CM.TN++;
                    else if (!Expected && Predicted)
                        CM.FP++;
                    else if (Expected && !Predicted)
                        CM.FN++;
                }

            return CM;
        }

        internal double Compare(float[,] CombinedOutput, int w, int h, int Border, int Offset, int Stride)
        {

            double TotalError = 0;
            int Count = 0;
            for (int x = Offset + Border; x < w - Border; x += Stride)
                for (int y = Offset + Border; y < h - Border; y += Stride)
                {
                    bool Ignore = this.IgnoreMask != null ? this.IgnoreMask[y, x] : false;
                    if (Ignore)
                        continue;

                    float Expected = this.Target.Img.Data[y, x, 0];
                    float Predicted = CombinedOutput[y, x];

                    float Error = Expected - Predicted;

                    // if (Error < 0) Error *= -1;
                    TotalError += Error * Error;
                    Count++;
                }
            TotalError /= Count;
            return Math.Sqrt(TotalError);
        }

        private bool? _OverallClass = null;
        private Image<Bgr, float> SrcImg;
        private bool p1;
        private bool p2;
        private bool p3;
        public bool OverallClass()
        {
            if (_OverallClass != null)
                return (bool)_OverallClass;

            _OverallClass =
                this.Target.Img.Data[0, 0, 0] > 128;

            return (bool)_OverallClass;
        }

        public CGPImageTestCase()
        {

        }

        public CGPImageTestCase(Image<Bgr, float> SrcImg, bool ScaleImages, bool SplitRGB, bool SplitHSB, float BrightnessAdjust)
        {

            Image<Bgr, float> OriginalInputImage = SrcImg;

            if (Parameters.FilterImagesWithSmallDimensions > 0 &&
                (OriginalInputImage.Width <= Parameters.FilterImagesWithSmallDimensions ||
                OriginalInputImage.Height <= Parameters.FilterImagesWithSmallDimensions)
                )
            {
                Reporting.Say("Image too small to be useful");
                throw new Exception("Image too small");
            }

            if (Parameters.FilterImagesWithLargeDimensions > 0 &&
                (OriginalInputImage.Width >= Parameters.FilterImagesWithLargeDimensions ||
                OriginalInputImage.Height >= Parameters.FilterImagesWithLargeDimensions)
                )
            {
                Reporting.Say("Image too large to be useful");
                throw new Exception("Image too large");
            }

            this.Inputs = new List<CGPImage>();

            if (BrightnessAdjust > 0)
                OriginalInputImage._Mul(BrightnessAdjust);

            this.Inputs.Add(new CGPImage(OriginalInputImage.Convert<Gray, float>(), "GrayLevel"));

            if (SplitHSB)
            {
                Image<Hsv, float> HsvImg = OriginalInputImage.Convert<Hsv, float>();
                Image<Gray, float>[] HsvComps = HsvImg.Split();
                HsvComps[1]._Mul(255);
                this.Inputs.Add(new CGPImage(HsvComps[0], "H"));
                this.Inputs.Add(new CGPImage(HsvComps[1], "S"));
                this.Inputs.Add(new CGPImage(HsvComps[2], "B"));
            }

            if (SplitRGB)
            {
                Image<Gray, float>[] RgbComps = OriginalInputImage.Split();
                this.Inputs.Add(new CGPImage(RgbComps[0], "B"));
                this.Inputs.Add(new CGPImage(RgbComps[1], "G"));
                this.Inputs.Add(new CGPImage(RgbComps[2], "R"));
            }




            if (Parameters.LocalNormalizeInputs)
            {
                List<CGPImage> AdditionalProperties = new List<CGPImage>();
                foreach (CGPImage Cmp in Inputs)
                {
                    AdditionalProperties.Add(CGPImageFunctions.LocalNormalizeWithGauss(Cmp, 2, 20));
                    //   AdditionalProperties.Add(CGPImageFunctions.LocalNormalizeWithGauss(Cmp, 4, 20));
                    // AdditionalProperties.Add(CGPImageFunctions.LocalNormalizeWithGauss(Cmp, 2, 40));
                    //AdditionalProperties.Add(CGPImageFunctions.LocalNormalizeWithGauss(Cmp, 10, 40));
                }
                if (Parameters.UseOnlyLocalNormalizeInputs)
                    Inputs.Clear();
                Inputs.AddRange(AdditionalProperties);
            }

            if (Parameters.PreComputeAdditionalInputs)
            {
                List<CGPImage> AdditionalProperties = new List<CGPImage>();
                foreach (CGPImage Cmp in Inputs)
                    AdditionalProperties.AddRange(ImageProperties.GetProperties(Cmp));

                Inputs.AddRange(AdditionalProperties);
            }

            foreach (CGPImage Cmp in Inputs)
                Cmp.AssociatedFileName = "CAMIMG";

            

            if (ScaleImages)
            {
                List<CGPImage> ScaledImages = new List<CGPImage>();
                if (Parameters.Rescales != null)
                {
                    foreach (CGPImage Orig in Inputs)
                    {
                        foreach (double S in Parameters.Rescales)
                        {
                            CGPImage t = CGPImageFunctions.Rescale(Orig, S, false);
                            ScaledImages.Add(t);                          
                        }
                    }                    
                }
                if (Parameters.RescaleToFit != null)
                {
                    foreach (CGPImage Orig in Inputs)
                    {
                        foreach (Size S in Parameters.RescaleToFit)
                        {
                            double XScale = double.MaxValue;
                            double YScale = double.MaxValue;

                            if (S.Width > 1)
                            {
                                XScale = (double)S.Width / (double)(Orig.Width);
                            }
                            else if (S.Width > 1)
                            {
                                YScale = (double)S.Height / (double)(Orig.Height);
                            }
                            else
                                throw new Exception("One of the resize dimensions must be above 1");

                            CGPImage t = CGPImageFunctions.Rescale(Orig, Math.Min(XScale, YScale), true);
                            ScaledImages.Add(t);
                            Reporting.Say("Image resized by " + S + " to " + t.Width + "x" + t.Height + " with scale " + XScale + "x" + YScale);
                        }
                    }                    
                }

                Inputs = ScaledImages;

            }


            //this.IgnoreMask = null;
            this.ValidationImage = this.Inputs[0].Clone();

            this.InputFileName = "CAMIMG";
            this.TargetFileName = "NA";

        }

        public CGPImageTestCase Extract(Rectangle ROI)
        {
            CGPImageTestCase T = new CGPImageTestCase();
            T.InputFileName = this.InputFileName;
            T.TargetFileName = this.TargetFileName;
            T.ValidationImage = CGPImageFunctions.Extract(this.ValidationImage, ROI);
            T.Target = CGPImageFunctions.Extract(this.Target, ROI);
            T.Inputs = new List<CGPImage>();
            foreach (CGPImage I in this.Inputs)
                T.Inputs.Add(CGPImageFunctions.Extract(I, ROI));
            return T;
        }

        public CGPImageTestCase Rotate(double Angle)
        {
            CGPImageTestCase T = new CGPImageTestCase();
            T.InputFileName = this.InputFileName;
            T.TargetFileName = this.TargetFileName;

            T.ValidationImage = CGPImageFunctions.Rotate(this.ValidationImage, Angle);
            T.Target = CGPImageFunctions.Rotate(this.Target, Angle);
            T.Inputs = new List<CGPImage>();
            foreach (CGPImage I in this.Inputs)
                T.Inputs.Add(CGPImageFunctions.Rotate(I, Angle));
            return T;
        }
    }

    public class CGPImageTestSet
    {
        public List<CGPImageTestCase> TestCases = new List<CGPImageTestCase>();

        public void CheckCheckSum()
        {
            foreach (CGPImageTestCase TC in TestCases)
                TC.CheckCheckSum();
        }

        public void MakeCheckSums()
        {
            foreach (CGPImageTestCase TC in TestCases)
            {
                TC.Target.CheckSum = TC.Target.Img.GetSum().Intensity;
                foreach (CGPImage I in TC.Inputs)
                    I.CheckSum = I.Img.GetSum().Intensity;
            }
        }

        public CGPImageTestSet(
           string InputFolder,
           string TargetsFolder,
           string Filter,
           bool ScaleImages,
            bool SplitHSB,
           bool SplitRGB,
            bool FilterRedForPositive,
            bool FilterBlueForIgnore, 
            bool InvertTarget)
            
        {
            this.TestCases = new List<CGPImageTestCase>();

            string[] InputImageFileNames = Directory.GetFiles(InputFolder, Filter);
            Random RNG = new Random(67);
            foreach(float BrightnessAdjust in Parameters.InputImageBrightnesses)
            foreach (string InputFile in InputImageFileNames)
            {
                string TargetFileName = TargetsFolder + "\\" + Path.GetFileName(InputFile);
                TargetFileName = TargetFileName.Replace("\\\\", "\\");
                TargetFileName = TargetFileName.Replace("\\", "/");
                TargetFileName = TargetFileName.Replace("//", "/");
                if (!File.Exists(TargetFileName))
                    continue;

                try
                {

                    CGPImageTestCase TestCase = new CGPImageTestCase(InputFile, TargetFileName, ScaleImages, SplitRGB, SplitHSB, FilterRedForPositive, FilterBlueForIgnore, InvertTarget, BrightnessAdjust);

                    //for (int i = 0; i < TestCase.Inputs.Length; i++)
                    //    TestCase.Inputs[i].Save("Input_" + this.TestCases.Count + "_" + i + ".png");

                    //   TestCase.Target.Save("Target" + this.TestCases.Count + ".png");
                    //TestCase = TestCase.Extract(new Rectangle(128, 128, 128, 128));
                    if (Parameters.ChunkTestCases)
                    {

                        List<CGPImageTestCase> Chunks = SmartChunk(TestCase);
                        for (int c = 0; c < Chunks.Count; c++)
                        {

                            CGPImageTestCase Chunk = Chunks[c];
                            this.TestCases.Add(Chunk);

                            if (this.TestCases.Count >= Parameters.MaxFilesToLoad)
                                break;
                            if (Parameters.AddRotatedTestCases != null && TestCase != null)
                            {
                                foreach (double Angle in Parameters.AddRotatedTestCases)
                                {
                                    CGPImageTestCase Rotated = Chunk.Rotate(Angle);
                                    this.TestCases.Add(Rotated);
                                    if (this.TestCases.Count >= Parameters.MaxFilesToLoad)
                                        break;
                                }
                            }
                        }
                        TestCase.UseType = CGPImageTestCase.TestCaseUseType.Validation;
                        TestCase = null;
                        System.GC.Collect();
                        Thread.Sleep(0);

                    }
                    else
                    {
                        this.TestCases.Add(TestCase);
                    }
                    //int InsertTo = RNG.Next(0, this.TestCases.Count);
                    //TestCases.Insert(InsertTo, TestCase);


                    if (Parameters.AddRotatedTestCases != null && TestCase != null)
                    {
                        foreach (double Angle in Parameters.AddRotatedTestCases)
                        {
                            CGPImageTestCase Rotated = TestCase.Rotate(Angle);
                            this.TestCases.Add(Rotated);
                            if (this.TestCases.Count >= Parameters.MaxFilesToLoad)
                                break;
                        }
                    }

                   /* if (Parameters.InputImageBrightnesses != null && TestCase != null)
                    {
                        foreach (double Brightness in Parameters.InputImageBrightnesses)
                        {
                            CGPImageTestCase Altered = new CGPImageTestCase();
                            Altered.Inputs = new List<CGPImage>();
                            Altered.Target = TestCase.Target.Clone();
                            Altered.ValidationImage = TestCase.ValidationImage == null ? null : TestCase.ValidationImage.Clone();
                            Altered.InputFileName = TestCase.InputFileName;
                            Altered.TargetFileName = TestCase.TargetFileName;
                            foreach(CGPImage I in TestCase.Inputs)
                                Altered.Inputs.Add(CGPImageFunctions.Mul(I, (float)Brightness));

                            this.TestCases.Add(Altered);
                            if (this.TestCases.Count >= Parameters.MaxFilesToLoad)
                                break;
                        }
                    }*/
                }
                catch (Exception err)
                {
                    Reporting.Say("Unable to load " + InputFile);
                    Reporting.Say(err.ToString());
                }
                if (this.TestCases.Count >= Parameters.MaxFilesToLoad)
                    break;
            }

            /*for (int i = 0; i < this.TestCases.Count; i++)
            {
                int a = RNG.Next(0, this.TestCases.Count);
                int b = RNG.Next(0, this.TestCases.Count);
                CGPImageTestCase temp = this.TestCases[a];
                this.TestCases[a] = this.TestCases[b];
                this.TestCases[b] = temp;
            }*/

        }

        private List<CGPImageTestCase> SmartChunk(CGPImageTestCase TestCase)
        {
            List<CGPImageTestCase> Chunks = new List<CGPImageTestCase>();

            List<Rectangle> Blobs = CGPImageFunctions.Blobs(TestCase.Target, 2, true, -1, Parameters.SmartChunkScale);
            /* Image<Gray, float> BlobImage = TestCase.Inputs[0].Img.Clone();
             foreach (Rectangle R in Blobs)
             {
                 BlobImage.Draw(R, new Gray(255), 2);
             }

             BlobImage.Save("smartchunks_" + Path.GetFileNameWithoutExtension(TestCase.InputFileName) + ".png");
             */
            if (Parameters.SmartChunkAddRandomAreas)
            {
                List<Rectangle> RandomBlobs = new List<Rectangle>();
                Random RNG = new Random(745);

                for (int i = 0; i < Parameters.SmartChunkAddRandomAreasCount; i++)
                    foreach (Rectangle R in Blobs)
                    {
                        Rectangle RandomRect = new Rectangle(
                            RNG.Next(0, TestCase.Target.Width - R.Width),
                            RNG.Next(0, TestCase.Target.Height - R.Height),
                            R.Width,
                            R.Height);
                        RandomBlobs.Add(RandomRect);
                    }

                Blobs.AddRange(RandomBlobs);
            }

            foreach (Rectangle R in Blobs)
            {

                CGPImageTestCase NewTestCase = TestCase.Extract(R);
                Chunks.Add(NewTestCase);
            }

            return Chunks;
        }

        public CGPImageTestSet()
        {
        }

        public CGPImageTestSet(
          string InputFolder,
          bool ScaleImages,
           bool FilterRed, bool InvertTarget)
        {
            this.TestCases = new List<CGPImageTestCase>();

            string[] InputImageFileNames = Directory.GetFiles(InputFolder, "*.txt");

            foreach (string InputFile in InputImageFileNames)
            {

                CGPImageTestCase TestCase = new CGPImageTestCase(InputFile, FilterRed, ScaleImages, InvertTarget);

                //   for (int i = 0; i < TestCase.Inputs.Length; i++)
                //      TestCase.Inputs[i].Save("Input_" + this.TestCases.Count + "_" + i + ".png");

                //     TestCase.Target.Save("Target" + this.TestCases.Count + ".png");

                this.TestCases.Add(TestCase);

                if (this.TestCases.Count >= Parameters.MaxFilesToLoad)
                    break;
            }

            Parameters.MaxFilesToLoad = this.TestCases.Count;
        }

        public void SaveToPrecooked()
        {
            if (!Directory.Exists(Parameters.PrecookedFolder))
            {
                Directory.CreateDirectory(Parameters.PrecookedFolder);
            }

            TextWriter tw = new StreamWriter(Parameters.PrecookedFolder + "/precookedinputs.txt");
            int TcIndex = 0;
            foreach (CGPImageTestCase tc in this.TestCases)
            {
                tw.WriteLine(TcIndex + "\t" + tc.Inputs.Count);
                for (int i = 0; i < tc.Inputs.Count; i++)
                {
                    tc.Inputs[i].Save(String.Format("{0}/{1:0000000}_input_{2:0000}.png", Parameters.PrecookedFolder, TcIndex, i));
                }
                tc.Target.Save(String.Format("{0}/{1:0000000}_target.png", Parameters.PrecookedFolder, TcIndex));

                TcIndex++;
            }
            tw.Close();
        }

        public void LoadFromPrecooked()
        {
            string[] Lines = File.ReadAllLines(Parameters.PrecookedFolder + "/precookedinputs.txt");
            this.TestCases = new List<CGPImageTestCase>();
            int TcIndex = 0;
            Reporting.Say("Loading from precooked inputs...");
            foreach (string Line in Lines)
            {

                string[] Tokens = Line.Split('\t');
                Reporting.Say("\tLoading test case " + Tokens[0]);
                int InputCount = int.Parse(Tokens[1]);
                CGPImageTestCase tc = new CGPImageTestCase();
                tc.Inputs = new List<CGPImage>();
                for (int i = 0; i < InputCount; i++)
                {

                    CGPImage Img = new CGPImage(String.Format("{0}/{1:0000000}_input_{2:0000}.png", Parameters.PrecookedFolder, TcIndex, i));
                    tc.Inputs.Add(Img);

                }
                tc.Target = new CGPImage(String.Format("{0}/{1:0000000}_target.png", Parameters.PrecookedFolder, TcIndex));
                tc.InputFileName = Tokens[1] + ".png";
                tc.TargetFileName = Tokens[1] + ".png";
                this.TestCases.Add(tc);
                TcIndex++;
            }


        }

        public void LabelTestAndTraining(double POfValidation)
        {
            /*FastRandom RNG = new FastRandom(12345);
            for (int i = 0; i < this.TestCases.Count; i++)
            {
                this.TestCases[i].UseType = RNG.NextDouble()<POfValidation ? CGPIP2.CGPImageTestCase.TestCaseUseType.Validation : CGPIP2.CGPImageTestCase.TestCaseUseType.Training;
            }*/
            FastRandom RNG = new FastRandom(45);
            int ValidationCases = (int)Math.Round(this.TestCases.Count * POfValidation);
            if (this.TestCases.Count > 1 && ValidationCases == 0)
                ValidationCases = 1;
            Reporting.Say("Setting validation/training. " + ValidationCases + " validation cases out of " + this.TestCases.Count);
            for (int i = 0; i < ValidationCases; i++)
            {
                int Index = RNG.Next(0, this.TestCases.Count);
                while (this.TestCases[Index].UseType == CGPImageTestCase.TestCaseUseType.Validation)
                    Index = RNG.Next(0, this.TestCases.Count);
                this.TestCases[Index].UseType = CGPImageTestCase.TestCaseUseType.Validation;
                Reporting.Say("\tPicked " + Index + " for validation " + this.TestCases[Index].InputFileName);
            }
            Reporting.Say("Finished picking validation/traning.");
        }
    }
}